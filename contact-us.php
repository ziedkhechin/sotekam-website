<?php
	require_once "assets/config.php";
	if (isset($_POST['name']) && !empty($_POST['name']) && isset($_POST['email']) && !empty($_POST['email']) && isset($_POST['phone']) && !empty($_POST['phone']) && isset($_POST['subject']) && !empty($_POST['subject']) && isset($_POST['message']) && !empty($_POST['message']))
	{
		mysqli_query($con, "INSERT INTO messages (name, email, phone, subject, message) VALUES ('".trim(mysqli_real_escape_string($con, $_POST['name']))."','".trim(mysqli_real_escape_string($con, $_POST['email']))."','".trim(mysqli_real_escape_string($con, $_POST['phone']))."','".trim(mysqli_real_escape_string($con, $_POST['subject']))."','".trim(mysqli_real_escape_string($con, $_POST['message']))."')");
	}
	include 'cart.php';
	$cart= new Cart;
	echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
	getHead(["color"=>"#1A569F","title"=>trans("Contact-us"),
			"keywords"=>"Vidéo Surveillance,CCTV,Système Alarme,Détection d’Incendie,Télédistribution,Pointeuse,Contrôle d'accés,Videophone,Porte Automatique,GPS,Accessoires,anti-intrusion,anti-incendie,caméra de surveillance,contrôle d'accès,géolocalisation,sécurité,sécurité maison,tunisie,tunis",
			"description"=>"Nous sommes fiers du service à la clientèle que nous fournissons. Contactez-nous aujourd'hui et nous vous aiderons à votre demande.",
			"icon"=>"/assets/images/logo-icon-dark.png",
			"css"=>["https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css","assets/old/css/stack-interface.css","assets/old/css/theme.css"],
			"js"=>["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"]]);
	echo "<body data-smooth-scroll-offset=\"77\">\n";
	getPreloader("public");
	getNavbar("simple", $cart->total_items());
?>
	<a id="start"></a>
	<style type="text/css">
		.gm-style {font: 400 11px Roboto, Arial, sans-serif; text-decoration: none;} .gm-style img {max-width: none;}
		.gm-style .gm-style-cc span,.gm-style .gm-style-cc a,.gm-style .gm-style-mtc div{font-size:10px}
		@media print {.gm-style .gmnoprint, .gmnoprint {display:none}}@media screen {.gm-style .gmnoscreen, .gmnoscreen {display:none}}
		.gm-style-pbc{transition:opacity ease-in-out;background-color:rgba(0,0,0,0.45);text-align:center}.gm-style-pbt{font-size:22px;color:white;font-family:Roboto,Arial,sans-serif;position:relative;margin:0;top:50%;-webkit-transform:translateY(-50%);-ms-transform:translateY(-50%);transform:translateY(-50%)}
	</style>
	<div class="main-container">
		<section class="imagebg image--light cover cover-blocks height-70 parallax" style="visibility: visible;">
			<div class="background-image-holder" style="background: url('assets/images/promo-1.jpg'); opacity: 1; transform: translate3d(0px, 0px, 0px);"><img alt="background" src="assets/images/promo-1.jpg"></div>
			<div class="container pos-vertical-center">
				<div class="row">
					<div class="col-sm-6 col-md-5">
						<div>
							<h1>CONTACTEZ-NOUS</h1>
							<p class="lead">Nous sommes fiers du service à la clientèle que nous fournissons. Contactez-nous aujourd'hui et nous vous aiderons à votre demande.</p>
							<hr class="short">
							<p>Envoyez-nous toutes vos demandes d'information, propositions<br class="hidden-xs hidden-sm">
							et commentaires sur les offres et les services <a href="index.php">SOTEKAM</a>.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="space--xxs"></section>
		<section class="unpad">
			<div class="map-container border--round" data-address="35.691347, 10.102231" data-map-style="[{&quot;featureType&quot;:&quot;landscape&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#FFBB00&quot;},{&quot;saturation&quot;:43.400000000000006},{&quot;lightness&quot;:37.599999999999994},{&quot;gamma&quot;:1}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#FFC200&quot;},{&quot;saturation&quot;:-61.8},{&quot;lightness&quot;:45.599999999999994},{&quot;gamma&quot;:1}]},{&quot;featureType&quot;:&quot;road.arterial&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#FF0300&quot;},{&quot;saturation&quot;:-100},{&quot;lightness&quot;:51.19999999999999},{&quot;gamma&quot;:1}]},{&quot;featureType&quot;:&quot;road.local&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#FF0300&quot;},{&quot;saturation&quot;:-100},{&quot;lightness&quot;:52},{&quot;gamma&quot;:1}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#0078FF&quot;},{&quot;saturation&quot;:-13.200000000000003},{&quot;lightness&quot;:2.4000000000000057},{&quot;gamma&quot;:1}]},{&quot;featureType&quot;:&quot;poi&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#00FF6A&quot;},{&quot;saturation&quot;:-1.0989010989011234},{&quot;lightness&quot;:11.200000000000017},{&quot;gamma&quot;:1}]}]" data-map-zoom="15" data-maps-api-key="AIzaSyCfo_V3gmpPm1WzJEC9p_sRbgvyVbiO83M" data-marker-title="Stack"></div>
		</section>
		<section class="space--xs switchable">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-12">
						<form class="row" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
							<div class="col-md-6 col-12"><label>Votre Nom:</label><input type="text" name="name" maxlength="50" required=""></div>
							<div class="col-md-6 col-12"><label>Adresse Email:</label><input type="email" name="email" maxlength="50" required=""></div>
							<div class="col-md-6 col-12"><label>Votre Numéro de Téléphone:</label><input type="text" name="phone" maxlength="15" required=""></div>
							<div class="col-md-6 col-12"><label>Sujet:</label><input type="text" name="subject" maxlength="100" required=""></div>
							<div class="col-md-12 col-12"><label>Message:</label><textarea rows="4" name="message" required=""></textarea></div>
							<div class="col-12"></div><div class="col-md-5 col-lg-4 col-6">
								<button type="submit" class="btn btn--primary type--uppercase">Envoyer</button>
							</div>
						</form>
					</div>
					<div class="col-md-5">
						<p class="lead">E: <a href="mailto:contact@sotekam.tn">CONTACT@SOTEKAM.TN</a><br />P: <a href="tel:+216 70 035 349">+216 70 035 349</a></p>
						<p class="lead">Appelez-nous ou déposez-vous en tout temps, nous nous efforçons de répondre à toutes les demandes dans les 10 heures les jours ouvrables.</p>
						<p class="lead">Nous sommes ouverts 8—13, 16—19<br />du lundi au samedi.</p>
					</div>
				</div>
			</div>
		</section>
<?php
	getFooter("dark");
	mysqli_close($con);
?>
		<a class="back-to-top inner-link" data-scroll-class="100vh:active" href="#start"><i class="stack-interface stack-up-open-big"></i></a>
	</div><?php getJSCalls(["assets/old/js/parallax.js","assets/old/js/smooth-scroll.min.js","assets/old/js/scripts.js"]); ?>
	<script async class="gMapsAPI" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDE8dvwLh1qttF0gsM87QsWL8YXQfHNpQg&amp;callback=mr.maps.init" type="text/javascript"></script>
	<script async type="text/javascript" src="https://www.googletagmanager.com/gtag/js?id=UA-135626600-1"></script>
	<script async type="text/javascript">
		window.dataLayer= window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-135626600-1');
	</script>
</body>
</html>