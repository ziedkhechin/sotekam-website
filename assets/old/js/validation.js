function ValidateContactForm()
{
    var email = ContactForm.email;
    var last_name = ContactForm.last_name;
    var first_name = ContactForm.first_name;
    var phone = ContactForm.phone;
    var address1 = ContactForm.address1;
    var city = ContactForm.city;
    var zip = ContactForm.zip;
	if (email.value == ""){email.focus(); return false;}
	if (last_name.value == ""){last_name.focus(); return false;}
	if (first_name.value == ""){first_name.focus(); return false;}
	if ((phone.value.length <8)||(isNaN(phone.value))){phone.focus(); return false;}
	if (address1.value == ""){address1.focus(); return false;}
	if (city.value == "empty"){city.focus(); return false;}
	if ((zip.value.length != 4)||(isNaN(zip.value))){zip.focus(); return false;}
}