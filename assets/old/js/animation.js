(function($) {
	"use strict";
	$(window).load(function() {
		$(".loader").delay(1600).fadeOut();
		$(".animationload").delay(2600).fadeOut("slow");
	});
})(jQuery);