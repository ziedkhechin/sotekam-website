<?php
session_start();
$data=json_decode(file_get_contents('/home/ncnkmcxd/public_html/assets/lang.json'), true); // Get file of all website text
$conf=['server'=>'localhost', 'username'=>'ncnkmcxd_st_user', 'password'=>'so-account0login', 'database'=>'ncnkmcxd_sotekam']; // Default server user
$con= mysqli_connect($conf['server'], $conf['username'], $conf['password'], $conf['database']) or die("Unable to connect database: ".mysqli_connect_error());
$db= new mysqli($conf['server'], $conf['username'], $conf['password'], $conf['database']) or die("Unable to connect database: ".$db->connect_error);
if (!isset($_SESSION['language'])) $_SESSION['language']='fr'; // Define default website language if not exist.
if (isset($_GET['lang'])) {changeLang($_GET['lang']); header('Location: '.$_SERVER['PHP_SELF']); exit;} // Change website language
mysqli_set_charset($con, "utf8"); $db->set_charset("utf8"); // Define default character set when exchanging with database server
date_default_timezone_set('Africa/Tunis'); // Define timezone

function trans ($text) // Translating Texts
{
	global $data;
	return $data[$text][$_SESSION['language']];
}

function changeLang ($lang)
{
	global $data, $con;
	if (isset($data['SOTEKAM'][$lang]))
	{
		$_SESSION['language']=$_GET['lang'];
		if (isset($_SESSION['username']))
			mysqli_query($con, "UPDATE users SET language='".mysqli_real_escape_string($con, $lang)."' WHERE username='".$_SESSION['username']."'");
	}
}

function getHead ($tags=[])
{
	echo "<head>
	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />
	<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
	<meta name=\"author\" content=\"ZIED KHECHIN\" />
	<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=0, minimal-ui\" />
	<meta name=\"apple-mobile-web-app-capable\" content=\"yes\" />";
	foreach ($tags as $key=>$val)
	{
		switch ($key)
		{
			case 'keywords': echo "\n\t<meta name=\"keywords\" content=\"".$val."\" />"; break;
			case 'description': echo "\n\t<meta name=\"description\" content=\"".$val."\" />"; break;
			case 'color': echo "\n\t<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"".$val."\" />\n\t<meta name=\"theme-color\" content=\"".$val."\" />"; break;
			case 'title': echo "\n\t<title>".$val." – ".trans('SOTEKAM')."</title>"; break;
			case 'icon': echo "\n\t<link rel=\"icon\" href=\"".$val."\" />\n\t<link rel=\"apple-touch-icon\" href=\"".$val."\" />"; break;
			case 'css':
				foreach ($val as $style)
					echo "\n\t<link rel=\"stylesheet\" type=\"text/css\" href=\"".$style."\" />";
				break;
			case 'js':
				foreach ($val as $script)
					echo "\n\t<script type=\"text/javascript\" src=\"".$script."\"></script>";
				break;
		}
	}
	echo "\n</head>\n";
}

function getPreloader ($type="admin")
{
	if ($type=="public")
		echo "\t<style>\n\t\t.no-js #loader {display: none;}\n\t\t.js #loader {display: block; position: absolute; left: 100px; top: 0;}\n\t\t.se-pre-con {position: fixed; width: 100%; height: 100%; z-index: 9999; background: url('assets/images/loading.gif') center no-repeat #FFFFFF; background-size: 100px 100px;}\n\t</style>\n\t<script>\$(window).on('load', function() {\$('.se-pre-con').fadeOut('slow');;});</script>\n\t<div class=\"se-pre-con\"></div>\n";
	elseif ($type=="admin")
		echo "\n\t<div class=\"preloader\"><div class=\"loader\"><div class=\"loader__figure\"></div><p class=\"loader__label\">SOTEKAM Admin</p></div></div>\n";
}

function getNavbar ($type, $total_items)
{
?>
	<div class="nav-container">
		<div class="bar bar--sm visible-xs">
			<div class="container">
				<div class="row">
					<div class="col-xs-3 col-sm-2">
						<img alt="logo" class="logo logo-dark" src="assets/images/logo-dark.png"><img alt="logo" class="logo logo-light" src="assets/images/logo-light.png">
					</div>
					<div class="col-xs-9 col-sm-10 text-right">
						<a class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs" href="#"><i class="fas fa-bars"></i></a>
					</div>
				</div>
			</div>
		</div>
		<nav class="bar bar--sm bar-1 hidden-xs<?php echo ($type=="transp")? " bar--transparent bar--absolute\" data-scroll-class=\"90vh:pos-fixed\"" : "\" data-scroll-class=\"366px:pos-fixed\""; ?> id="menu1">
			<div class="container">
				<div class="row">
					<div class="col-md-1 col-sm-2 hidden-xs">
						<div class="bar__module">
							<img alt="logo" class="logo logo-dark" src="assets/images/logo-dark.png"><img alt="logo" class="logo logo-light" src="assets/images/logo-light.png">
						</div>
					</div>
					<div class="col-sm-12 text-right text-left-xs text-left-sm col-md-11">
						<div class="bar__module">
							<ul class="menu-horizontal text-left">
								<li><a href="index.php" target="_self">Accueil</a></li>
								<li class="dropdown">
									<span class="dropdown__trigger">Produits</span>
									<div class="dropdown__container">
										<div class="container">
											<div class="row">
												<div class="dropdown__content col-md-3">
													<ul class="menu-vertical">
														<li class="dropdown">
															<span class="dropdown__trigger">Vidéo Surveillance</span>
															<div class="dropdown__container">
																<div class="container">
																	<div class="row">
																		<div class="dropdown__content col-md-2 col-sm-3">
																			<ul class="menu-vertical">
																				<li><a href="all-CCTV-products.php">Tous les Produits</a></li>
																				<li><a href="URMET-CCTV.php">URMET</a></li>
																				<li><a href="Dahua-CCTV.php">Dahua</a></li>
																			</ul>
																		</div>
																	</div>
																</div>
															</div>
														</li>
														<li><a href="alarm.php">Systéme Alarme</a></li>
														<li><a href="fire-detection.php">Détection d’Incendie</a></li>
														<li><a href="tv-distribution.php">Télédistribution</a></li>
														<li><a href="access-control.php">Contrôle d'accés</a></li>
														<li><a href="videophone.php">Videophone</a></li>
														<!--<li><a href="automatic-door.php">Porte Automatique</a></li>
														<li><a href="gps.php">GPS<br></a></li>-->
														<li><a href="accessoires.php">Accessoires</a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li class="dropdown">
									<span class="dropdown__trigger">A Propos</span>
									<div class="dropdown__container">
										<div class="container">
											<div class="row">
												<div class="dropdown__content col-md-3">
													<ul class="menu-vertical">
														<li><a href="about.php">Présentation</a></li>
														<li><a href="our-projects.php">Nos Projets</a></li>
														<li><a href="our-team.php">Notre Équipe<br></a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
						<div class="bar__module">
							<a class="btn btn--sm type--uppercase" href="contact-us.php"><span class="btn__text">CONTACTEZ-NOUS<i></i></span></a>
							<a href="shop-cart.php" class="btn btn--sm btn--primary type--uppercase inner-link">
								<span class="btn__text"><?php echo $total_items; ?> articles dans votre <i class="fas fa-shopping-cart"></i><span>
							</a><a href="search.php" title="Rechercher" class="btn btn--sm type--uppercase" style="padding: 0.3em 0.55em;"><i class="btn__text fas fa-search"></i></a>
						</div>
					</div>
				</div>
			</div>
		</nav>
	</div>
<?php
}

function getFooter ($type="admin")
{
	if (in_array($type, ["dark", "white"]))
	{
?>
		<footer class="text-center-xs space--xxs<?php echo ($type=="dark")? ' bg--dark':''; ?>">
			<div class="container">
				<div class="row">
					<div class="col-sm-7">
						<ul class="list-inline">
							<li>
								<a href="about.php"><span aria-multiline="true" class="h6 type--uppercase medium-editor-element"
								data-medium-editor-editor-index="2" data-medium-editor-element="true"data-placeholder="Type your text" role="textbox" spellcheck="true"></span>
								<p><span aria-multiline="true" class="h6 type--uppercase medium-editor-element" data-medium-editor-editor-index="2"
								data-medium-editor-element="true" data-placeholder="Type your text" role="textbox" spellcheck="true">A Propos</span></p></a>
							</li>
							<li>
								<a href="use-conditions.php"><span aria-multiline="true" class="h6 type--uppercase medium-editor-element"
								data-medium-editor-editor-index="2" data-medium-editor-element="true" data-placeholder="Type your text" role="textbox" spellcheck="true"></span>
								<p><span aria-multiline="true" class="h6 type--uppercase medium-editor-element" data-medium-editor-editor-index="2"
								data-medium-editor-element="true" data-placeholder="Type your text" role="textbox" spellcheck="true">Conditions d'Utilisation</span></p></a>
							</li>
							<li>
								<a href="location.php"><span aria-multiline="true" class="h6 type--uppercase medium-editor-element" data-medium-editor-editor-index="2"
								data-medium-editor-element="true" data-placeholder="Type your text" role="textbox" spellcheck="true"></span>
								<p><span aria-multiline="true" class="h6 type--uppercase medium-editor-element" data-medium-editor-editor-index="2" data-medium-editor-element="true"
								data-placeholder="Type your text" role="textbox" spellcheck="true">Emplacement</span></p></a>
							</li>
						</ul>
					</div>
					<div class="col-sm-5 text-right text-center-xs">
						<ul class="social-list list-inline list--hover">
							<li>
								<a href="https://www.facebook.com/SOTEKAM" target="_blank"><i class="fab fa-facebook-f"></i></a>
							</li>
							<li>
								<a href="https://plus.google.com/106253300284597348906" target="_blank"><i class="fab fa-google-plus-g"></i></a>
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-7">
						<span class="type--fine-print">WEBSITE C&D BY <a href="http://on.fb.me/1zspNmb" target="_blank">ZK</a>. © <span class="update-year">2017</span> SOTEKAM Inc.</span>
					</div>
					<div class="col-sm-5 text-right text-center-xs">
						<a class="type--fine-print" href="mailto:contact@sotekam.tn" target="_self">contact@sotekam.tn</a>
					</div>
				</div>
			</div>
		</footer>
<?php
	}
	elseif ($type=="dark_large")
	{
?>
		<footer class="footer-6 unpad--bottom bg--dark">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-5">
						<h6 class="type--uppercase">Qui sommes-nous?</h6>
						<p>SOTEKAM est un fabricant de solutions de sécurité et d'automatisation pour les applications résidentielles et commerciales.
						Des systèmes d'intrusion, d'incendie et de domotique, à la dernière vidéo IP et au contrôle d'accès,
						nous nous concentrons sur les technologies qui créent des «maisons connectées» et des «bâtiments connectés».</p>
					</div>
					<div class="col-sm-6 col-md-4">
						<h6 class="type--uppercase">Facebook</h6><iframe frameborder="0" height="250" scrolling="no" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FSOTEKAM" style="border:none;overflow:hidden" width="340"></iframe>
					</div>
					<div class="col-sm-6 col-md-3">
						<h6 class="type--uppercase">Google+</h6><iframe frameborder="0" scrolling="no" src="https://apis.google.com/u/0/_/widget/render/page?usegapi=1&amp;width=273&amp;href=https%3A%2F%2Fplus.google.com%2F106253300284597348906&amp;layout=landscape&amp;rel=publisher&amp;hl=fr&amp;origin=file%3A%2F%2F&amp;gsrc=3p&amp;ic=1&amp;jsh=m%3B%2F_%2Fscs%2Fapps-static%2F_%2Fjs%2Fk%3Doz.gapi.en.xuXD-FaIHqQ.O%2Fm%3D__features__%2Fam%3DEQ%2Frt%3Dj%2Fd%3D1%2Frs%3DAGLTcCMMWCxvu0YSjIm2loIip1gTP1YxSg#_methods=onPlusOne%2C_ready%2C_close%2C_open%2C_resizeMe%2C_renderstart%2Concircled%2Cdrefresh%2Cerefresh%2Conload&amp;id=I0_1501853252636&amp;parent=file%3A%2F%2F&amp;pfname=&amp;rpctoken=25373144"></iframe>
					</div>
				</div>
			</div>
			<div class="footer__lower text-center-xs">
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
						<span class="type--fine-print">WEBSITE C&D BY <a href="http://on.fb.me/1zspNmb" target="_blank">ZK</a>. © <span class="update-year">2017</span> SOTEKAM Inc.</span>
						</div>
						<div class="col-sm-6 text-right text-center-xs">
							<ul class="social-list list-inline">
								<li>
									<a href="https://www.facebook.com/SOTEKAM" target="_blank"><i class="socicon socicon-facebook icon icon--xs"></i></a>
								</li>
								<li>
									<a href="https://plus.google.com/106253300284597348906" target="_blank"><i class="socicon socicon-google icon icon--xs"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>
<?php
	}
	elseif ($type=="admin")
		echo "\t\t<footer class=\"footer\">\n\t\t\t<span dir=\"auto\">&copy;&nbsp;<span id=\"year\"></span>\n\t\t\t".trans('SOTEKAM Inc.')." ".trans('WEBSITE CREATED & DEVELOPED BY')."&nbsp;<a href=\"http://on.fb.me/1zspNmb\" style=\"text-decoration: unset;\" target=\"_blank\">ZK</a></span>\n\t\t</footer>\n";
}

function getJSCalls ($js=[])
{
	foreach ($js as $val)
		echo "\n\t<script type=\"text/javascript\" src=\"".$val."\"></script>";
	echo "\n";
}