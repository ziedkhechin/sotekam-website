$("#year")[0].innerHTML = new Date().getFullYear(),
$('a[data-skin="'+document.body.classList[1]+'"]').addClass("working"),
$(function() {
	"use strict";
	$(function() {
		$(".preloader").fadeOut()
	}), jQuery(document).on("click", ".mega-dropdown", function(e) {
		e.stopPropagation()
	});
	var e = function() {
		(window.innerWidth > 0 ? window.innerWidth : this.screen.width) < 1170 ? ($("body").addClass("mini-sidebar"), $(".sidebartoggler i").addClass("ti-menu")) : $("body").removeClass("mini-sidebar");
		var e = (window.innerHeight > 0 ? window.innerHeight : this.screen.height) - 1;
		(e -= 75) < 1 && (e = 1), e > 75 && $(".page-wrapper").css("min-height", e + "px")
	};
	$(window).ready(e), $(window).on("resize", e), $(".sidebartoggler").on("click", function() {
		$("body").toggleClass("mini-sidebar")
	}), $(".nav-toggler").on("click", function() {
		$("body").toggleClass("show-sidebar")
	}), $(".nav-lock").on("click", function() {
		$("body").toggleClass("lock-nav"), $(".page-wrapper").trigger("resize")
	}), $(".search-box a, .search-box .app-search .srh-btn").on("click", function() {
		$(".app-search").toggle(200), $(".app-search input").focus()
	}), $(".right-side-toggle").click(function() {
		$(".right-sidebar").slideDown(50), $(".right-sidebar").toggleClass("shw-rside")
	}), $(".floating-labels .form-control").on("focus blur", function(e) {
		$(this).parents(".form-group").toggleClass("focused", "focus" === e.type || 0 < this.value.length)
	}).trigger("blur"), $(function() {
		$('[data-toggle="tooltip"]').tooltip()
	}), $(function() {
		$('[data-toggle="popover"]').popover()
	}), $(".scroll-sidebar, .right-side-panel, .message-center, .right-sidebar").perfectScrollbar(), $("body, .page-wrapper").trigger("resize"), $(".list-task li label").click(function() {
		$(this).toggleClass("task-done")
	}), $('a[data-action="collapse"]').on("click", function(e) {
		e.preventDefault(), $(this).closest(".card").find('[data-action="collapse"] i').toggleClass("fas fa-minus fas fa-plus"), $(this).closest(".card").children(".card-body").collapse("toggle")
	}), $('a[data-action="expand"]').on("click", function(e) {
		e.preventDefault(), $(this).closest(".card").find('[data-action="expand"] i').toggleClass("mdi-arrow-expand mdi-arrow-compress"), $(this).closest(".card").toggleClass("card-fullscreen")
	}), $('a[data-action="close"]').on("click", function() {
		$(this).closest(".card").removeClass().slideUp("fast")
	});
	var a, n = {
		"skin-default": "363d4a", "skin-green": "318c8f", "skin-red": "f62d51", "skin-blue": "00b6d2", "skin-purple": "513ac6", "skin-megna": "1e6f9e", "skin-default-dark": "363d4a",
		"skin-green-dark": "318c8f", "skin-red-dark": "f62d51", "skin-blue-dark": "00b6d2",	"skin-purple-dark": "513ac6", "skin-megna-dark": "1e6f9e"
	};
	function t(e) {
		var a= new XMLHttpRequest();
		return $.each(n, function(e) {
			$("body").removeClass(e)
		}), $("body").addClass(e), $('meta[name="theme-color"]').attr("content", "#" + n[e]), $('meta[name="apple-mobile-web-app-status-bar-style"]').attr("content", "#" + n[e]), "skin", a.open("GET", "index.php?action=change_theme&theme=" + e + "&color=" + n[e], !0), a.send()
	}(a = void("undefined" != typeof Storage || window.alert("Please use a modern browser to properly view this template!"))) && $.inArray(a, n) && t(a), $("[data-skin]").on("click", function(e) {
		$(this).hasClass("knob") || (e.preventDefault(), t($(this).data("skin")))
	}), $("#themecolors").on("click", "a", function() {
		$("#themecolors li a").removeClass("working"), $(this).addClass("working")
	})
});