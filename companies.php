﻿<!DOCTYPE html>
<html>
<head>
	<link href='assets/old/css/style2.css' id='billio_report_post_style-css' media='all' rel='stylesheet' type='text/css'>
	<link href='assets/old/css/bootstrap2.css' media='all' rel='stylesheet' type='text/css'>
	<link href='assets/old/css/js_composer.min.css' id='js_composer_front-css' media='all' rel='stylesheet' type='text/css'>
	<script src='assets/old/js/jquery.js' type='text/javascript'></script>
</head>
<body>
	<div class="container">
		<div class="col-xs-12">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="vc_row wpb_row vc_row-fluid vc_custom_detheme3">
						<div class="wpb_column vc_column_container vc_col-sm-12">
							<div class="vc_column-inner">
								<div class="wpb_wrapper">
									<div class="wpb_text_column wpb_content_element wpb_animate_when_almost_visible wpb_left-to-right">
										<div class="wpb_wrapper">
											<p><a href="http://www.radiosabrafm.net" target="_blank"><img alt="SABRA FM" class="alignleft wp-image-12835" height="84" src="assets/images/SABRA FM.jpg" width="150"></a></p>
											<p>Radio généraliste dont le studio est installé à Kairouan, elle est lancée le 14 janvier 2012, elle émet principalement dans les régions du Grand Tunis, Kairouan,
											Nabeul, Bizerte, Sousse, Monastir, Sfax.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="wpb_column vc_column_container vc_col-sm-12">
							<div class="vc_column-inner">
								<div class="wpb_wrapper">
									<div class="wpb_text_column wpb_content_element partnertext wpb_animate_when_almost_visible wpb_right-to-left">
										<div class="wpb_wrapper">
											<p><a href="http://www.steg.com.tn" target="_blank"><img alt="STEG" class="alignright wp-image-12849" height="84" src="assets/images/STEG.jpg" width="150"></a></p>
											<p>La Société tunisienne de l'électricité et du gaz ou STEG une société tunisienne de droit public à caractère non administratif. Créée en 1962, elle a pour
											mission la production et la distribution de l'électricité et du gaz naturel sur le territoire tunisien. La STEG est la deuxième plus grande entreprise
											tunisienne par son chiffre d'affaires en 2012.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="vc_row wpb_row vc_row-fluid vc_custom_detheme4">
						<div class="wpb_column vc_column_container vc_col-sm-12">
							<div class="vc_column-inner">
								<div class="wpb_wrapper">
									<div class="wpb_text_column wpb_content_element partnertext wpb_animate_when_almost_visible wpb_left-to-right">
										<div class="wpb_wrapper">
											<p><a href="https://www.goldenyasmin.com" target="_blank"><img alt="GOLDEN YASMIN" class="alignleft wp-image-12856" height="84" src="assets/images/GOLDEN YASMIN.jpg" width="150"></a></p>
											<p>La Chaîne Hôtelière Golden Yasmin Hotels, implantés dans six régions du pays sur des sites aussi variés qu’exceptionnels, les hôtels Golden Yasmin sont aussi des escales
											parfaites pour découvrir la Tunisie.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="vc_row wpb_row vc_row-fluid vc_custom_detheme5">
						<div class="wpb_column vc_column_container vc_col-sm-12">
							<div class="vc_column-inner">
								<div class="wpb_wrapper">
									<div class="wpb_text_column wpb_content_element partnertext wpb_animate_when_almost_visible wpb_right-to-left">
										<div class="wpb_wrapper">
											<p><img alt="AGRI AFFAIRES" class="alignright wp-image-12877" height="84" src="assets/images/AGRI AFFAIRE.jpg" width="150">
											AgriAffaire est une société qui vente de produits et matériels agricoles et industriels.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="vc_row wpb_row vc_row-fluid vc_custom_detheme6">
						<div class="wpb_column vc_column_container vc_col-sm-12">
							<div class="vc_column-inner">
								<div class="wpb_wrapper">
									<div class="wpb_text_column wpb_content_element partnertext wpb_animate_when_almost_visible wpb_left-to-right">
										<div class="wpb_wrapper">
											<p><img alt="SABRINE" class="alignleft wp-image-12880" height="84" src="assets/images/SABRINE.jpg" width="150">Depuis 1991, le Forage d’oued Kharroub alimente une usine de conditionnement
											d’eau sous<br />l’appellation: "Eau Minerale Naturelle SABRINE".</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="vc_row wpb_row vc_row-fluid vc_custom_detheme7">
						<div class="wpb_column vc_column_container vc_col-sm-12">
							<div class="vc_column-inner">
								<div class="wpb_wrapper">
									<div class="wpb_text_column wpb_content_element partnertext wpb_animate_when_almost_visible wpb_right-to-left">
										<div class="wpb_wrapper">
											<p><img alt="SAFAC" class="alignright wp-image-12893" height="84" src="assets/images/SAFAC.jpg" width="150">La Société Afrique Acier SAFAC recrute pour ses besoins de son siège Ban Arous
											et son usine sis à ESBIKHA à Kairouan.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="vc_row wpb_row vc_row-fluid vc_custom_detheme8">
						<div class="wpb_column vc_column_container vc_col-sm-12">
							<div class="vc_column-inner">
								<div class="wpb_wrapper">
									<div class="wpb_text_column wpb_content_element partnertext wpb_animate_when_almost_visible wpb_left-to-right">
										<div class="wpb_wrapper">
											<p><img alt="SOBAF" class="alignleft wp-image-12895" height="84" src="assets/images/SOBAF.jpg" width="150">SOBAF SARL est spécialisée dans la fabrication de béton pré à l'emlpoi
											et aussi dans le secteur des travaux publics (VRD). Créer en 2012.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="vc_row wpb_row vc_row-fluid vc_custom_detheme9">
						<div class="wpb_column vc_column_container vc_col-sm-12">
							<div class="vc_column-inner">
								<div class="wpb_wrapper">
									<div class="wpb_text_column wpb_content_element partnertext wpb_animate_when_almost_visible wpb_right-to-left">
										<div class="wpb_wrapper">
											<p><img alt="JANNET" class="alignright wp-image-12899 size-full" height="84" src="assets/images/JANNET.jpg" width="150"></p>
											<p>Gardens Mineral Water Company le 7 Mars 2002 a été établi à zone Haffouz de Kairouan en Tunisie. Il est une eau minérale naturelle extraite des profondeurs de Haffouz.
											Sels léger et équilibré pur réalise le bien-être pour toute la famille.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="vc_row wpb_row vc_row-fluid vc_custom_detheme10">
						<div class="wpb_column vc_column_container vc_col-sm-12">
							<div class="vc_column-inner">
								<div class="wpb_wrapper">
									<div class="wpb_text_column wpb_content_element partnertext wpb_animate_when_almost_visible wpb_left-to-right">
										<div class="wpb_wrapper">
											<p><a href="http://www.socyptunisie.com" target="_blank"><img alt="SOCYP" class="alignleft wp-image-12907" height="84" src="assets/images/SOCYP.jpg" width="150"></a>
											SOCIETE SOCYP TUNISIE créer à 2012,est spécialisée dans la fabrication des<br />pièces pour motocycle.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="vc_row wpb_row vc_row-fluid vc_custom_detheme11">
						<div class="wpb_column vc_column_container vc_col-sm-12">
							<div class="vc_column-inner">
								<div class="wpb_wrapper">
									<div class="wpb_text_column wpb_content_element partnertext wpb_animate_when_almost_visible wpb_right-to-left">
										<div class="wpb_wrapper">
											<p><a href="https://www.luxedecors.com.tn/" target="_blank"><img alt="LUXE DECORS" class="alignright wp-image-12915" height="84" src="assets/images/LUXE DECORS.jpg" width="150"></a>
											Luxe Decors: Vente en Gros et en Détail de Papiers Peints, Posters et tout articles de décoration d'intérieur.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="vc_row wpb_row vc_row-fluid vc_custom_detheme12">
						<div class="wpb_column vc_column_container vc_col-sm-12">
							<div class="vc_column-inner">
								<div class="wpb_wrapper">
									<div class="wpb_text_column wpb_content_element partnertext wpb_animate_when_almost_visible wpb_left-to-right">
										<div class="wpb_wrapper">
											<p><img alt="Pharmacie Diwéni" class="alignleft wp-image-12922" height="84" src="assets/images/PHARMACIE.png" width="150">
											Pharmacie Diwéni assure l'approvisionnement en médecine humaine et vétérinaire dans divers établissements de santé publique, semi-publics et privés.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="vc_row wpb_row vc_row-fluid vc_custom_detheme15">
						<div class="wpb_column vc_column_container vc_col-sm-12">
							<div class="vc_column-inner">
								<div class="wpb_wrapper">
									<div class="wpb_text_column wpb_content_element partnertext wpb_animate_when_almost_visible wpb_right-to-left">
										<div class="wpb_wrapper">
											<p><img alt="Pharmacie Ben-Hamra" class="alignright wp-image-12935" height="84" src="assets/images/PHARMACIE.png" width="150">
											Pharmacie Ben-Hamra assure l'approvisionnement en médecine humaine et vétérinaire dans divers établissements de santé publique, semi-publics et privés.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src='assets/old/js/js_composer_front.min.js' type='text/javascript'></script> 
	<script src='assets/old/js/waypoints.min.js' type='text/javascript'></script>
</body>
</html>