<?php
	require_once "assets/config.php";
	include 'cart.php';
	$cart= new Cart;
	echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
	getHead(["color"=>"#1A569F","title"=>trans("Location"),
			"keywords"=>"Vidéo Surveillance,CCTV,Système Alarme,Détection d’Incendie,Télédistribution,Pointeuse,Contrôle d'accés,Videophone,Porte Automatique,GPS,Accessoires,anti-intrusion,anti-incendie,caméra de surveillance,contrôle d'accès,géolocalisation,sécurité,sécurité maison,tunisie,tunis",
			"description"=>"SOTEKAM: 3140 CITÉ ICHBILIA, route de Tunis - Kairouan - Tunisie",
			"icon"=>"/assets/images/logo-icon-dark.png",
			"css"=>["https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.min.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css","assets/old/css/stack-interface.css","assets/old/css/theme.css"],
			"js"=>["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"]]);
	echo "<body data-smooth-scroll-offset=\"77\">\n";
	getPreloader("public");
	getNavbar("simple", $cart->total_items());
?>
	<a id="start"></a>
	<div class="main-container">
		<section class="imageblock switchable feature-large space--lg">
				<div class="imageblock__content col-md-6 col-sm-4 col-xs-12 pos-right">
					<div class="map-container">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6117.133810151612!2d10.098764849041068!3d35.69081858690773!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4f4b0c0b2f334d7f!2sSOTEKAM!5e0!3m2!1sen!2stn!4v1501236859905"></iframe>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-md-5 col-sm-7">
							<h3>3140 CITÉ ICHBILIA, route de Tunis -<br />Kairouan - Tunisie</h3>
							<p class="lead"> E: <a href="mailto:contact@sotekam.tn">CONTACT@SOTEKAM.TN</a><br /> P: +216 70 035 349 </p>
							<p class="lead"> Appelez-nous ou déposez-vous en tout temps, nous nous efforçons de répondre à toutes les demandes dans les 10 heures les jours ouvrables. </p>
							<p class="lead">Nous sommes ouverts 8—13, 16—19<br />du lundi au samedi.</p>
						</div>
					</div>
				</div>
		</section>
<?php
	getFooter("dark");
	mysqli_close($con);
?>
		<a class="back-to-top inner-link" data-scroll-class="100vh:active" href="#start"><i class="stack-interface stack-up-open-big"></i></a>
	</div><?php getJSCalls(["assets/old/js/parallax.js","https://cdnjs.cloudflare.com/ajax/libs/granim/2.0.0/granim.min.js","https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.0/flickity.pkgd.min.js","assets/old/js/jquery.steps.min.js","assets/old/js/smooth-scroll.min.js","assets/old/js/scripts.js"]); ?>
	<script async type="text/javascript" src="https://www.googletagmanager.com/gtag/js?id=UA-135626600-1"></script>
	<script async type="text/javascript">
		window.dataLayer= window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-135626600-1');
	</script>
</body>
</html>