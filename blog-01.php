<?php
	require_once "assets/config.php";
	include 'cart.php';
	$cart= new Cart;
	echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
	getHead(["color"=>"#1A569F","title"=>"Découvrez les points faibles de votre maison et comment les protéger",
			"icon"=>"/assets/images/logo-icon-dark.png",
			"css"=>["https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.min.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css","assets/old/css/stack-interface.css","assets/old/css/theme.css"],
			"js"=>["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"]]);
	echo "<body class=\"boxed-layout\">\n";
	getPreloader("public");
	getNavbar("transp", $cart->total_items());
?>
	<a id="start"></a>
	<div class="main-container">
		<section class="bg--primary-1 text-center">
			<div class="container">
				<div class="row">
					<div class="col-md-7 col-sm-9 col-xs-12">
						<div class="mt--2">
							<h1>Découvrez les Points Faibles de votre Maison et Comment les Protéger</h1>
							<p class="lead">Et si vous vous mettiez cinq minutes dans la tête du cambrioleur qui étudie votre domicile? Tout est affaire d’anticipation… État des lieux à sécuriser.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="imagebg height-60">
			<div class="background-image-holder"><img alt="bg" src="assets/images/landing-04.jpg"></div>
		</section>
		<section class="text-center space--xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-md-8">
					<h3>Dois-je contacter un serrurier ou un menuisier pour renforcer mes portes et menuiseries ou alors un électricien pour l’installation d’un système d’alarme, on m’a aussi parlé des grilles qu’installe un ferronnier et si j’optais pour des caméras de surveillance?</h3><br />
					<img src="assets/images/landing-07.jpg"></div>
				</div>
			</div>
		</section>
		<section class="switchable feature-large unpad--bottom imagebg" data-overlay="3">
			<div class="background-image-holder"><img alt="background" src="assets/images/landing-09.jpg"></div>
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="switchable__text">
							<p class="lead">Choisir un système de sécurité à domicile est une étape importante dans la protection de votre maison et votre famille. Il existe de nombreuses options et coûts à prendre en considération avant de prendre une décision. Votre meilleur pari est d'acheter un système doté de fonctionnalités non exclusives, qui peut être modifié ou réparé par des pièces génériques.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="switchable space--xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 text-center"><img alt="Image" class="border--round box-shadow-wide" src="assets/images/landing-06.jpg"></div>
					<div class="col-sm-6 col-md-5">
						<ol class="process-3">
							<li class="process_item">
								<div class="process__number">
									<span>1</span>
								</div>
								<div class="process__body">
									<h4>Décidez</h4>
									<p>si vous voulez un système de sécurité professionnellement installé ou préférez une approche de bricolage où vous allez acheter et installer le système.</p>
								</div>
							</li>
							<li class="process_item">
								<div class="process__number">
									<span>2</span>
								</div>
								<div class="process__body">
									<h4>Obtenez un système avec un contrat de suivi d'un an.</h4>
									<p>La signature d'un contrat peut aider à réduire le coût initial de l'équipement, mais vous devez prendre en compte les frais d'annulation si vous voulez sortir du contrat sur la route.</p>
								</div>
							</li>
							<li class="process_item">
								<div class="process__number">
									<span>3</span>
								</div>
								<div class="process__body">
									<h4>Comptez le nombre d'entrées et de fenêtres dans votre maison qui doivent être sécurisées.</h4>
									<p>Cela aidera à déterminer le nombre de capteurs dont vous aurez besoin.</p>
								</div>
							</li>
						</ol>
					</div>
				</div>
			</div>
		</section>
		<section class="imagebg height-50">
			<div class="background-image-holder"><img alt="bg" src="assets/images/landing-05.jpg"></div>
		</section>
		<section class="space--xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="process-1">
							<div class="process__item">
								<h4>Obtenez au moins un détecteur de fumée sur votre système et incluez une surveillance automatique.</h4>
								<p>Si vous éloignez de votre domicile, le détecteur de fumée peut économiser certains de vos effets personnels.</p>
							</div>
							<div class="process__item">
								<h4>Gardez toutes les portes de niveau principal contactées avec un périphérique pour indiquer au panneau principal qu'il a été ouvert.</h4>
								<p>Si vous avez plusieurs portes qui ne peuvent être atteintes avec le câblage, vous pouvez les protéger avec un détecteur de mouvement de couverture de zone.</p>
							</div>
							<div class="process__item">
								<h4>Envisager d'utiliser des détecteurs de mouvement dans les zones où plusieurs fenêtres sont accessibles depuis le niveau du sol.</h4>
								<p>Cela peut également réduire le coût total.</p>
							</div>
							<div class="process__item">
								<h4>Pensez à la façon dont votre sous-sol est adapté à vos options de sécurité.</h4>
								<p>Si vous avez un sous-sol fini, parfois un ajout sans fil à votre système peut être nécessaire pour protéger votre sous-sol, vos fenêtres, vos portes et votre plancher principal. En ayant un récepteur sans fil sur votre système, vous avez accès à des boutons-poussoirs clés qui peuvent armer et désarmer votre système à distance et envoyer des alertes d'urgence à votre système.</p>
							</div>
							<div class="process__item">
								<h4>Promenez-vous autour de votre domicile avant d'avoir un représentant de l'entreprise pour une proposition.&nbsp;</h4>
								<p>Recherchez les zones les plus accessibles et cachées pour déterminer les zones que vous souhaitez couvrir le plus. Recherchez un bon endroit pour votre clavier qui contrôlera votre système. Recherchez également un placard centralisé ou un sous-sol inachevé pour votre boîte principale d'électronique.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="imagebg space--sm text-center" data-gradient-bg="#8F48BD,#5448BD,#C70039,#BD48B1">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-md-6">
						<div class="cta">
							<h2>Solutions de Sécurité sur Mesure.</h2><a class="btn btn--primary type--uppercase" href="anti-intrusion.php"><span class="btn__text">Découvrir les produits de Système Alarme<br /></span><span class="label">Nouveau</span></a>
						</div>
					</div>
				</div>
			</div>
		</section>
<?php
	getFooter("dark");
	mysqli_close($con);
?>
		<a class="back-to-top inner-link" data-scroll-class="100vh:active" href="#start"><i class="stack-interface stack-up-open-big"></i></a>
	</div><?php getJSCalls(["assets/old/js/parallax.js","https://cdnjs.cloudflare.com/ajax/libs/granim/2.0.0/granim.min.js","assets/old/js/jquery.steps.min.js","assets/old/js/smooth-scroll.min.js","assets/old/js/scripts.js"]); ?>
	<script async type="text/javascript" src="https://www.googletagmanager.com/gtag/js?id=UA-135626600-1"></script>
	<script async type="text/javascript">
		window.dataLayer= window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-135626600-1');
	</script>
</body>
</html>