<?php
	require_once "assets/config.php";
	include 'cart.php';
	$cart= new Cart;
	echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
	getHead(["color"=>"#1A569F","title"=>trans("Terms of Use"),
			"icon"=>"/assets/images/logo-icon-dark.png",
			"css"=>["https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css","assets/old/css/stack-interface.css","assets/old/css/theme.css"],
			"js"=>["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"]]);
	echo "<body data-smooth-scroll-offset=\"77\">\n";
	getPreloader("public");
	getNavbar("simple", $cart->total_items());
?>

	<a id="start"></a>
	<div class="main-container">
		<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h1>Conditions d'Utilisation</h1>
						<ol class="breadcrumbs">
							<li>
								<a href="index.php">Accueil</a>
							</li>
							<li>Conditions d'Utilisation</li>
						</ol>
						<hr>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-15">
						<article>
							<span class="h4">Dernière mise à jour: (28/07/2017)</span>
							<p>Veuillez lire attentivement ces Conditions d'Utilisation avant d'utiliser le site Web SOTEKAM exploité par notre société.</p>
							<p>Votre accès et votre utilisation du Service sont conditionnés par votre acceptation et votre conformité avec ces conditions.<br />
							Ces conditions s'appliquent à tous les visiteurs, utilisateurs et autres personnes qui ont accès ou utilisent le Service.</p>
							<h5>INFORMATIONS À PROPOS DE NOUS</h5>
							<p>www.sotekam.tn est un site géré par SOTEKAM. Nous sommes enregistrés en Tunisie sous le numéro de société +216 70 035 349 et avons notre siège social et notre adresse commerciale principale à<br />
							CITÉ ICHBILIA, route de Tunis - Kairouan 3140. Nous sommes une société à responsabilité limitée.</p>
							<h5>ACCÈS À NOTRE SITE</h5>
							<p>L'accès à notre site est autorisé sur une base temporaire, et nous nous réservons le droit de retirer ou de modifier le service que nous fournissons sur notre site sans préavis (voir ci-dessous). Nous ne serons pas responsables si, pour une raison quelconque, notre site n'est pas disponible à tout moment ou pour toute période. Lorsque vous utilisez notre site, vous devez respecter les dispositions de notre politique d'utilisation acceptable.</p>
							<h5>UTILISATION ACCEPTABLE</h5>
							<p>Vous ne devez pas utiliser notre site de manière discriminatoire, obscène, pornographique, diffamatoire, susceptible de haine raciale, illégale, illégale, frauduleuse ou nuisible.<br />
							Vous ne devez pas utiliser notre site pour le préjudice, y compris, et sans s'y limiter, les virus informatiques, les chevaux de Troie, les données corrompues ou d'autres logiciels malveillants.</p>
							<h5>DROITS DE PROPRIÉTÉ INTELLECTUELLE</h5>
							<p>Vous pouvez imprimer une copie et télécharger des extraits de toutes les pages de notre site pour votre référence personnelle et vous pouvez attirer l'attention d'autres personnes au sein de votre organisation sur les documents publiés sur notre site.<br />
							Vous ne devez pas modifier le papier ou les copies numériques de tout matériel que vous avez imprimé ou téléchargé de quelque manière que ce soit, et vous ne devez utiliser aucune illustration, photographie, séquence vidéo ou audio ou aucun graphique séparément de tout texte qui l'accompagne.<br />
							Notre statut (et celui de tous les contributeurs identifiés) comme auteurs de matériel sur notre site doit toujours être reconnu.<br />
							Vous ne devez utiliser aucune partie du matériel sur notre site à des fins commerciales sans obtenir une licence de notre part ou de nos concédants.<br />
							Si vous imprimez, copiez ou téléchargez une partie de notre site en violation de ces conditions d'utilisation, votre droit d'utiliser notre site cessera immédiatement et vous devez, à notre choix, retourner ou détruire tout exemplaire des documents que vous avez faits.</p>
							<h5>CONFIANCE SUR L'INFORMATION POSTÉE</h5>
							<p>Les commentaires et les autres documents publiés sur notre site ne sont pas destinés à constituer des conseils sur lesquels dépendre.<br />
							Nous déclinons donc toute responsabilité et responsabilité découlant de toute confiance accordée à ces matériaux par un visiteur de notre site ou par toute personne qui peut être informée de l'un de ses contenus.</p>
							<h5>CONDITIONS DE VENTE</h5>
							<div class="boxed bg--secondary boxed--lg boxed--border">
								<p>Les présentes conditions générales de vente s'appliquent à toutes les ventes de produits, matériels et équipements commercialisés en Tunisie auprès des professionnels par SOTEKAM.<br />
								Toute commande de produits, matériels et équipements implique l'acceptation sans réserve par l’acheteur et son adhésion pleine et entière aux présentes conditions générales de vente qui prévalent sur tout autre document de l'acheteur, et notamment sur toutes conditions générales d'achat.<br />
								Tout autre document, notamment les catalogues, les prospectus, les publicités et les notices, n’a qu'une valeur informative et indicative, non contractuelle.<br />
								<br />
								<b>1- OUVERTURE D’UN COMPTE CLIENT:</b><br />
								Toute première commande doit être précédée de l'ouverture dans les livres de SOTEKAM, d'un compte au nom de l’entité juridique qui envisage de passer la commande. Cette ouverture est réalisée par SOTEKAM après réception par elle des informations nécessaires.<br />
								<b>2- COMMANDES:</b><br />
								Les commandes peuvent être adressées à SOTEKAM par écrit sous toute forme, ainsi que par téléphone à condition de comporter toutes les informations nécessaires à sa bonne exécution et spécialement, les références des produits commandés, leur quantité et les modalités de livraison souhaitées. Pour être réputée valable, toute commande orale devra être confirmée par écrit. En cas de refus par l’acheteur des termes de paiements proposés par SOTEKAM, et de son incapacité à proposer une garantie financière suffisante, SOTEKAM pourra refuser d'honorer la (les) commande(s) passée(s) et de livrer la marchandise concernée, sans que l’acheteur puisse arguer d'un refus de vente justifié, ou prétendre à une quelconque indemnité.<br />
								<b>3- TARIF/ PRIX:</b><br />
								TARIF : A une date donnée, le tarif s’applique de façon identique à tous les acheteurs. Celui-ci peut-être revu à tout moment en cours d'année et fera l'objet d'une information à destination de nos acheteurs. Toute modification tarifaire sera automatiquement applicable à la date indiquée sur le nouveau tarif.<br />
								PRIX: Nos produits sont vendus aux prix figurant au tarif en vigueur au jour de la passation de la commande par l’acheteur. Ils s'entendent toujours hors taxes.<br />
								<b>4- REGLEMENTS DES COMMANDES:</b><br />
								Toute première commande d’un acheteur est payable au comptant sans escompte, à la réception de la facture. Les commandes suivantes sont payables à la date convenue d'un commun accord entre les parties lors de l'ouverture du compte et qui figurera sur chaque facture, étant entendu que le délai de règlement convenu ne peut pas excéder 60 jours, à compter de la date d’émission de facture.<br />
								Les factures sont établies lors de la mise à la disposition de l’acheteur des produits commandés, elles sont payables au siège de SOTEKAM. Tout paiement s'impute en priorité sur les factures les plus anciennes.<br />
								<b>5- LIVRAISONS:</b><br />
								Les délais de livraison mentionnés sur la confirmation de commande émise par SOTEKAM ne sont donnés qu'à titre informatif et indicatif. SOTEKAM s’efforce de respecter les délais de livraison qu’elle indique. Il ne saurait toutefois s’analyser en une condition déterminante de la commande et leur inobservation ne peut donner lieu à aucune pénalité ou indemnité de quelque nature que ce soit, ni motiver l’annulation de la commande.<br />
								<b>6- RESERVE DE PROPRIETE:</b><br />
								Le transfert de propriété des produits commandés est suspendu jusqu'à paiement complet du prix de ceux-ci par l’acheteur, en principal et accessoires, même en cas d'octroi de délais de paiement. Toute clause contraire, notamment insérée dans les conditions générales d'achat de l’acheteur est réputée non écrite.:<br />
								De convention expresse, SOTEKAM pourra faire jouer les droits qu'elle détient au titre de la présente clause de réserve de propriété, pour l'une quelconque de ses créances, sur la totalité de ses produits en possession de l’acheteur.:<br />
								<b>7- GARANTIE:</b><br />
								La garantie constructeur ne couvre pas:<br />
								- Les accessoires vendus avec les produits : alimentations, connectiques.<br />
								- Toute mauvaise manipulation.<br />
								En cas de produit livré par un transporteur:<br />
								Le client doit examiner soigneusement les produits à la livraison.<br />
								En cas de doute, le client doit apposer les réserves nécessaires sur le bon de livraison.<br />
								En cas d’anomalie (Rayures, produits non-conformes…) le client doit écrire et signer le motif de son refus sur le bon de livraison, la marchandise sera ramenée<br />
								et relivree dans les 48 heures si le produit est disponible.<br />
								- Il est recommandé au client de conserver l’emballage d’origine durant toute la période de garantie.<br />
								- Aucune réclamation concernant ces points ne sera prise en considération au-delà de 48 heures.<br />
								En cas de produit emporté par le client, en présence du commercial, le client doit vérifier:<br />
								- L’état général du produit.<br />
								- La présence de tous les accessoires.<br />
								- La présence des manuels d’utilisations et des logiciels.<br />
								- Il est recommandé au client de conserver l’emballage d’origine durant toute la période de garantie<br />
								<b>8- ATTRIBUTION DE JURIDICTION:</b><br />
								Pour l'application des présentes conditions générales de vente, des contrats de vente et de leurs suites, élection de domicile est faite par SOTEKAM, à son siège social. Tout différend au sujet de l'application des présentes conditions générales de vente et de leur interprétation, de leur exécution et des contrats de vente conclus par SOTEKAM ou au paiement du prix, sera porté devant les tribunaux de Kairouan. Quel que soit le lieu de la commande, de la livraison, et du paiement et le mode de paiement, et même en cas d'appel en garantie ou de pluralité.<br />
								L'attribution de compétence est générale et s'applique, qu'il s'agisse d'une demande principale, d'une demande incidente, d'une action au fond ou d'un référé.<br />
								En outre, en cas d'action judiciaire ou toute autre action en recouvrement de créances par SOTEKAM, les frais de sommation, de justice, ainsi que les honoraires d'avocat et d'huissier, et tous les frais annexes seront à la charge de l’acheteur fautif, ainsi que les frais liés ou découlant du non-respect par le client des conditions de paiement ou de livraison de la commande considérée.<br />
								<b>9- RENONCIATION:</b><br />
								Le fait pour SOTEKAM de ne pas se prévaloir à un moment donné de l'une quelconque des clauses des présentes, ne peut valoir renonciation à se prévaloir ultérieurement de ces mêmes clauses.</p>
							</div>
							<h5>NOTRE SITE CHANGES REGULIEREMENT</h5>
							<p>Nous avons l'intention de mettre à jour régulièrement notre site et de modifier le contenu à tout moment. Si le besoin se présente, nous pouvons suspendre l'accès à notre site, ou le fermer indéfiniment. L'un des documents sur notre site peut être périmé à tout moment, et nous ne sommes pas tenus de mettre à jour ce matériel.</p>
							<h5>CONTACTEZ-NOUS</h5>
							<p>Si vous avez des questions sur ces conditions, <a href="contact-us.php">contactez-nous</a>.</p>
						</article>
					</div>
				</div>
			</div>
		</section>
<?php
	getFooter("dark");
	mysqli_close($con);
?>
		<a class="back-to-top inner-link" data-scroll-class="100vh:active" href="#start"><i class="stack-interface stack-up-open-big"></i></a>
	</div><?php getJSCalls(["assets/old/js/smooth-scroll.min.js","assets/old/js/scripts.js","assets/old/js/typed.min.js"]); ?>
	<script async type="text/javascript" src="https://www.googletagmanager.com/gtag/js?id=UA-135626600-1"></script>
	<script async type="text/javascript">
		window.dataLayer= window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-135626600-1');
	</script>
</body>
</html>