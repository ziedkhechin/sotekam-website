<?php
	require_once "assets/config.php";
	include 'cart.php';
	$cart= new Cart;
	echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
	getHead(["color"=>"#1A569F","title"=>trans("Our Team"),
			"icon"=>"/assets/images/logo-icon-dark.png",
			"css"=>["https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css","assets/old/css/stack-interface.css","assets/old/css/theme.css"],
			"js"=>["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"]]);
	echo "<body data-smooth-scroll-offset=\"77\">\n";
	getPreloader("public");
	getNavbar("transp", $cart->total_items());
?>
	<a id="start"></a>
	<div class="main-container">
		<section class="text-center imagebg space--lg" data-overlay="6">
			<div class="background-image-holder"><img alt="background" src="assets/images/cowork-01.jpg"></div>
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-md-6">
						<h1>L'Équipe des Services Techniques</h1>
						<p class="lead">assurer la rapidité, efficacité et conseils.</p>
					</div>
				</div>
			</div>
		</section>
		<section class="space--xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<h4>NOTRE ÉQUIPE TECHNIQUE</h4>
						<p class="lead">Notre équipe est composée de personnes spécialistes en sécurité, expérimentées dans le domaine, ayant reçu une formation spécifique
						afin de pouvoir répondre à toutes les exigences et circonstances.<br />
						Nous sommes présents sur&nbsp;le centre de la république tunisienne.</p>
						<p>Nous utilisons des produits soigneusement choisis pour leur efficacité et durabilité.<br />
						Les collaborateurs de SOTEKAM ont une forte expérience et une maitrise de métier.<br />
						Nos prestations s’étendent également sur la géolocalisation par GPS, le pointage et contrôle d’accès et installation d’alarme et vidéosurveillance...</p>
						<p>N'hésitez pas à nous appeler au +216 70 035 349 ou par simple demande via notre <a href="contact-us.php">formulaire de contact</a>.</p>
					</div>
					<div class="col-sm-6">
						<div class="boxed boxed--lg boxed--border bg--secondary">
							<img alt="image" class="border--round" src="assets/images/cowork-02.jpg">
							<p><b>Les spécialités de SOTEKAM</b> comprennent la conception, l'écriture de spécifications, la fabrication, l'intégration, l'installation,
							le service après-vente, la maintenance et la réparation.<br />Nous sommes ici pour prendre votre sécurité au prochain niveau.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="space--xs text-center bg--secondary">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-md-6">
						<div class="cta">
							<h2>Solutions de Sécurité sur Mesure.</h2><a class="btn btn--primary type--uppercase" href="all-CCTV-products.php"><span class="btn__text">Découvrir les produits CCTV<br /></span><span class="label">NEW</span></a>
						</div>
					</div>
				</div>
			</div>
		</section>
<?php
	getFooter("white");
	mysqli_close($con);
?>
		<a class="back-to-top inner-link" data-scroll-class="100vh:active" href="#start"><i class="stack-interface stack-up-open-big"></i></a>
	</div><?php getJSCalls(["assets/old/js/jquery.steps.min.js","assets/old/js/smooth-scroll.min.js","assets/old/js/scripts.js"]); ?>
	<script async type="text/javascript" src="https://www.googletagmanager.com/gtag/js?id=UA-135626600-1"></script>
	<script async type="text/javascript">
		window.dataLayer= window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-135626600-1');
	</script>
</body>
</html>