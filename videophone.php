<?php
	require_once "assets/config.php";
	include 'cart.php';
	$cart= new Cart;
	echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
	getHead(["color"=>"#1A569F","title"=>trans("Videophone"),
			"keywords"=>"Vidéo Surveillance,CCTV,Système Alarme,Détection d’Incendie,Télédistribution,Pointeuse,Contrôle d'accés,Videophone,Porte Automatique,GPS,Accessoires,anti-intrusion,anti-incendie,caméra de surveillance,contrôle d'accès,géolocalisation,sécurité,sécurité maison,tunisie,tunis",
			"icon"=>"/assets/images/logo-icon-dark.png",
			"css"=>["https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.min.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css","assets/old/css/lightbox.min.css","assets/old/css/stack-interface.css","assets/old/css/theme.css"],
			"js"=>["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"]]);
	echo "<body data-smooth-scroll-offset=\"77\">\n";
	getPreloader("public");
	getNavbar("simple", $cart->total_items());
?>
	<a id="start"></a>
	<div class="main-container">
		<section class="switchable switchable--switch space--xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="height-50 imagebg border--round" data-overlay="4">
							<div class="background-image-holder"><img alt="background" src="assets/images/hero-06.jpg"></div>
							<div class="pos-vertical-center col-sm-6 col-md-5 col-md-offset-6">
								<h3>Réalisez les meilleures affaires du moment en acquérant chez SOTEKAM!</h3>
								<p class="lead">À noter que tout achat d’équipement de sécurité effectué chez le géant français du commerce en ligne vous réserve d’innombrables avantages.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="space--sm">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="masonry">
							<div class="masonry-filter-container">
								<span>Catégorie:</span>
								<div class="masonry-filter-holder">
									<div class="masonry__filters" data-filter-all-text="Toute Catégories"></div>
								</div>
							</div>
							<div class="row">
								<div class="masonry__container">
<?php
			$r= mysqli_query($con, "SELECT id,code,title,subcategory,pic1,sp,vat FROM products WHERE category='Videophone' AND visibility='on'");
			if (mysqli_num_rows($r)==0)
				echo "<script>window.location.replace('index.php');</script>";
			else
				while ($row= mysqli_fetch_array($r))
				{
?>
									<div class="masonry__item col-sm-4" data-masonry-filter="<?php echo $row['subcategory'] ?>">
										<div class="product">
											<a href="product.php?code=<?php echo $row['id'] ?>"><img alt="Image" src="assets/images/products/<?php echo $row['pic1'] ?>"></a>
											<a class="block" href="product.php?code=<?php echo $row['id'] ?>">
											<div><span><?php echo $row['title'] ?></span> <h5><?php echo $row['code'] ?></h5></div>
											<div><span class="h4 inline-block"><?php echo ($row['sp']!=0)? (float)$row['sp']*(1+$row['vat']/100)." TND" : "Sur demande"; ?></span></div></a>
										</div>
									</div>
<?php
				}
?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="text-center cta cta-4 space--xxs border--bottom imagebg" data-gradient-bg="#8F48BD,#5448BD,#C70039,#BD48B1">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<span class="label label--inline">Important!</span> <span>Nous sommes fiers de l'excellent service à la clientèle que nous fournissons. <a href="contact-us.php">Contactez-nous</a> aujourd'hui et nous vous aiderons dans votre requête.</span>
					</div>
				</div>
			</div>
		</section>
<?php
	getFooter("dark");
	mysqli_close($con);
?>
		<a class="back-to-top inner-link" data-scroll-class="100vh:active" href="#start"><i class="stack-interface stack-up-open-big"></i></a>
	</div><?php getJSCalls(["https://cdnjs.cloudflare.com/ajax/libs/granim/2.0.0/granim.min.js","https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.0/flickity.pkgd.min.js","https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js","assets/old/js/smooth-scroll.min.js","assets/old/js/scripts.js"]); ?>
	<script async type="text/javascript" src="https://www.googletagmanager.com/gtag/js?id=UA-135626600-1"></script>
	<script async type="text/javascript">
		window.dataLayer= window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-135626600-1');
	</script>
</body>
</html>