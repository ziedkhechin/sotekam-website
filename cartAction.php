<?php
	include 'assets/config.php';
	include 'cart.php';
	$cart= new Cart;

	if (isset($_REQUEST['action']) && !empty($_REQUEST['action']))
	{
		if ($_REQUEST['action']=='addToCart' && !empty($_REQUEST['id']) && !empty($_REQUEST['qte']))
		{
			$productID= $_REQUEST['id'];
			$query= $db->query("SELECT * FROM products WHERE id=".trim(mysqli_real_escape_string($con, $_REQUEST['id'])));
			$row= $query->fetch_assoc();
			$itemData= array(
				'id'=> $row['id'],
				'model'=> $row['code'],
				'name'=> $row['title'],
				'image'=> "".$row['pic1'],
				'price'=> $row['sp']*(1+$row['vat']/100),
				'qty'=> trim(mysqli_real_escape_string($con, $_REQUEST['qte']))
			);
			$insertItem= $cart->insert($itemData);
			$redirectLoc= $insertItem ? 'shop-cart.php' : 'index.php';
			header ("Location: ".$redirectLoc);
		}
		elseif ($_REQUEST['action']=='updateCartItem' && !empty($_REQUEST['id']))
		{
			$itemData= array(
				'rowid'=> $_REQUEST['id'],
				'qty'=> $_REQUEST['qty']
			);
			$updateItem= $cart->update($itemData);
			echo $updateItem ? 'ok' : 'err';
			die;
		}
		elseif ($_REQUEST['action']=='removeCartItem' && !empty($_REQUEST['id']))
		{
			$deleteItem= $cart->remove($_REQUEST['id']);
			header ("Location: shop-cart.php");
		}
		elseif ($_REQUEST['action']=='removeAllCartItems')
		{
			$cartItems= $cart->contents();
			foreach($cartItems as $item)
			{
				$deleteItem= $cart->remove(md5($item['id']));
			}
			header ("Location: shop-cart.php");
		}
		else
		{
			header ("Location: index.php");
		}
	}
	else
	{
		header ("Location: index.php");
		exit;
	}