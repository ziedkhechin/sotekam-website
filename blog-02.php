<?php
	require_once "assets/config.php";
	include 'cart.php';
	$cart= new Cart;
	echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
	getHead(["color"=>"#1A569F","title"=>"Découvrez les points faibles de votre maison et comment les protéger",
			"icon"=>"/assets/images/logo-icon-dark.png",
			"css"=>["https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.min.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css","assets/old/css/stack-interface.css","assets/old/css/theme.css"],
			"js"=>["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"]]);
	echo "<body class=\"boxed-layout\">\n";
	getPreloader("public");
	getNavbar("transp", $cart->total_items());
?>
	<a id="start"></a>
	<div class="main-container">
		<section class="cover height-70 imagebg text-center slider slider--ken-burns" data-arrows="true" data-paging="true">
			<ul class="slides">
				<li class="imagebg" data-overlay="4">
					<div class="background-image-holder background--top"><img alt="background" src="assets/images/landing-10.jpg"></div>
					<div class="container pos-vertical-center">
						<div class="row">
							<div class="col-sm-12">
								<h1>Contrôle d'accès, système sécurisés de gestion d'accès</h1>
							</div>
						</div>
					</div>
				</li>
				<li class="imagebg" data-overlay="4">
					<div class="background-image-holder"><img alt="background" src="assets/images/landing-11.jpg"></div>
					<div class="container pos-vertical-center">
						<div class="row">
							<div class="col-sm-12">
								<h1>Contrôle d'accès, système sécurisés de gestion d'accès</h1>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</section>
		<section class="text-center">
			<div class="container">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
						<h2>DÉCOUVREZ LA GAMME COMPLÈTE DE NOS SOLUTIONS DE CONTRÔLE D'ACCÈS</h2>
						<p class="lead">Pour protéger vos bâtiments et vos locaux contre les actes de malveillance</p>
					</div>
				</div>
			</div>
		</section>
		<section class="switchable bg--dark space--xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-5">
						<ul class="accordion accordion-1 accordion--oneopen">
							<li class="active">
								<div class="accordion__title">
									<span class="h5"><p>Un système de contrôle d’accès</p></span>
								</div>
								<div class="accordion__content">
									<p class="lead">Permet d’organiser le flux de personnes et de gérer l’accessibilité des zones de façon sélective en restreignant par exemple les accès à des zones sensibles et/ou d’enregistrer les personnes et leurs déplacements.</p>
									<p class="lead">Nécessite la mise en place d’un ensemble de cartes ou de badges sécurisés, adossés à un dispositif de lecteurs associés de serrures résistantes et mécaniquement fiables et contrôlés par un logiciel accessible et convivial.</p>
									<p class="lead">Cette solution est plus souvent installée dans les entreprises que dans les résidences de particuliers.</p>
								</div>
							</li>
						</ul>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="video-cover border--round box-shadow">
							<div class="background-image-holder"><img alt="image" src="assets/images/landing-12.jpg"></div>
							<div class="video-play-icon"></div><iframe allowfullscreen="allowfullscreen" src="https://www.youtube.com/embed/XbcnhErxpG4"></iframe>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="imageblock switchable feature-large bg--secondary">
			<div class="imageblock__content col-md-4 col-sm-3 pos-right">
				<div class="background-image-holder"><img alt="image" src="assets/images/landing-13.jpg"></div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-7 col-sm-6">
						<h2>Avec le système de contrôle d'accès de SOTEKAM,</h2>
						<p class="lead">vous contrôlez et sécurisez les accès aux bâtiments de votre entreprise en toute simplicité. En effet, il permet de définir des droits personnalisés en fonction des zones du
						bâtiment, du profil de l'utilisateurs et de l'horaire afin de vérifier que seules les personnes autorisées accèdent à une zone.</p>
						<div class="feature-large__group">
							<div class="row">
								<div class="col-sm-6">
									<div class="feature feature-6">
										<i class="icon icon--sm icon-Security-Block color--primary"></i>
										<h5>Blocage de sécurité</h5>
										<p>Nier les étrangers d'accéder à une<br />zone spécifique</p>
									</div><!--end feature-->
								</div>
								<div class="col-sm-6">
									<div class="feature feature-6">
										<i class="icon icon--sm icon-Remote-Controll color--primary"></i>
										<h5>Intégration facile</h5>
										<p>Il est conçu pour tirer parti de vos données démographiques existantes des employés afin de réduire les étapes d'entrée manuelle et les chances d'erreurs de bureau.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="space--xs text-center bg--secondary">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-md-6">
						<div class="cta">
							<h2>Solutions de Sécurité sur Mesure.</h2><a class="btn btn--primary type--uppercase" href="pointer-access-control.php"><span class="btn__text">Découvrir les produits de Contrôle d'accés<br /></span><span class="label">Nouveau</span></a>
						</div>
					</div>
				</div>
			</div>
		</section>
<?php
	getFooter("dark");
	mysqli_close($con);
?>
		<a class="back-to-top inner-link" data-scroll-class="100vh:active" href="#start"><i class="stack-interface stack-up-open-big"></i></a>
	</div><?php getJSCalls(["assets/old/js/parallax.js","https://cdnjs.cloudflare.com/ajax/libs/granim/2.0.0/granim.min.js","https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.0/flickity.pkgd.min.js","assets/old/js/jquery.steps.min.js","assets/old/js/smooth-scroll.min.js","assets/old/js/scripts.js"]); ?>
	<script async type="text/javascript" src="https://www.googletagmanager.com/gtag/js?id=UA-135626600-1"></script>
	<script async type="text/javascript">
		window.dataLayer= window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-135626600-1');
	</script>
</body>
</html>