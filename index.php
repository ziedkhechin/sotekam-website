<?php
	require_once "assets/config.php";
	include 'cart.php';
	$cart= new Cart;
	echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
	getHead(["color"=>"#1A569F","title"=>trans("Sale and Installation of Security Equipment"),
			"keywords"=>"Vidéo Surveillance,CCTV,Système Alarme,Détection d’Incendie,Télédistribution,Pointeuse,Contrôle d'accés,Videophone,Porte Automatique,GPS,Accessoires,anti-intrusion,anti-incendie,caméra de surveillance,contrôle d'accès,géolocalisation,sécurité,sécurité maison,tunisie,tunis",
			"description"=>"SOTEKAM: Vente et Installation d'Equipements de Sécurité (Vidéo Surveillance, Anti-Intrusion, Détection d’Incendie, Télédistribution, Pointeuse & Contrôle d'accés, Videophone, Porte Automatique, GPS, Accessoires)",
			"icon"=>"/assets/images/logo-icon-dark.png",
			"css"=>["https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.min.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css","assets/old/css/stack-interface.css","assets/old/css/slider_style.css","assets/old/css/theme.css"],
			"js"=>["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"]]);
	echo "<body data-smooth-scroll-offset=\"77\">\n";
	getPreloader("public");
	getNavbar("transp", $cart->total_items());
?>
	<a id="start"></a>
	<div class="main-container">
		<section class="text-center imagebg space--lg" data-overlay="6">
			<div class="background-image-holder"><img alt="background" src="assets/images/landing-01.jpg"></div>
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h6 class="type--uppercase">Votre sécurité est notre priorité.</h6>
						<div class="typed-headline">
							<span class="h1 inline-block">Nous offrons</span>
							<span class="h1 inline-block typed-text typed-text--cursor" data-typed-strings="une structure organisée et expérimentée.,un stock important et varié.,un centre de transfert de compétences.,une assistance gratuite et continue.">Un centr</span>
						</div>
						<p class="lead">Solutions de Sécurité sur Mesure.</p>
						<a class="btn btn--primary type--uppercase inner-link" href="about.php"><span class="btn__text">Présentation de Société</span></a>
					</div>
				</div>
			</div>
		</section>
		<section class="text-center cta cta-4 space--xxs border--bottom imagebg" data-gradient-bg="#8F48BD,#5448BD,#C70039,#BD48B1">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<span class="label label--inline">Important!</span>
						<span>Nous sommes fiers de l'excellent service à la clientèle que nous fournissons.
						<a href="contact-us.php">Contactez-nous</a> aujourd'hui et nous vous aiderons dans votre requête.</span>
					</div>
				</div>
			</div>
		</section>
		<section class="bg--dark space--xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="feature feature-1">
							<a href="blog-01.php"><img alt="Image" src="assets/images/blog-01.jpg">
							<div class="feature__body boxed boxed--border">
								<h5>DÉCOUVREZ LES POINTS FAIBLES DE VOTRE MAISON ET LES PROTÉGER</h5></a>
								<p>Trouver des solutions pour sécuriser votre maison ou votre lieu de travail..</p><a href="blog-01">Lire la suite</a>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="feature feature-1">
							<a href="blog-02.php"><img alt="Image" src="assets/images/blog-02.jpg">
							<div class="feature__body boxed boxed--border">
								<h5>SÉCURISATION D'ACCÈS</h5></a>
								<p>Organiser le flux de personnes et de gérer l'accessibilité des zones de façon sélective en restreignant..</p>
								<a href="blog-02">Lire la suite</a>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="feature feature-1">
							<a href="blog-03.php"><img alt="Image" src="assets/images/blog-03.jpg">
							  <div class="feature__body boxed boxed--border">
								  <h5>PORTE AUTOMATIQUE COULISSANTE</h5></a>
					  			  <p>Installer une porte automatique qui s'intègre harmonieusement dans la devanture de votre maison ou lieu de travail..</p>
								  <a href="blog-03">Lire la suite</a><span class="label">Nouveau</span>
							  </div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="switchable imagebg switchable--switch parallax space--sm" data-overlay="5">
				<div class="background-image-holder"> <img alt="background" src="assets/images/hero-01.jpg"> </div>
				<div class="container">
					<div class="row">
						<div class="col-sm-6 col-md-5">
							<ul class="accordion accordion-2 accordion--oneopen">
								<li class="active">
									<div class="accordion__title"> <span class="h5"><p>Portail Coulissant ou à Battant</p></span> </div>
									<div class="accordion__content">
										<p class="lead"> Les modèles en aluminium font parties des portails les plus populaires.En effet, ils disposent d'avantages intéressants,
										notamment au niveau du design et du peu d'entretien nécessaire.
										SOTEKAM vous propose une large gamme de portails avec ouverture coulissante ou à double battants à petits prix. </p>
									</div>
								</li>
								<li>
									<div class="accordion__title"> <span class="h5"><p>Televisions</p></span> </div>
									<div class="accordion__content">
										<p class="lead"> SOTEKAM vous propose des téléviseurs de haute qualité, pour une utilisation normale ou pour l'affichage
										de caméras de surveillance. Ainsi que les Smart TVs.</p>
									</div>
								</li>
								<li>
									<div class="accordion__title"> <span class="h5"><p>Détection d'Incendie</p></span> </div>
									<div class="accordion__content">
										<p class="lead"> Avec un détecteur d'incendie, le départ du feu est repéré efficacement et permet aux pompiers d’intervenir rapidement pour évacuer
										une habitation et stopper le départ d'incendie. </p>
									</div>
								</li>
								<li>
									<div class="accordion__title"> <span class="h5"><p>Systèmes de Contrôle d'Accès</p></span> </div>
									<div class="accordion__content">
										<p class="lead"> Il permet de délimiter la frontière de la propriété à protéger et de double fonction de dissuasion et de retard. </p>
									</div>
								</li>
							</ul>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="slider box-shadow-wide border--round" data-arrows="true" data-paging="true" data-timing="2000">
								<ul class="slides">
									<li> <img alt="img" src="assets/images/work-01.jpg"> </li>
									<li> <img alt="img" src="assets/images/work-02.jpg"> </li>
									<li> <img alt="img" src="assets/images/work-03.jpg"> </li>
									<li> <img alt="img" src="assets/images/work-04.jpg"> </li>
									<li> <img alt="img" src="assets/images/work-05.jpg"> </li>
									<li> <img alt="img" src="assets/images/work-06.jpg"> </li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</section>
		<section class="text-center bg--secondary space--dm">
			<div class="container">
				<div class="row">
					<div class="col-sm-10 col-md-11">
						<h2>Nos Services</h2>
						<p class="lead">SOTEKAM est une entreprise de classe nationale fournissant des solutions de sécurité adaptées aux maisons, aux entreprises
						et aux organismes du secteur public en Tunisie. Nous sommes une organisation qui opère au niveau local et nous sommes engagés à fournir des
						solutions de pointe à tous nos clients. Notre force réside dans notre fiabilité et notre engagement envers le service à la clientèle.</p>
					</div>
				</div>
			</div>
		</section>
		<section class="space--sm bg--secondary">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-3">
						<div class="feature feature-6">
							<i class="icon color--primary icon-Remote-Controll icon--sm"></i>
							<h5>Contrôle d'Accès</h5>
							<p>Créez un environnement plus sécurisé pour votre entreprise avec le système de contrôle d'accès SOTEKAM. Avec plusieurs options disponibles,
							notre équipe de sécurité expérimentée aidera à concevoir un système pour répondre à vos besoins spécifiques.</p>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="feature feature-6">
							<i class="icon color--primary icon-Fire-Flame icon--sm"></i>
							<h5>Alarme Incendie</h5>
							<p>Si vous êtes à la recherche de conseils, d'évaluations, de formation ou d'une installation complète ou d'une prise en charge complète des
							services de secours, les experts de SOTEKAM peuvent vous fournir un service basé sur vos besoins individuels.</p>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="feature feature-6">
							<i class="icon color--primary icon-Security-Camera icon--sm"></i>
							<h5>Vidéosurveillance</h5>
							<p>Il se peut que vous ne puissiez pas garder votre entreprise à tout moment, mais les systèmes de vidéosurveillance de SOTEKAM peuvent vous
							donner la tranquillité d'esprit que votre entreprise est protégée.</p>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="feature feature-6">
							<i class="icon color--primary icon-Bell icon--sm"></i>
							<h5>Intruder Alarms</h5>
							<p>Protégez ce qui compte le plus pour votre entreprise avec un système d'alarme intrus entièrement intégré. SOTEKAM propose des solutions
							ersonnalisées pour vous assurer que vos locaux, stock ou équipement sont protégés 24/7.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="text-center imagebg space--xxs" data-gradient-bg="#8F48BD,#5448BD,#C70039,#BD48B1">
				<div class="container">
					<div class="row">
						<div class="col-sm-8 col-md-7">
							<div class="cta">
								<a class="btn btn--primary btn--lg type--uppercase" href="all-CCTV-products.php"><span class="btn__text">Découvrir les produits CCTV</span>
								<span class="label">Nouveau</span></a>
								<p class="lead"> Chaque achat de SOTEKAM est livré avec une garantie de 5 ans au maximum - avec une support gratuit. </p>
								<p class="type--fine-print">Pour plus d'informations,&nbsp;<a href="contact-us.php" class="inner-link">contactez-nous</a>.</p>
							</div>
						</div>
					</div>
				</div>
			</section>
		<div id="container">
			<div class="photobanner-wrap bg--dark">
				<div class="photobanner">
					<div class="small-photo"><a href="http://www.fdimatelec.com" target="_blank"><img class="first" height="53" src="assets/images/img%20(1).png" width="170"></a></div>
					<div class="small-photo"><a href="http://www.finecctv.com" target="_blank"><img height="53" src="assets/images/img%20(2).png" width="170"></a></div>
					<div class="small-photo"><a><img height="53" src="assets/images/img%20(3).png" width="170"></a></div>
					<div class="small-photo"><a href="http://eng.hitron.co.kr" target="_blank"><img height="53" src="assets/images/img%20(4).png" width="170"></a></div>
					<div class="small-photo"><a href="http://www.icantek.com/icantek" target="_blank"><img height="53" src="assets/images/img%20(5).png" width="170"></a></div>
					<div class="small-photo"><a href="http://www.vicon-security.com" target="_blank"><img height="53" src="assets/images/img%20(6).png" width="170"></a></div>
					<div class="small-photo"><a href="http://www.jvc.com" target="_blank"><img height="53" src="assets/images/img%20(7).png" width="170"></a></div>
					<div class="small-photo"><a href="http://www.panasonic.com" target="_blank"><img height="53" src="assets/images/img%20(8).png" width="170"></a></div>
					<div class="small-photo"><a href="http://www.probedigital.com" target="_blank"><img height="53" src="assets/images/img%20(9).png" width="170"></a></div>
					<div class="small-photo"><a href="http://www.samsung.com" target="_blank"><img height="53" src="assets/images/img%20(10).png" width="170"></a></div>
					<div class="small-photo"><a href="https://www.sony.com" target="_blank"><img height="53" src="assets/images/img%20(11).png" width="170"></a></div>
					<div class="small-photo"><a href="http://urmetgroup.tn" target="_blank"><img height="53" src="assets/images/img%20(12).png" width="170"></a></div>
					<div class="small-photo"><a href="http://www.v2elettronica.com" target="_blank"><img height="53" src="assets/images/img%20(13).png" width="170"></a></div>
					<div class="small-photo"><a href="http://www.vivotek.com" target="_blank"><img height="53" src="assets/images/img%20(14).png" width="170"></a></div>
					<div class="small-photo"><a href="http://www.yokis.com" target="_blank"><img height="53" src="assets/images/img%20(15).png" width="170"></a></div>
					<div class="small-photo"><a href="http://www.zavio.com" target="_blank"><img height="53" src="assets/images/img%20(16).png" width="170"></a></div>
					<div class="small-photo"><a href="https://www.acti.com" target="_blank"><img height="53" src="assets/images/img%20(17).png" width="170"></a></div>
					<div class="small-photo"><a><img height="53" src="assets/images/img%20(18).png" width="170"></a></div>
					<div class="small-photo"><a href="http://www.dlink.com" target="_blank"><img height="53" src="assets/images/img%20(19).png" width="170"></a></div>
					<div class="small-photo"><a href="http://www.elkron-securite.fr" target="_blank"><img height="53" src="assets/images/img%20(20).png" width="170"></a></div>
					<div class="small-photo"><a href="http://www.nextguvenlik.com.tr" target="_blank"><img height="53" src="assets/images/img%20(21).png" width="170"></a></div>
					<div class="small-photo"><a href="http://www.dahuasecurity.com" target="_blank"><img height="53" src="assets/images/img%20(22).png" width="170"></a></div>
					<div class="small-photo"><a href="http://www.fdimatelec.com" target="_blank"><img class="first" height="53" src="assets/images/img%20(1).png" width="170"></a></div>
					<div class="small-photo"><a href="http://www.finecctv.com" target="_blank"><img height="53" src="assets/images/img%20(2).png" width="170"></a></div>
					<div class="small-photo"><a><img height="53" src="assets/images/img%20(3).png" width="170"></a></div>
					<div class="small-photo"><a href="http://eng.hitron.co.kr" target="_blank"><img height="53" src="assets/images/img%20(4).png" width="170"></a></div>
					<div class="small-photo"><a href="http://www.icantek.com/icantek" target="_blank"><img height="53" src="assets/images/img%20(5).png" width="170"></a></div>
					<div class="small-photo"><a href="http://www.vicon-security.com" target="_blank"><img height="53" src="assets/images/img%20(6).png" width="170"></a></div>
					<div class="small-photo"><a href="http://www.jvc.com" target="_blank"><img height="53" src="assets/images/img%20(7).png" width="170"></a></div>
				</div>
			</div>
		</div>
<?php
	getFooter("dark");
	mysqli_close($con);
?>
		<a class="back-to-top inner-link" data-scroll-class="100vh:active" href="#start"><i class="fas fa-angle-up"></i></a>
	</div><?php getJSCalls(["https://cdnjs.cloudflare.com/ajax/libs/granim/2.0.0/granim.min.js","https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.0/flickity.pkgd.min.js","assets/old/js/parallax.js","assets/old/js/smooth-scroll.min.js","assets/old/js/scripts.js","assets/old/js/typed.min.js"]); ?>
	<script async type="text/javascript" src="https://www.googletagmanager.com/gtag/js?id=UA-135626600-1"></script>
	<script async type="text/javascript">
		window.dataLayer= window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-135626600-1');
	</script>
</body>
</html>