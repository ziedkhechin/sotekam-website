<?php
	require_once "../assets/config.php";
	if (!isset($_SESSION['username']))
	{
		header("Location: login.php?request_page=products");
		exit;
	}
	elseif ($_SESSION['state']=="inactive")
	{
		header("Location: lockscreen.php?request_page=products");
		exit;
	}
	elseif (!in_array($_SESSION['role'], ["Admin","Editor"]))
	{
		header("Location: /error.php?code=403");
		exit;
	}
	elseif (isset($_GET['change_visb']) && !empty($_GET['change_visb'])) // Changing the visibility of a product in the public website
	{
		$r= mysqli_query($con, "SELECT visibility FROM products WHERE id='".trim(mysqli_real_escape_string($con, $_GET['change_visb']))."'");
		$sql="UPDATE products SET visibility= '";
		switch (mysqli_fetch_assoc($r)['visibility'])
		{
			case 'on': $sql.="off"; break;
			case 'off': $sql.="on"; break;
		}
		mysqli_query($con, $sql."' WHERE id='".trim(mysqli_real_escape_string($con, $_GET['change_visb']))."'");
		exit;
	}
	elseif (isset($_GET['delete']) && !empty($_GET['delete'])) // Delete a product
	{
		mysqli_query($con, "DELETE FROM products WHERE id='".trim(mysqli_real_escape_string($con, $_GET['delete']))."'");
		header('Location: products.php');
		exit;
	}
	elseif ($_SERVER['REQUEST_METHOD']=="POST" && isset($_REQUEST['mode'], $_POST['code'], $_POST['category']) && in_array(trim($_REQUEST['mode']),["add","edit"]) && strlen($_POST['code'])<=33 && preg_match("/^.{3,22}$/", $_POST['category']) && (trim($_REQUEST['mode'])=="edit" && isset($_POST['id']) || trim($_REQUEST['mode'])=="add"))
	{
		$p['barcode']= (!empty($_POST['barcode']))? "'".trim(mysqli_real_escape_string($con, $_POST['barcode']))."'" : "NULL";
		$p['title']= (!empty($_POST['title']))? "'".trim(mysqli_real_escape_string($con, $_POST['title']))."'" : "NULL";
		$_POST['code']= "'".trim(mysqli_real_escape_string($con, $_POST['code']))."'";
		$p['brand']= (!empty($_POST['brand']))? trim(mysqli_real_escape_string($con, $_POST['brand'])) : "NULL";
		if ($p['brand']!="NULL" && !file_exists('../assets/images/products/'.$p['brand'])) mkdir('../assets/images/products/'.$p['brand']);
		$_POST['category']= "'".trim(mysqli_real_escape_string($con, $_POST['category']))."'";
		$p['subcategory']= (!empty($_POST['subcategory']))? "'".trim(mysqli_real_escape_string($con, $_POST['subcategory']))."'" : "NULL";
		$p['shortdescr']= (!empty($_POST['shortdescr']))? "'".trim(mysqli_real_escape_string($con, $_POST['shortdescr']))."'" : "NULL";
		$p['descr']= (!empty($_POST['descr']))? "'".trim(mysqli_real_escape_string($con, $_POST['descr']))."'" : "NULL";
		$p['pic1']= (!empty($_FILES['pic']['name'][0]))? "'".(($p['brand']!="NULL")? $p['brand']."/" : "").$_FILES['pic']['name'][0]."'" : "NULL";
		if (!empty($_FILES['pic']['tmp_name'][0]) && getimagesize($_FILES['pic']['tmp_name'][0]) && $_FILES['pic']['size'][0]<=500000) move_uploaded_file($_FILES['pic']["tmp_name"][0], "../assets/images/products/".(($p['brand']!="NULL")? $p['brand']."/":"").basename($_FILES["pic"]["name"][0]));
		$p['pic2']= (!empty($_FILES['pic']['name'][1]))? "'".(($p['brand']!="NULL")? $p['brand']."/" : "").$_FILES['pic']['name'][1]."'" : "NULL";
		if (!empty($_FILES['pic']['tmp_name'][1]) && getimagesize($_FILES['pic']['tmp_name'][1]) && $_FILES['pic']['size'][1]<=500000) move_uploaded_file($_FILES['pic']["tmp_name"][1], "../assets/images/products/".(($p['brand']!="NULL")? $p['brand']."/":"").basename($_FILES["pic"]["name"][1]));
		$p['pic3']= (!empty($_FILES['pic']['name'][2]))? "'".(($p['brand']!="NULL")? $p['brand']."/" : "").$_FILES['pic']['name'][2]."'" : "NULL";
		if (!empty($_FILES['pic']['tmp_name'][2]) && getimagesize($_FILES['pic']['tmp_name'][2]) && $_FILES['pic']['size'][2]<=500000) move_uploaded_file($_FILES['pic']["tmp_name"][2], "../assets/images/products/".(($p['brand']!="NULL")? $p['brand']."/":"").basename($_FILES["pic"]["name"][2]));
		$p['pic4']= (!empty($_FILES['pic']['name'][3]))? "'".(($p['brand']!="NULL")? $p['brand']."/" : "").$_FILES['pic']['name'][3]."'" : "NULL";
		if (!empty($_FILES['pic']['tmp_name'][3]) && getimagesize($_FILES['pic']['tmp_name'][3]) && $_FILES['pic']['size'][3]<=500000) move_uploaded_file($_FILES['pic']["tmp_name"][3], "../assets/images/products/".(($p['brand']!="NULL")? $p['brand']."/":"").basename($_FILES["pic"]["name"][3]));
		$p['pic5']= (!empty($_FILES['pic']['name'][4]))? "'".(($p['brand']!="NULL")? $p['brand']."/" : "").$_FILES['pic']['name'][4]."'" : "NULL";
		if (!empty($_FILES['pic']['tmp_name'][4]) && getimagesize($_FILES['pic']['tmp_name'][4]) && $_FILES['pic']['size'][4]<=500000) move_uploaded_file($_FILES['pic']["tmp_name"][4], "../assets/images/products/".(($p['brand']!="NULL")? $p['brand']."/":"").basename($_FILES["pic"]["name"][4]));
		if ($p['brand']!="NULL") $p['brand']= "'".$p['brand']."'";
		$p['webpage']= (!empty($_POST['webpage']))? "'".trim(mysqli_real_escape_string($con, $_POST['webpage']))."'" : "NULL";
		$p['pdf']= (!empty($_POST['pdf']))? "'".trim(mysqli_real_escape_string($con, $_POST['pdf']))."'" : "NULL";
		$p['unit']= (!empty($_POST['unit']))? "'".trim(mysqli_real_escape_string($con, $_POST['unit']))."'" : "DEFAULT";
		$p['qte']= (!empty($_POST['qte']))? trim(mysqli_real_escape_string($con, $_POST['qte'])) : "DEFAULT";
		$p['cost']= (!empty($_POST['cost']))? trim(mysqli_real_escape_string($con, $_POST['cost'])) : "DEFAULT";
		$p['margin']= (!empty($_POST['margin']))? trim(mysqli_real_escape_string($con, $_POST['margin'])) : "DEFAULT";
		$p['sp']= (!empty($_POST['sp']))? trim(mysqli_real_escape_string($con, $_POST['sp'])) : "DEFAULT";
		$p['vat']= (!empty($_POST['vat']))? trim(mysqli_real_escape_string($con, $_POST['vat'])) : "DEFAULT";
		if (trim($_REQUEST['mode'])=="edit") $p['rempic']= [trim($_POST['rempic'][0]),trim($_POST['rempic'][1]),trim($_POST['rempic'][2]),trim($_POST['rempic'][3]),trim($_POST['rempic'][4])];
		switch (trim($_REQUEST['mode']))
		{
			case "add":
				mysqli_query($con, "INSERT INTO products (barcode,code,title,brand,subcategory,category,shortdescr,descr,pic1,pic2,pic3,pic4,pic5,webpage,pdf,unit,qte,cost,margin,sp,vat)
				VALUES ({$p['barcode']},{$_POST['code']},{$p['title']},{$p['brand']},{$p['subcategory']},{$_POST['category']},{$p['shortdescr']},{$p['descr']},{$p['pic1']},{$p['pic2']},{$p['pic3']},{$p['pic4']},{$p['pic5']},{$p['webpage']},{$p['pdf']},{$p['unit']},{$p['qte']},{$p['cost']},{$p['margin']},{$p['sp']},{$p['vat']})");
				break;
			case "edit":
				mysqli_query($con, "UPDATE products SET barcode={$p['barcode']}, code={$_POST['code']}, title={$p['title']}, brand={$p['brand']}, subcategory={$p['subcategory']}, category={$_POST['category']}, shortdescr={$p['shortdescr']}, descr={$p['descr']}".(($p['pic1']!="NULL" || $p['rempic'][0]=="yes")? ", pic1={$p['pic1']}":"").(($p['pic2']!="NULL" || $p['rempic'][1]=="yes")? ", pic2={$p['pic2']}":"")
				.(($p['pic3']!="NULL" || $p['rempic'][2]=="yes")? ", pic3={$p['pic3']}":"").(($p['pic4']!="NULL" || $p['rempic'][3]=="yes")? ", pic4={$p['pic4']}":"").(($p['pic5']!="NULL" || $p['rempic'][4]=="yes")? ", pic5={$p['pic5']}":"").", webpage={$p['webpage']},  pdf={$p['pdf']}, unit={$p['unit']}, qte={$p['qte']}, cost={$p['cost']}, margin={$p['margin']}, sp={$p['sp']}, vat={$p['vat']} WHERE id=".trim(mysqli_real_escape_string($con, $_POST['id'])));
				break;
		}
		if ($_POST['category']=="'TV Receivers'")
			mysqli_query($con, "INSERT INTO receivers (code) VALUES ({$_POST['code']})");
		header('Location: products.php');
		exit;
	}
	else
	{
		echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
		getHead(["color"=>$_SESSION['color'],"title"=>trans("Products"),"icon"=>"/assets/images/logo-icon.png","css"=>["/assets/css/admin-modern.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css"]]);
?>
<body class="fixed-layout <?php echo $_SESSION['theme']."\">"; getPreloader(); ?>
	<div id="main-wrapper"><?php require "topbar"; require "left-sidebar"; ?>
		<div class="page-wrapper">
			<div class="container-fluid">
				<div class="row page-titles">
					<div class="col-md-5 align-self-center">
						<h4 class="text-themecolor"><?php echo trans("Products"); ?></h4>
					</div>
					<div class="col-md-7 align-self-center text-right">
						<div class="d-flex justify-content-end align-items-center">
							<ol class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="index.php"><?php echo trans("Home"); ?></a>
								</li>
								<li class="breadcrumb-item active"><?php echo trans("Products"); ?></li>
							</ol>
						</div>
					</div>
				</div>
<?php
		if (isset($_GET['modify']) && !empty($_GET['modify'])) // Modify a product
		{
			$res= mysqli_query($con, "SELECT * FROM products WHERE id='".trim(mysqli_real_escape_string($con, $_GET['modify']))."'");
			if (mysqli_num_rows($res)==0)
			{
				echo "<script type=\"text/javascript\">window.location.replace('products.php');</script>";
				exit;
			}
			$row= mysqli_fetch_assoc($res);
			
?>
				<div class="row">
					<div class="col-lg-12">
						<div class="card">
							<div class="card-body">
								<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
									<input type="hidden" name="mode" value="edit" />
									<input type="hidden" name="id" value="<?php echo $row['id']; ?>" />
									<input type="hidden" name="rempic[]" value="no" />
									<input type="hidden" name="rempic[]" value="no" />
									<input type="hidden" name="rempic[]" value="no" />
									<input type="hidden" name="rempic[]" value="no" />
									<input type="hidden" name="rempic[]" value="no" />
									<div class="form-body">
										<h3 class="card-title"><?php echo trans("Modify Product"); ?></h3><hr>
										<div class="row p-t-20">
											<div class="col-md-4"><div class="form-group">
												<label class="control-label" for="barcode"><?php echo trans("Barcode"); ?></label>
												<input id="barcode" class="form-control" value="<?php echo $row['barcode']; ?>" name="barcode" maxlength="17" type="number" autocomplete="off" autofocus="" />
											</div></div>
											<div class="col-md-4"><div class="form-group">
												<label class="control-label" for="code"><?php echo trans("Code"); ?></label>
												<input id="code" class="form-control" value="<?php echo $row['code']; ?>" name="code" maxlength="33" type="text" autocomplete="off" required="" />
											</div></div>
											<div class="col-md-4"><div class="form-group">
												<label class="control-label" for="title"><?php echo trans("Title"); ?></label>
												<input id="title" class="form-control" value="<?php echo $row['title']; ?>" name="title" maxlength="22" type="text" autocomplete="on" />
											</div></div>
										</div>
										<div class="row">
											<div class="col-md-4"><div class="form-group">
												<label class="control-label" for="brand"><?php echo trans("Brand"); ?></label>
												<input id="brand" class="form-control" value="<?php echo $row['brand']; ?>" name="brand" maxlength="22" type="text" autocomplete="on" />
											</div></div>
											<div class="col-md-4"><div class="form-group">
												<label class="control-label" for="category"><?php echo trans("Category"); ?></label>
												<input id="category" class="form-control" value="<?php echo $row['category']; ?>" name="category" maxlength="22" type="text" autocomplete="on" required="" pattern=".{3,22}" />
											</div></div>
											<div class="col-md-4"><div class="form-group">
												<label class="control-label" for="subcategory"><?php echo trans("Sub-category"); ?></label>
												<input id="subcategory" class="form-control" value="<?php echo $row['subcategory']; ?>" name="subcategory" type="text" autocomplete="off" />
											</div></div>
										</div>
										<div class="row">
											<div class="col-md-3"><div class="form-group">
												<label class="control-label" for="cost"><?php echo trans("Product Cost"); ?></label>
												<input id="cost" class="form-control" value="<?php echo $row['cost']; ?>"  name="cost" min="0" step="0.01" type="number" autocomplete="off" />
											</div></div>
											<div class="col-md-3"><div class="form-group">
												<label class="control-label" for="margin"><?php echo trans("Margin"); ?></label>
												<input id="margin" class="form-control" value="<?php echo $row['margin']; ?>"  name="margin" min="0" step="0.01" type="number" autocomplete="off" />
											</div></div>
											<div class="col-md-3"><div class="form-group">
												<label class="control-label" for="sp"><?php echo trans("Selling Price"); ?></label>
												<input id="sp" class="form-control" value="<?php echo $row['sp']; ?>" name="sp" min="0" step="0.01" type="number" autocomplete="off" />
											</div></div>
											<div class="col-md-3"><div class="form-group">
												<label class="control-label" for="vat"><?php echo trans("VAT"); ?> (%)</label>
												<input id="vat" class="form-control" value="<?php echo $row['vat']; ?>" name="vat" min="0" max="100" type="number" autocomplete="off" />
											</div></div>
										</div>
										<div class="row">
											<div class="col-md-4"><div class="form-group">
												<label class="control-label" for="webpage"><?php echo trans("Webpage"); ?></label>
												<input id="webpage" class="form-control" value="<?php echo $row['webpage']; ?>" name="webpage" type="url" autocomplete="off" />
											</div></div>
											<div class="col-md-4"><div class="form-group">
												<label class="control-label" for="pdf">PDF</label>
												<input id="pdf" class="form-control" value="<?php echo $row['pdf']; ?>" name="pdf" type="url" autocomplete="off" />
											</div></div>
											<div class="col-md-4"><div class="form-group">
												<label class="control-label" for="qte"><?php echo trans("Quantity"); ?></label>
												<input id="qte" class="form-control" value="<?php echo $row['qte']; ?>" name="qte" min="0" max="999999" type="number" autocomplete="off" />
											</div></div>
										</div>
										<div class="row">
											<div class="col-md-8"><div class="form-group">
												<label class="control-label" for="descr"><?php echo trans("Description"); ?></label>
												<textarea id="descr" class="form-control" name="descr" style="height: 85px;" autocomplete="off"><?php echo $row['descr']; ?></textarea>
											</div></div>
											<div class="col-md-4"><div class="form-group">
												<label class="control-label" for="shortdescr"><?php echo trans("Short Description"); ?></label>
												<input id="shortdescr" class="form-control" value="<?php echo $row['shortdescr']; ?>" name="shortdescr" type="text" autocomplete="off" />
											</div></div>
										</div>
										<div class="row">
											<style>.fupload{padding: 3px 10px; width: 100%;} @media (min-width: 767px){.fupload {width: 20%; padding: 10px; padding-bottom: 20px}}</style>
											<div class="fupload"><input type="file" class="dropify" id="pic0" name="pic[]" data-allowed-file-extensions="jpg png" data-max-file-size="500K"<?php echo (!empty($row['pic1']))? " data-default-file=\"/assets/images/products/{$row['pic1']}\"":" "; ?>/></div>
											<div class="fupload"><input type="file" class="dropify" id="pic1" name="pic[]" data-allowed-file-extensions="jpg png" data-max-file-size="500K"<?php echo (!empty($row['pic2']))? " data-default-file=\"/assets/images/products/{$row['pic2']}\"":" "; ?>/></div>
											<div class="fupload"><input type="file" class="dropify" id="pic2" name="pic[]" data-allowed-file-extensions="jpg png" data-max-file-size="500K"<?php echo (!empty($row['pic3']))? " data-default-file=\"/assets/images/products/{$row['pic3']}\"":" "; ?>/></div>
											<div class="fupload"><input type="file" class="dropify" id="pic3" name="pic[]" data-allowed-file-extensions="jpg png" data-max-file-size="500K"<?php echo (!empty($row['pic4']))? " data-default-file=\"/assets/images/products/{$row['pic4']}\"":" "; ?>/></div>
											<div class="fupload"><input type="file" class="dropify" id="pic4" name="pic[]" data-allowed-file-extensions="jpg png" data-max-file-size="500K"<?php echo (!empty($row['pic5']))? " data-default-file=\"/assets/images/products/{$row['pic5']}\"":" "; ?>/></div>
										</div>
										<div class="form-group row">
											<div class="col-sm-3">
												<label><?php echo trans('Select Unit'); ?></label>
												<select class="custom-select" name="unit"><option value="Piece"><?php echo trans('Piece'); ?></option><option value="Meter"><?php echo trans('Meter'); ?></option></select>
												<script type="text/javascript">document.querySelector('option[value="<?php echo $row['unit']; ?>"]').setAttribute('selected','');</script>
											</div>
										</div>
									</div>
									<div class="form-actions">
										<button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> <?php echo trans("Save"); ?></button>
										<button type="button" onclick="window.location.replace('products.php')" class="btn btn-inverse"><?php echo trans("Cancel"); ?></button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
<?php
		}
		else
		{
?>
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
								<div class="modal fade in" id="add-product" role="dialog" style="display: none;" tabindex="-1">
									<div class="modal-dialog">
										<form class="modal-content" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
											<input type="hidden" name="mode" value="add" />
											<div class="modal-header">
												<h4 class="modal-title"><?php echo trans("Add New Product"); ?></h4><button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
											</div>
											<div class="modal-body">
												<style>.col-md-6 {float: left;} .col-md-12 {clear: both;}</style>
												<div class="form-group">
<div class="col-md-6 m-b-20"><input class="form-control" name="barcode" maxlength="17" placeholder="<?php echo trans("Barcode"); ?>" type="number" autocomplete="off" autofocus="" /></div>
<div class="col-md-6 m-b-20"><input class="form-control" name="code" maxlength="33" placeholder="<?php echo trans("Code"); ?>" type="text" autocomplete="off" required="" pattern="{,33}" /></div>
<div class="col-md-6 m-b-20"><input class="form-control" name="title" maxlength="22" placeholder="<?php echo trans("Title"); ?>" type="text" autocomplete="on" pattern="{,22}" /></div>
<div class="col-md-6 m-b-20"><input class="form-control" name="brand" maxlength="22" placeholder="<?php echo trans("Brand"); ?>" type="text" autocomplete="on" pattern="[A-Za-z]{,22}" /></div>
<div class="col-md-6 m-b-20"><input class="form-control" name="category" maxlength="22" placeholder="<?php echo trans("Category"); ?>" type="text" autocomplete="on" required="" pattern=".{3,22}" /></div>
<div class="col-md-6 m-b-20"><input class="form-control" name="subcategory" maxlength="22" placeholder="<?php echo trans("Sub-category"); ?>" type="text" autocomplete="on" pattern=".{3,22}" /></div>
<div class="col-md-12 m-b-20" style="height: 85px;"><textarea class="form-control" name="descr" placeholder="<?php echo trans("Description"); ?>" style="height: 85px;" autocomplete="off"></textarea></div>
<div class="col-md-6 m-b-20"><input class="form-control" name="shortdescr" placeholder="<?php echo trans("Short Description"); ?>" type="text" autocomplete="off" /></div>
<div class="col-md-6 m-b-20"><input class="form-control" name="qte" min="0" max="999999" placeholder="<?php echo trans("Quantity"); ?>" type="number" autocomplete="off" /></div>
<div class="col-md-6 m-b-20"><input class="form-control" name="webpage" placeholder="<?php echo trans("Webpage"); ?>" type="url" autocomplete="off" /></div>
<div class="col-md-6 m-b-20"><input class="form-control" name="pdf" placeholder="PDF" type="url" autocomplete="off" /></div>
<div class="col-md-6 m-b-20"><input class="form-control" name="cost" min="0" step="0.01" placeholder="<?php echo trans("Product Cost"); ?>" type="number" autocomplete="off" /></div>
<div class="col-md-6 m-b-20"><input class="form-control" name="margin" min="0" step="0.01" placeholder="<?php echo trans("Margin"); ?>" type="number" autocomplete="off" /></div>
<div class="col-md-6 m-b-20"><input class="form-control" name="sp" min="0" step="0.01" placeholder="<?php echo trans("Selling Price"); ?>" type="number" autocomplete="off" /></div>
<div class="col-md-6 m-b-20"><input class="form-control" name="vat" min="0" max="100" maxlength="" placeholder="<?php echo trans("VAT"); ?>" type="number" autocomplete="off" /></div>
<div class="col-md-12 m-b-20"><?php echo trans("Pictures"); ?><div class="row"><style>.fupload{padding: 3px 10px; width: 100%;} @media (min-width: 767px){.fupload {width: 20%; padding: 10px;}}</style>
<div class="fupload"><input type="file" class="dropify" name="pic[]" data-allowed-file-extensions="jpg png" data-max-file-size="500K" /></div>
<div class="fupload"><input type="file" class="dropify" name="pic[]" data-allowed-file-extensions="jpg png" data-max-file-size="500K" /></div>
<div class="fupload"><input type="file" class="dropify" name="pic[]" data-allowed-file-extensions="jpg png" data-max-file-size="500K" /></div>
<div class="fupload"><input type="file" class="dropify" name="pic[]" data-allowed-file-extensions="jpg png" data-max-file-size="500K" /></div>
<div class="fupload"><input type="file" class="dropify" name="pic[]" data-allowed-file-extensions="jpg png" data-max-file-size="500K" /></div></div></div>
<div class="col-sm-6"><label><?php echo trans('Select Unit'); ?></label><select class="custom-select" name="unit"><option value="Piece"><?php echo trans('Piece'); ?></option><option value="Meter"><?php echo trans('Meter'); ?></option></select></div>
												</div>
											</div>
											<div class="modal-footer">
												<button class="btn btn-info waves-effect" type="submit"><?php echo trans("Save"); ?></button>
												<button class="btn btn-default waves-effect" data-dismiss="modal" type="button"><?php echo trans("Cancel"); ?></button>
											</div>
										</form>
									</div>
								</div>
								<button class="btn btn-info btn-rounded m-t-10 mb-2 float-right" data-target="#add-product" data-toggle="modal" type="button"><?php echo trans("New Product"); ?></button>
<?php
			$r= mysqli_query($con, "SELECT * FROM products ORDER BY id DESC");
			if (!mysqli_num_rows($r))
				echo "<div class=\"alert alert-warning\" style=\"margin: 0; clear: both;\"><h3 class=\"text-warning\"><i class=\"far fa-frown\"></i> ".trans("Sorry")."</h3>".trans("We couldn't find any record in database!")."</div>\n";
			else
			{
?>
								<div class="table-responsive">
									<style>
										.table td, .table th {overflow: hidden; padding: 13px; text-overflow: ellipsis; font-size: 13px;}
										@media (min-width: 767px){table {table-layout: fixed; white-space: nowrap;}}
									</style>
									<table class="table table-bordered m-t-30">
										<colgroup><col span="5" style="width: 7%"><col span="2"><col style="width:44px"><col span="6" style="width:6%"><col style="width: 85px"></colgroup>
<?php
				echo "<thead><tr><th title=\"".trans("Barcode")."\">".trans("Barcode")."</th><th title=\"".trans("Code")."\">".trans("Code")."</th><th title=\"".trans("Title")."\">".trans("Title")."</th><th title=\"".trans("Brand")."\">".trans("Brand")."</th><th title=\"".trans("Category")."\">".trans("Category")."</th><th title=\"".trans("Short Description")."\">".trans("Short Description")."</th><th title=\"".trans("Description")."\">".trans("Description")."</th><th title=\"".trans("Webpage")."\">@</th><th title=\"".trans("Unit")."\">".trans("Unit")."</th><th title=\"".trans("Quantity")."\">".trans("Quantity")."</th><th title=\"".trans("Product Cost")."\">".trans("C")."</th><th title=\"".trans("Margin")."\">".trans("Margin")."</th><th title=\"".trans("Selling Price")."\">".trans("SP")."</th></th><th title=\"".trans("VAT")."\">".trans("VAT")."</th></th><th title=\"".trans("Options")."\">".trans("Options")."</th></tr></thead>";
				echo "<tfoot><tr><th title=\"".trans("Barcode")."\">".trans("Barcode")."</th><th title=\"".trans("Code")."\">".trans("Code")."</th><th title=\"".trans("Title")."\">".trans("Title")."</th><th title=\"".trans("Brand")."\">".trans("Brand")."</th><th title=\"".trans("Category")."\">".trans("Category")."</th><th title=\"".trans("Short Description")."\">".trans("Short Description")."</th><th title=\"".trans("Description")."\">".trans("Description")."</th><th title=\"".trans("Webpage")."\">@</th><th title=\"".trans("Unit")."\">".trans("Unit")."</th><th title=\"".trans("Quantity")."\">".trans("Quantity")."</th><th title=\"".trans("Product Cost")."\">".trans("C")."</th><th title=\"".trans("Margin")."\">".trans("Margin")."</th><th title=\"".trans("Selling Price")."\">".trans("SP")."</th></th><th title=\"".trans("VAT")."\">".trans("VAT")."</th></th><th title=\"".trans("Options")."\">".trans("Options")."</th></tr></tfoot><tbody>";
				while ($row= mysqli_fetch_assoc($r))
				{
					echo "<tr><td title=\"{$row['barcode']}\">{$row['barcode']}</td><td title=\"{$row['code']}\">{$row['code']}</td><td title=\"{$row['title']}\">{$row['title']}</td><td title=\"{$row['brand']}\">{$row['brand']}</td><td title=\"{$row['category']}\">{$row['category']}</td><td title=\"{$row['shortdescr']}\">{$row['shortdescr']}</td><td title=\"{$row['descr']}\">{$row['descr']}</td><td>"; echo (!empty($row['webpage']))? "<a target=\"_blank\" href=\"{$row['webpage']}\" class=\"fas fa-link\"></a>":""; echo "</td><td>".trans($row['unit'])."</td><td>{$row['qte']}</td><td>{$row['cost']}</td><td>{$row['margin']}</td><td>{$row['sp']}</td><td>{$row['vat']}%</td><td><a href=\"?modify={$row['id']}\" title=\"".trans("Edit")."\" class=\"fas fa-edit\"></a>&nbsp;<i title=\"".trans("Mark it invisible")."\" style=\"margin-left: 4px;cursor: pointer; display: ";echo ($row['visibility']=='on')?"unset":"none"; echo"\" onclick=\"this.style.display='none';$('#vsb{$row['id']}')[0].style.display='unset';change_visb({$row['id']});\" id=\"nvsb{$row['id']}\" class=\"fas fa-eye-slash\"></i>&nbsp;<i title=\"".trans("Mark it visible")."\" style=\"margin-right: 4px; cursor: pointer; display: ";echo ($row['visibility']=='off')?"unset":"none"; echo"\" onclick=\"$('#nvsb{$row['id']}')[0].style.display='unset';this.style.display='none';change_visb({$row['id']});\" id=\"vsb{$row['id']}\" class=\"fas fa-eye\" style=\"cursor: pointer\"></i>&nbsp;<a title=\"".trans("Remove")."\" style=\"color: red\" href=\"?delete={$row['id']}\" class=\"fas fa-trash-alt\"></a></td></tr>";
				}
?>
										</tbody>
									</table>
								</div>
							<button class="btn btn-info btn-rounded m-t-10 mb-2 float-right" data-target="#add-product" data-toggle="modal" type="button"><?php echo trans("New Product"); ?></button>
<?php
			}
?>
							</div>
						</div>
					</div>
				</div>
				<script type="text/javascript">
					function change_visb (id)
					{
						var xhr= new XMLHttpRequest();
						xhr.open("get", "<?php echo $_SERVER['PHP_SELF']; ?>?change_visb="+id, true);
						xhr.send();
					}
				</script>
<?php
		}
?>
			</div>
		</div><?php require "right-sidebar"; getFooter(); ?>
	</div><?php getJSCalls(["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js","/assets/js/popper.min.js","https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js","/assets/js/perfect-scrollbar.jquery.min.js","/assets/js/waves.js","/assets/js/sidebarmenu.js","/assets/js/custom.js","/assets/js/sessionTimeout.php","https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"]); ?>
	<script type="text/javascript">
		$('.dropify').dropify({
			messages:
			{
				'default': '<?php echo trans('Drag & drop a file here or click'); ?>',
				'replace': '<?php echo trans('Drag & drop or click to replace'); ?>',
				'remove': '<?php echo trans('Remove'); ?>',
				'error': '<?php echo trans('Ooops, something wrong happended.'); ?>'
			},
			tpl:
			{clearButton: '<button type="button" onclick="document.getElementsByName(\'rempic[]\')[Number(this.previousSibling.id[3])].value=\'yes\'" class="dropify-clear">{{ remove }}</button>'}
		});
	</script>
</body>
</html>
<?php
	}
	mysqli_close($con);
?>