<?php
	require_once "../assets/config.php";
	if (!isset($_SESSION['username']))
	{
		header("Location: login.php?request_page=customers");
		exit;
	}
	elseif ($_SESSION['state']=="inactive")
	{
		header("Location: lockscreen.php?request_page=customers");
		exit;
	}
	elseif (!in_array($_SESSION['role'], ["Admin","Editor"]))
	{
		header("Location: /error.php?code=403");
		exit;
	}
	elseif (in_array($_SESSION['role'], ["Admin","Editor"]) && isset($_GET['delete']))
	{
		mysqli_query($con, "DELETE FROM customers WHERE id=".trim(mysqli_real_escape_string($con, $_GET['delete'])));
		header('Location: customers.php');
		exit;
	}
	else
	{
		echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
		getHead(["color"=>$_SESSION['color'],"title"=>trans("Customers"),"icon"=>"/assets/images/logo-icon.png","css"=>["/assets/css/admin-modern.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css"]]);
?>
<body class="fixed-layout <?php echo $_SESSION['theme']." lock-nav\">"; getPreloader(); ?>
	<div id="main-wrapper"><?php require "topbar"; require "left-sidebar"; ?>
		<div class="page-wrapper">
			<div class="container-fluid">
				<div class="row page-titles">
					<div class="col-md-5 align-self-center">
						<h4 class="text-themecolor"><?php echo trans("Customers"); ?></h4>
					</div>
					<div class="col-md-7 align-self-center text-right">
						<div class="d-flex justify-content-end align-items-center">
							<ol class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="index.php"><?php echo trans("Home"); ?></a>
								</li>
								<li class="breadcrumb-item active"><?php echo trans("Customers"); ?></li>
							</ol>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
<?php
		$r= mysqli_query($con, "SELECT * FROM customers ORDER BY id DESC");
		if (!mysqli_num_rows($r))
			echo "<div class=\"alert alert-warning\" style=\"margin: 0; clear: both;\"><h3 class=\"text-warning\"><i class=\"far fa-frown\"></i> ".trans("Sorry")."</h3>".trans("We couldn't find any record in database!")."</div>\n";
		else
		{
?>
								<div class="table-responsive">
									<table class="table table-bordered m-t-30">
<?php
			echo "<thead><tr><th>".trans("ID")."</th><th>".trans("Name")."</th><th>".trans("Email")."</th></th><th>".trans("Phone")."</th></th><th>".trans("Company")."</th><th>".trans("Address")."</th><th>".trans("Options")."</th></tr></thead><tbody>";
			while ($row= mysqli_fetch_assoc($r))
			{
				$adr= $row['address1'].(($row['address2']!="")? ", ".$row['address2']:"").", ".$row['zip'].", ".$row['city'];
				echo "<tr><td>{$row['id']}</td><td>{$row['last_name']} {$row['first_name']}</td><td><a href=\"mailto:{$row['email']}\">{$row['email']}</a></td><td><a href=\"tel:{$row['phone']}\">{$row['phone']}</a></td><td>{$row['company']}</td><td><a href=\"https://www.google.com/maps/search/$adr\" target=\"_blank\">$adr</a></td><td><a href=\"".$_SERVER['PHP_SELF']."?delete=".$row['id']."\" class=\"btn btn-danger fas fa-trash\"></a></td></tr>";
			}
?>
										</tbody>
									</table>
								</div>
<?php
		}
?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><?php require "right-sidebar"; getFooter(); ?>
	</div><?php getJSCalls(["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js","/assets/js/popper.min.js","https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js","/assets/js/perfect-scrollbar.jquery.min.js","/assets/js/waves.js","/assets/js/sidebarmenu.js","/assets/js/custom.js","/assets/js/sessionTimeout.php"]); ?>
</body>
</html>
<?php
	}
	mysqli_close($con);
?>