<?php
	require_once "../assets/config.php";
	if (!isset($_SESSION['username']))
	{
		header("Location: login.php?request_page=orders");
		exit;
	}
	elseif ($_SESSION['state']=="inactive")
	{
		header("Location: lockscreen.php?request_page=orders");
		exit;
	}
	elseif (!in_array($_SESSION['role'], ["Admin","Editor"]))
	{
		header("Location: /error.php?code=403");
		exit;
	}
	elseif (isset($_REQUEST['action'], $_GET['id']))
	{
		switch ($_REQUEST['action'])
		{
			case 'delete': mysqli_query($con, "DELETE FROM orders WHERE id=".trim(mysqli_real_escape_string($con, $_GET['id']))); break;
			case 'confirm': mysqli_query($con, "UPDATE orders SET status='2' WHERE id=".trim(mysqli_real_escape_string($con, $_GET['id']))); break;
		}
		header('Location: orders.php');
		exit;
	}
	else
	{
		echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
		getHead(["color"=>$_SESSION['color'],"title"=>trans("Orders"),"icon"=>"/assets/images/logo-icon.png","css"=>["/assets/css/admin-modern.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css"]]);
?>
<body class="fixed-layout <?php echo $_SESSION['theme']." lock-nav\">"; getPreloader(); ?>
	<div id="main-wrapper"><?php require "topbar"; require "left-sidebar"; ?>
		<div class="page-wrapper">
			<div class="container-fluid">
				<div class="row page-titles">
					<div class="col-md-5 align-self-center">
						<h4 class="text-themecolor"><?php echo trans("Orders"); ?></h4>
					</div>
					<div class="col-md-7 align-self-center text-right">
						<div class="d-flex justify-content-end align-items-center">
							<ol class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="index.php"><?php echo trans("Home"); ?></a>
								</li>
								<li class="breadcrumb-item active"><?php echo trans("Orders"); ?></li>
							</ol>
						</div>
					</div>
				</div>
<?php
		if (isset($_GET['id']))
		{
			$r= mysqli_query($con, "SELECT * FROM orders JOIN customers ON customer_id=customers.id WHERE orders.id=".trim(mysqli_real_escape_string($con, $_GET['id'])));
			if (!mysqli_num_rows($r))
			{
				echo "<script>window.location.replace('orders.php');</script>";
				exit;
			}
			else
			{
				$row= mysqli_fetch_assoc($r);
				$r= mysqli_query ($con,"SELECT pic1, title, code, quantity, sp, vat FROM order_items JOIN products ON product_id=products.id WHERE order_items.order_id={$_GET['id']}");
				switch ($row['method'])
				{
					case NULL: $row['method']= trans("Not specified yet"); break;
					case '0': $row['method']= trans("Delivery only"); break;
					case '1': $row['method']= trans("Delivery with installation"); break;
				}
				switch ($row['status'])
				{
					case '0': $status= "<span class=\"label label-danger\">".trans("outstanding")."</span>"; break;
					case '1': $status= "<span class=\"label label-info\">".trans("confirmed")."</span>"; break;
					case '2': $status= "<span class=\"label label-success\">".trans("done")."</span>"; break;
				}
				$adr= $row['address1'].(($row['address2']!="")? ", ".$row['address2']:"").", ".$row['zip'].", ".$row['city'];
				if ($row['company']==NULL)
					$row['company']= trans("Not specified");
?>
				<style>
					.col-md-6 {margin-bottom: 20px}
					.table td {padding: 8px 12px}
					.panel > .table {margin: 0}
					.panel, .panel-default {background-color: white; border: 1.5px solid #dcdcdc;}
					.panel-heading {padding: 12px 15px; font-family: 'Open Sans',sans-serif; color: #4c4d5a; border-color: #dcdcdc; background: #f6f6f6;}
					.panel-body {padding: 15px}
					h4 {margin: 0}
				</style>
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading"><h4 class="panel-title"><i class="fa fa-shopping-cart"></i> <?php echo trans("Order Details"); ?></h4></div>
							<table class="table">
								<tbody>
									<tr><td style="width: 1%;"><button class="btn btn-info btn-xs" title="ID"><i class="fa fa-id-badge fa-fw"></i></button></td><td><?php echo $row["id"]; ?></td></tr>
									<tr><td><button class="btn btn-info btn-xs" title="<?php echo trans("Date Added"); ?>"><i class="fa fa-calendar fa-fw"></i></button></td><td><?php echo date("H:i:s d/m/Y", strtotime(htmlspecialchars($row['date']))); ?></td></tr>
									<tr><td><button class="btn btn-info btn-xs" title="<?php echo trans("Method"); ?>"><i class="fa fa-truck fa-fw"></i></button></td><td><?php echo $row["method"]; ?></td></tr>
									<tr><td><button class="btn btn-info btn-xs" title="<?php echo trans("Status"); ?>"><i class="fa fa-hourglass-start fa-fw"></i></button></td><td><?php echo $status; ?></td></tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading"><h4 class="panel-title"><i class="fa fa-user"></i> <?php echo trans("Customer Details"); ?></h4></div>
							<table class="table">
								<tbody>
									<tr><td style="width: 1%;"><button class="btn btn-info btn-xs" title="<?php echo trans("Name"); ?>"><i class="fa fa-user fa-fw"></i></button></td><td><?php echo $row["last_name"]." ".$row["first_name"]; ?></td></tr>
									<tr><td><button class="btn btn-info btn-xs" title="<?php echo trans("Company"); ?>"><i class="fa fa-building fa-fw"></i></button></td><td><?php echo $row["company"]; ?></td></tr>
									<tr><td><button class="btn btn-info btn-xs" title="<?php echo trans("Email"); ?>"><i class="far fa-envelope fa-fw"></i></button></td><td><?php echo "<a href=\"mailto:".$row["email"]."\">".$row["email"]."</a>"; ?></td></tr>
									<tr><td><button class="btn btn-info btn-xs" title="<?php echo trans("Phone"); ?>"><i class="fa fa-phone fa-fw"></i></button></td><td><?php echo "<a href=\"tel:".$row["phone"]."\">".$row["phone"]."</a>"; ?></td></tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading"><h4 class="panel-title"><i class="fa fa-info-circle"></i> <?php echo trans("Order"); ?></h4></div>
					<div class="panel-body">
						<table class="table table-bordered">
							<thead><tr><td class="text-left" style="width: 50%;"><?php echo trans("Address"); ?></td></tr></thead>
							<tbody><tr><td class="text-left"><?php echo $adr; ?></td></tr></tbody>
						</table>
						<table class="table table-bordered">
							<thead><tr><td class="text-left" width="22"><?php echo trans("Pictures"); ?></td><td class="text-left"><?php echo trans("Code"); ?></td><td class="text-left"><?php echo trans("Title"); ?></td><td class="text-right"><?php echo trans("Quantity"); ?></td><td class="text-right"><?php echo trans("Selling Price"); ?></td><td class="text-right"><?php echo trans("Total"); ?></td></tr></thead>
							<tbody>
<?php
				while ($row2= mysqli_fetch_assoc($r))
				{
?>
								<tr>
									<td class="text-center"><img src="/assets/images/products/<?php echo $row2["pic1"]; ?>" height="50"></img></td>
									<td class="text-left"><?php echo $row2["code"]; ?></td>
									<td class="text-left"><?php echo $row2["title"]; ?></td>
									<td class="text-right"><?php echo $row2["quantity"]; ?></td>
									<td class="text-right"><?php echo $row2["sp"]*(1+($row2["vat"]/100)); ?> TND</td>
									<td class="text-right"><?php echo $row2["quantity"]*$row2["sp"]*(1+($row2["vat"]/100)); ?> TND</td>
								</tr>
<?php
				}
?>
								<tr><td class="text-right" colspan="5"><?php echo trans("Fees"); ?></td><td class="text-right"><?php echo $row["fees"]; ?> TND</td></tr>
								<tr><td class="text-right" colspan="5"><?php echo trans("Total"); ?></td><td class="text-right"><?php echo $row["total"]; ?> TND</td></tr>
							</tbody>
						</table>
					</div>
				</div>
<?php
			}
		}
		else
		{
?>
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
<?php
			$r= mysqli_query($con, "SELECT orders.*,last_name,first_name FROM orders JOIN customers ON customer_id=customers.id ORDER BY date DESC");
			if (!mysqli_num_rows($r))
				echo "<div class=\"alert alert-warning\" style=\"margin: 0; clear: both;\"><h3 class=\"text-warning\"><i class=\"far fa-frown\"></i> ".trans("Sorry")."</h3>".trans("We couldn't find any record in database!")."</div>\n";
			else
			{
?>
								<div class="table-responsive">
									<table class="table table-bordered m-t-30">
<?php
				echo "<thead><tr><th>".trans("ID")."</th><th>".trans("Customer")."</th><th>".trans("Method")."</th></th><th>".trans("Fees")."</th></th><th>".trans("Total")."</th><th>".trans("Date Added")."</th><th>".trans("Status")."</th><th>".trans("Options")."</th></tr></thead><tbody>";
				while ($row= mysqli_fetch_assoc($r))
				{
					switch ($row['method'])
					{
						case NULL: $row['method']= trans("Not specified"); break;
						case '0': $row['method']= trans("Delivery only"); break;
						case '1': $row['method']= trans("Delivery with installation"); break;
					}
					switch ($row['status'])
					{
						case '0': $status= "<span class=\"label label-danger\">".trans("outstanding")."</span>"; break;
						case '1': $status= "<span class=\"label label-info\">".trans("confirmed")."</span>"; break;
						case '2': $status= "<span class=\"label label-success\">".trans("done")."</span>"; break;
					}
					echo "<tr><td>{$row['id']}</td><td><a href=\"customers.php\">{$row['last_name']} {$row['first_name']}</a></td><td>{$row['method']}</td><td>{$row['fees']} TND</td><td>{$row['total']} TND</td><td>".date("H:i:s d/m/Y", strtotime(htmlspecialchars($row['date'])))."</td><td>$status</td><td><a class=\"btn btn-info far fa-eye\" href=\"".$_SERVER['PHP_SELF']."?id=".$row['id']."\"></a> ".(($row['status']==1)? "<a href=\"".$_SERVER['PHP_SELF']."?action=confirm&id=".$row['id']."\" class=\"btn btn-success fas fa-check\"></a> ":"")."<a href=\"".$_SERVER['PHP_SELF']."?action=delete&id=".$row['id']."\" class=\"btn btn-danger fas fa-trash\"></a></td></tr>";
				}
?>
										</tbody>
									</table>
								</div>
<?php
			}
?>
							</div>
						</div>
					</div>
				</div>
<?php
		}
?>
			</div>
		</div><?php require "right-sidebar"; getFooter(); ?>
	</div><?php getJSCalls(["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js","/assets/js/popper.min.js","https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js","/assets/js/perfect-scrollbar.jquery.min.js","/assets/js/waves.js","/assets/js/sidebarmenu.js","/assets/js/custom.js","/assets/js/sessionTimeout.php"]); ?>
</body>
</html>
<?php
	}
	mysqli_close($con);
?>