<?php
	session_start();
	if (isset($_SESSION['language']))
		$lang=$_SESSION['language'];
	// $f= fopen(".log", "a");
	// fputs($f, "[".date('d-M-Y H:i:s')."] ".strtoupper($_SESSION['username'])." HAS BEEN SIGNED OUT FROM SESSION «".session_id()."».\n");
	// fclose($f);
	session_unset();
	session_destroy();
	session_start();
	if (isset($lang))
		$_SESSION['language']=$lang;
	print_r($_SESSION);
	header('Location: login.php');
	exit;