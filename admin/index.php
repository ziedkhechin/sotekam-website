<?php
	require_once "../assets/config.php";
	if (!isset($_SESSION['username']))
	{
		header("Location: login.php");
		exit;
	}
	elseif ($_SESSION['state']=="inactive")
	{
		header("Location: lockscreen.php");
		exit;
	}
	elseif ($_SESSION['role']=="User")
	{
		header("Location: receivers.php");
		exit;
	}
	if (isset($_REQUEST['action'], $_GET['theme'], $_GET['color']) && $_REQUEST['action']=='change_theme')
	{
		$_SESSION['theme']= $_GET['theme'];
		$_SESSION['color']= "#".$_GET['color'];
		mysqli_query($con, "UPDATE users SET theme='".mysqli_real_escape_string($con, $_GET['theme'])."', color='#".mysqli_real_escape_string($con, $_GET['color'])."' WHERE username='".$_SESSION['username']."'");
		exit;
	}
	else
	{
		echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
		getHead(["color"=>$_SESSION['color'],"title"=>trans("Dashboard"),"icon"=>"/assets/images/logo-icon.png","css"=>["/assets/css/admin-modern.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css"]]);
?>
<body class="fixed-layout <?php echo $_SESSION['theme']." lock-nav\">"; getPreloader(); ?>
	<div id="main-wrapper"><?php require "topbar"; require "left-sidebar"; ?>
		<div class="page-wrapper">
			<div class="container-fluid">
				<div class="row page-titles">
					<div class="col-md-5 align-self-center">
						<h4 class="text-themecolor"><?php echo trans("Dashboard"); ?></h4>
					</div>
					<div class="col-md-7 align-self-center text-right">
						<div class="d-flex justify-content-end align-items-center">
							<ol class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="index.php"><?php echo trans("Home"); ?></a>
								</li>
								<li class="breadcrumb-item active"><?php echo trans("Dashboard"); ?></li>
							</ol>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
                				<div id="embed-api-auth-container" style="display: none"></div>
                                <div id="chart-container"></div>
                                <div id="view-selector-container"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><?php require "right-sidebar"; getFooter(); ?>
	</div><?php getJSCalls(["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js","/assets/js/popper.min.js","https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js","/assets/js/perfect-scrollbar.jquery.min.js","/assets/js/waves.js","/assets/js/sidebarmenu.js","/assets/js/custom.js","/assets/js/sessionTimeout.php"]); ?>
	<script type="text/javascript">
		(function(w,d,s,g,js,fs){
		  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
		  js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
		  js.src='https://apis.google.com/js/platform.js';
		  fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
		}(window,document,'script'));

		gapi.analytics.ready(function() {
		  gapi.analytics.auth.authorize({
			container: 'embed-api-auth-container',
			clientid: '567913634068-l6fppmr88c5sdjbon55rq62ee38lpjsa.apps.googleusercontent.com'
		  });
		  var viewSelector = new gapi.analytics.ViewSelector({
			container: 'view-selector-container'
		  });
		  viewSelector.execute();
		  var dataChart = new gapi.analytics.googleCharts.DataChart({
			query: {
			  metrics: 'ga:sessions',
			  dimensions: 'ga:date',
			  'start-date': '30daysAgo',
			  'end-date': 'yesterday'
			},
			chart: {
			  container: 'chart-container',
			  type: 'LINE',
			  options: {
				width: '100%'
			  }
			}
		  });
		  viewSelector.on('change', function(ids) {
			dataChart.set({query: {ids: ids}}).execute();
		  });
		});
	</script>
</body>
</html>
<?php
	}
	mysqli_close($con);
?>