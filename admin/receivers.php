<?php
	require_once "../assets/config.php";
	if (!isset($_SESSION['username']))
	{
		header("Location: login.php?request_page=receivers");
		exit;
	}
	elseif ($_SESSION['state']=="inactive")
	{
		header("Location: lockscreen.php?request_page=receivers");
		exit;
	}
	elseif (isset($_REQUEST['upload']) && isset($_GET['code']) && !empty($_GET['code']) && isset($_GET['brand']) && !empty($_GET['brand']))
	{
		if (!file_exists("receivers/".trim($_GET['brand'])))
			mkdir("receivers/".trim($_GET['brand']));
		elseif (!file_exists("receivers/".trim($_GET['brand'])."/".trim($_GET['code'])))
			mkdir("receivers/".trim($_GET['brand'])."/".trim($_GET['code']));
		move_uploaded_file($_FILES['file']['tmp_name'], "receivers/".trim($_GET['brand'])."/".trim($_GET['code'])."/".basename($_FILES['file']['name']));
		$f= fopen(".log", "a");
		fputs($f, "[".date('d-M-Y H:i:s')."] «".strtoupper($_SESSION['username'])."» HAS UPLOADED «".basename($_FILES['file']['name'])."» TO RECEIVER «".trim($_GET['brand'])." ".trim($_GET['code'])."».\n");
		fclose($f);
		exit;
	}
	elseif (isset($_REQUEST['get_options']) && !empty($_REQUEST['get_options']))
	{
		$res= mysqli_query($con, "SELECT series, code FROM products JOIN receivers USING (code) WHERE brand='".trim(mysqli_real_escape_string($con, $_REQUEST['get_options']))."' ORDER BY series, code");
		if (mysqli_num_rows($res))
			while ($row= mysqli_fetch_assoc($res))
				echo "<option value=\"{$row['code']}\">".((!empty($row['series']) && $row['series']!=NULL)? $row['series'].' '.trans('Series'):'')."</option>";
		exit;
	}
	elseif (in_array($_SESSION['role'], ["Admin","Editor"]) && isset($_REQUEST['change_info']) && isset($_POST['code']) && isset($_POST['series']) && isset($_POST['link1']) && isset($_POST['link2']))
	{
		mysqli_query($con, "UPDATE receivers SET series='".trim(mysqli_real_escape_string($con, $_POST['series']))."', link1='".trim(mysqli_real_escape_string($con, $_POST['link1']))."', link2='".trim(mysqli_real_escape_string($con, $_POST['link2']))."' WHERE code='".trim(mysqli_real_escape_string($con, $_POST['code']))."'");
		echo mysqli_error($con);
		exit;
	}
	elseif (isset($_REQUEST['get_info']) && !empty($_REQUEST['get_info']))
	{
		$res= mysqli_query($con, "SELECT code,title,brand,shortdescr,descr,pic1,pic2,pic3,pic4,pic5,webpage,series,link1,link2 FROM products JOIN receivers USING (code) WHERE code='".trim(mysqli_real_escape_string($con, $_REQUEST['get_info']))."'");
		if (mysqli_num_rows($res)==1)
		{
			$row= mysqli_fetch_assoc($res);
			$x=0;
			if ($row['pic1']!=NULL && !empty($row['pic1'])) $pic[$x++]= $row['pic1']; if ($row['pic2']!=NULL && !empty($row['pic2'])) $pic[$x++]= $row['pic2']; if ($row['pic3']!=NULL && !empty($row['pic3'])) $pic[$x++]= $row['pic3']; if ($row['pic4']!=NULL && !empty($row['pic4'])) $pic[$x++]= $row['pic4']; if ($row['pic5']!=NULL && !empty($row['pic5'])) $pic[$x++]= $row['pic5'];
?>
			<div class="row" id="rec_info">
				<div class="col-md-5">
					<div class="card">
						<div class="card-body">
							<h4 class="card-title"><?php echo $row['code'] ?></h4><?php if (!empty($row['shortdescr'])) echo '<h6 class="card-subtitle">'.$row['shortdescr'].'</h6>'; ?> 
							<ul class="nav nav-tabs" role="tablist">
<?php
			if ($x)
				echo '			<li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#Photos" role="tab" aria-selected="true"><span><i class="fas fa-images"></i></span></a></li>';
			if (!empty($row['descr']))
				echo '			<li class="nav-item"><a class="nav-link'.((!$x)? " active show":"").'" data-toggle="tab" href="#Description" role="tab" aria-selected="false"><span><i class="fas fa-file-alt"></i></span></a></li>';
?>
								<li class="nav-item"><a class="nav-link<?php echo (!$x && empty($row['descr']))? " active show":""; ?>" data-toggle="tab" href="#Links" role="tab" aria-selected="false"><span><i class="fas fa-link"></i></span></a></li>
							</ul>
							<div class="tab-content" style="border: 1px solid #ddd; border-top: 0px;">
<?php
			if ($x)
			{
?>
								<div class="tab-pane active show" id="Photos" role="tabpanel">
									<div class="card" style="margin-bottom: unset">
										<div class="card-body">
											<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
												<div class="carousel-inner" role="listbox">
<?php
				for ($i=0; $i<$x; $i++)
					echo '							<div class="carousel-item'.(($i==0)? ' active':'').'"><img class="img-responsive" src="/assets/images/products/'.$pic[$i].'" /></div>'."\n";
?>
												</div>
<?php
			if ($x>1)
			{
?>
												<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span><span class="sr-only">Previous</span></a>
												<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next"><span class="carousel-control-next-icon" aria-hidden="true"></span><span class="sr-only">Next</span></a>
<?php
			}
?>
											</div>
										</div>
									</div>
								</div>
<?php
			}
			if (!empty($row['descr']))
			{
?>
								<div class="tab-pane p-20<?php echo (!$x)? " active show":""; ?>" id="Description" role="tabpanel"><ul style="padding-left: 20px;"><?php foreach (explode("\n", $row['descr']) as $line) echo "<li>".$line."</li>"; ?></ul></div>
<?php
			}
			if (!empty($row['webpage']) || !empty($row['link1']) || !empty($row['link2']))
				echo '			<div class="tab-pane p-20'.((!$x && empty($row['descr']))? " active show":"").'" id="Links" role="tabpanel">'.((!empty($row['webpage']))? '<a target="_blank" style="display: block; margin-bottom: 8px;" href="'.$row['webpage'].'">'.trans('Webpage').'&nbsp;&nbsp;<i class="fas fa-external-link-alt"></i></a>':'').((!empty($row['link1']))? '<a target="_blank" style="display: block; margin-bottom: 8px;" href="'.$row['link1'].'">'.$row['link1'].'</a>':'').((!empty($row['link2']))? '<a target="_blank" href="'.$row['link2'].'">'.$row['link2'].'</a>':'').'</div>'."\n";
?>
							</div>
<?php
			if (in_array($_SESSION['role'], ["Admin","Editor"]))
			{
?>
							<div role="form" class="p-20" style="border: 1px solid #ddd; margin-top: 18px;">
								<div class="m-b-20 input-group">
									<input class="form-control" id="series" maxlength="33" value="<?php echo $row['series']; ?>" placeholder="<?php echo trans('Series'); ?>" type="text" pattern=".{3,33}" />
									<div class="input-group-append"><span class="input-group-text"><i class="fas fa-project-diagram"></i></span></div>
								</div>
								<div class="m-b-20 input-group">
									<input class="form-control" id="link1" value="<?php echo $row['link1']; ?>" placeholder="<?php echo trans('Link'); ?> 1" type="url" />
									<div class="input-group-append"><span class="input-group-text"><i class="fas fa-link"></i></span></div>
								</div>
								<div class="m-b-20 input-group">
									<input class="form-control" id="link2" value="<?php echo $row['link2']; ?>" placeholder="<?php echo trans('Link'); ?> 2" type="url" />
									<div class="input-group-append"><span class="input-group-text"><i class="fas fa-link"></i></span></div>
								</div>
								<button class="btn btn-info waves-effect" onclick="if ($('#link1')[0].reportValidity() && $('#link2')[0].reportValidity()) change_info('<?php echo $row['code']; ?>', $('#series').val(), $('#link1').val(), $('#link2').val())"><?php echo trans('Save'); ?></button>
							</div>
<?php
			}
?>
						</div>
					</div>
				</div>
				<div class="col-md-7">
					<div class="card">
						<div class="card-body">
							<h4 class="card-title"><?php echo trans("Available Files"); ?></h4>
							<h6 class="card-subtitle"><?php echo trans("Here you will find all the files in the server concerning this receiver."); ?></h6>
							
<?php
			$PATH= 'receivers/'.$row['brand'].'/'.$row['code'].'/';
			$ERR= "<div class=\"alert alert-warning\"><h3 class=\"text-warning\"><i class=\"far fa-frown\"></i> ".trans('Sorry')."</h3>".trans("We couldn't find any file concerning this receiver!")."</div>";
			if (file_exists($PATH))
			{
				$dir= opendir($PATH);
				while ($file= readdir($dir))
					if (!is_dir($file))
					{
						$x= (string)filectime($PATH.$file).':'.$file;
						$FILES[$x]= pathinfo($file);
						$FILES[$x]['date']= date("H:i:s d/m/Y", (int)explode(':',$x)[0]);
						$FILES[$x]['dirname']= $PATH;
						$FILES[$x]['size']= filesize($FILES[$x]['dirname'].$file);
					}
				if (isset($FILES))
				{
?>
							<div class="table-responsive">
								<table class="table color-bordered-table dark-bordered-table">
									<style>.table {border: 1px solid #ddd !important} .table td {padding: unset} td a {display: block; padding: 1rem; color: #343a40;} tr {cursor: pointer; overflow: hidden;} tr:hover td {background:#f6f6f6}</style>
								<?php echo "<thead><tr><th>".trans("File Name")."</th><th>".trans("Size")."</th><th>".trans("Date Added")."</th><th></th></tr></thead>\n"; ?>
									<tbody>
<?php
					ksort($FILES);
					foreach ($FILES as $F)
						echo "			<tr><td><a href=\"".$FILES[$x]['dirname'].$F['basename']."\"><i class=\"far fa-file".((in_array($F['extension'], ['zip','rar']))? '-archive':'')."\"></i>&nbsp;&nbsp;".$F['basename']."</a></td><td><a href=\"".$FILES[$x]['dirname'].$F['basename']."\">".number_format($F['size']/1048576, 2)." ".trans('MB')."</a></td><td><a href=\"".$FILES[$x]['dirname'].$F['basename']."\">{$F['date']}</a></td><td>".(($_SESSION['role']=="Admin")?"<a style=\"color: red\" onclick=\"del_file('{$F['basename']}',$('input[list=brand]').val(),$('input[list=code]').val())\" class=\"fas fa-trash-alt\"></a>":"")."</td></tr>";
			
?>
									</tbody>
								</table>
							</div>
<?php
				}
				else
					echo $ERR;
			}
			else
				echo $ERR;
?>
							<style>.dz-default {padding: 0 !important}</style>
							<form id="uploadWidget" method="post" class="dropzone" style="border: 1px solid #ddd; min-height: unset;"><div class="fallback"></div></form>
						</div>
					</div>
				</div>
			</div>
<?php
			$f= fopen(".log", "a");
			fputs($f, "[".date('d-M-Y H:i:s')."] «".strtoupper($_SESSION['username'])."» HAS LOOKED FOR RECEIVER «{$row['brand']} {$row['code']}».\n");
			fclose($f);
		}
		exit;
	}
	elseif ($_SESSION['role']=="Admin" && isset($_REQUEST['del_file']) && !empty($_REQUEST['del_file']) && isset($_GET['code']) && !empty($_GET['code']) && isset($_GET['brand']) && !empty($_GET['brand']))
	{
		unlink('receivers/'.trim($_GET['brand']).'/'.trim($_GET['code']).'/'.trim($_REQUEST['del_file']));
		exit;
	}	
	else
	{
		echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
		getHead(["color"=>$_SESSION['color'],"title"=>trans("Receivers"),"icon"=>"/assets/images/logo-icon.png","css"=>["/assets/css/admin-modern.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css"]]);
?>
<body class="fixed-layout <?php echo $_SESSION['theme']." lock-nav\">"; getPreloader(); ?>
	<div id="main-wrapper"><?php require "topbar"; require "left-sidebar"; ?>
		<div class="page-wrapper">
			<div class="container-fluid">
				<div class="row page-titles">
					<div class="col-md-5 align-self-center">
						<h4 class="text-themecolor"><?php echo trans("Receivers"); ?></h4>
					</div>
					<div class="col-md-7 align-self-center text-right">
						<div class="d-flex justify-content-end align-items-center">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="index.php"><?php echo trans("Home"); ?></a></li>
								<li class="breadcrumb-item active"><?php echo trans("Receivers"); ?></li>
							</ol>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
								<h4 class="card-title"><?php echo trans("Find a Receiver"); ?></h4>
								<h6 class="card-subtitle"><?php echo trans("by choosing his brand then this code."); ?></h6>
								<div role="form" class="form-material m-t-40 row">
<?php
		$res= mysqli_query($con, "SELECT DISTINCT brand FROM products WHERE category='TV Receivers' ORDER BY brand");
		if (!mysqli_num_rows($res))
			echo "";
		else
		{
			echo "\t\t\t\t\t\t\t\t\t<datalist id=\"brand\">\n";
			while ($br= mysqli_fetch_array($res))
				echo "\t\t\t\t\t\t\t\t\t\t<option value=\"{$br[0]}\" />\n";
			echo "\t\t\t\t\t\t\t\t\t</datalist>\n";
?>
									<div class="form-group col-md-6 m-t-20">
										<input list="brand" class="form-control form-control-line" autofocus="" autocomplete="off"
										onchange="list_options(this.value)" placeholder="<?php echo trans("Select Brand"); ?>" required="" />
									</div>
<?php
		}
?>
									<datalist id="code"></datalist>
									<div class="form-group col-md-6 m-t-20">
										<input list="code" class="form-control form-control-line" autocomplete="off"
										onchange="list_info(this.value)" placeholder="<?php echo trans("Select Code"); ?>" required="" disabled="disabled" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><?php require "right-sidebar"; getFooter(); ?>
	</div><?php getJSCalls(["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js","/assets/js/popper.min.js","https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js","/assets/js/perfect-scrollbar.jquery.min.js","/assets/js/waves.js","/assets/js/sidebarmenu.js","/assets/js/custom.js","/assets/js/sessionTimeout.php","https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"]); ?>
	<script type="text/javascript">
		Dropzone.options.uploadWidget=
		{
			paramName: 'file', maxFilesize: 100, maxFiles: 5, dictDefaultMessage: "<?php echo trans('Drag & drop a file here or click'); ?>",
			init: function () {this.on("complete", function (file) {list_info(document.querySelector('input[list="code"]').value);})}
		};
		function list_options (brand)
		{
			document.querySelector('input[list="code"]').placeholder= "<?php echo trans("Loading options..."); ?>";
			document.querySelector('input[list="code"]').value= "";
			document.querySelector('input[list="code"]').setAttribute('disabled', 'disabled');
			if (document.getElementById('rec_info')!=null) document.getElementById('rec_info').remove();
			var xhr= new XMLHttpRequest();
			xhr.open('get', 'receivers.php?get_options='+brand, true);
			xhr.onreadystatechange= function ()
			{
				if (this.readyState==4 && this.status==200)
					if (this.responseText.length==0)
					{
						document.querySelector('input[list="code"]').placeholder= "<?php echo trans("Couldn't load options!"); ?>";
					}
					else
					{
						document.querySelector('input[list="code"]').placeholder= "<?php echo trans("Select Code"); ?>";
						document.querySelector('datalist[id="code"]').innerHTML= this.responseText;
						document.querySelector('input[list="code"]').removeAttribute('disabled');
					}
			}
			xhr.send();
		}
		function list_info (code)
		{
			if (code!="")
			{
				var xhr= new XMLHttpRequest();
				xhr.open('get', 'receivers.php?get_info='+code, true);
				xhr.onreadystatechange= function ()
				{
					if (this.readyState==4 && this.status==200)
					{
						if (document.getElementById('rec_info')!=null)
							document.getElementById('rec_info').remove();
						if (this.responseText!='')
						{
							document.getElementsByClassName('container-fluid')[0].insertAdjacentHTML("beforeend", this.responseText);
							var uploadWidget= new Dropzone("#uploadWidget", {url: "receivers.php?upload&code="+code+"&brand="+document.querySelector('input[list="brand"]').value});
						}
					}
				}
				xhr.send();
			}
		}
<?php
		if (in_array($_SESSION['role'], ["Admin","Editor"]))
		{
?>
		function change_info (code, series, link1, link2)
		{
			var xhr= new XMLHttpRequest();
			xhr.open('post', 'receivers.php', true);
			xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			xhr.onreadystatechange= function()
			{
				if (this.readyState==4 && this.status==200)
					list_info(code);
			}
			xhr.send('change_info&code='+code+'&series='+series+'&link1='+link1+'&link2='+link2);
		}
		function del_file (file, brand, code)
		{
			var xhr= new XMLHttpRequest();
			xhr.open('get', 'receivers.php?del_file='+file+'&brand='+brand+'&code='+code, true);
			xhr.send();
			list_info(code);
		}
<?php
		}
?>
	</script>
</body>
</html>
<?php
	}
	mysqli_close($con);
?>