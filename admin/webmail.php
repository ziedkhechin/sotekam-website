<?php
	require_once "../assets/config.php";
	if (!isset($_SESSION['username']))
	{
		header("Location: login.php?request_page=webmail");
		exit;
	}
	elseif ($_SESSION['state']=="inactive")
	{
		header("Location: lockscreen.php?request_page=webmail");
		exit;
	}
	elseif (!in_array($_SESSION['role'], ["Admin","Editor"]))
	{
		header("Location: /error.php?code=403");
		exit;
	}
	else
	{
		echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
		getHead(["color"=>$_SESSION['color'],"title"=>trans("Email"),"icon"=>"/assets/images/logo-icon.png","css"=>["/assets/css/admin-modern.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css"]]);
?>
<body class="fixed-layout <?php echo $_SESSION['theme'].'">'; getPreloader(); ?>
	<div id="main-wrapper"><?php require "topbar"; require "left-sidebar"; ?>
		<div class="page-wrapper">
			<div class="row"><div class="col-lg-12"><iframe name="main" frameborder="0" width="100%" height="722" src="https://pro.webhost.tn:2096"></iframe></div></div>
		</div><?php require "right-sidebar"; getFooter(); ?>
	</div><?php getJSCalls(["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js","/assets/js/popper.min.js","https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js","/assets/js/perfect-scrollbar.jquery.min.js","/assets/js/waves.js","/assets/js/sidebarmenu.js","/assets/js/custom.js","/assets/js/sessionTimeout.php"]); ?>
</body>
</html>
<?php
	}
	mysqli_close($con);
?>