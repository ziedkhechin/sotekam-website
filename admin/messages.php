<?php
	require_once "../assets/config.php";
	if (!isset($_SESSION['username']))
	{
		header("Location: login.php?request_page=messages");
		exit;
	}
	elseif ($_SESSION['state']=="inactive")
	{
		header("Location: lockscreen.php?request_page=messages");
		exit;
	}
	elseif (!in_array($_SESSION['role'], ["Admin","Editor"]))
	{
		header("Location: /error.php?code=403");
		exit;
	}
	elseif (isset($_GET['delete']))
	{
		mysqli_query($con, "DELETE FROM messages WHERE id='".trim(mysqli_real_escape_string($con, $_GET['delete']))."'");
		header('Location: messages.php');
		exit;
	}
	else
	{
		echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
		getHead(["color"=>$_SESSION['color'],"title"=>trans("Messages"),"icon"=>"/assets/images/logo-icon.png","css"=>["/assets/css/admin-modern.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css"]]);
?>
<body class="fixed-layout <?php echo $_SESSION['theme']." lock-nav\">"; getPreloader(); ?>
	<div id="main-wrapper"><?php require "topbar"; require "left-sidebar"; ?>
		<div class="page-wrapper">
			<div class="container-fluid">
<?php
		if (isset($_GET['id']) && !empty($_GET['id']))
		{
			$sql= "SELECT * FROM messages WHERE id='".mysqli_real_escape_string($con, $_GET['id'])."'";
			$r= mysqli_query($con, $sql);
			if (mysqli_num_rows($r)==0)
				echo "<script>window.location=\"messages.php\";</script>";
			$row= mysqli_fetch_assoc($r);
			mysqli_query($con, "UPDATE messages SET seen='1' WHERE id='".$row['id']."'");
?>
				<div class="row page-titles">
					<div class="col-md-5 align-self-center">
						<h4 class="text-themecolor"><?php echo trans("Message Details"); ?></h4>
					</div>
					<div class="col-md-7 align-self-center text-right">
						<div class="d-flex justify-content-end align-items-center">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="index.php"><?php echo trans("Home"); ?></a></li>
								<li class="breadcrumb-item"><?php echo trans("Inbox"); ?></li>
								<li class="breadcrumb-item"><a href="messages.php"><?php echo trans("Messages"); ?></a></li>
								<li class="breadcrumb-item active"><?php echo trans("Message Details"); ?></li>
							</ol>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="card b-all shadow-none">
							<div class="card-body">
								<h4 class="card-title m-b-0"><?php echo $row['subject']; ?></h4>
							</div>
							<div><hr class="m-t-0"></div>
							<div class="card-body">
								<div class="d-flex m-b-40">
									<div><img src="/assets/images/users/default_user.jpg" width="60" class="img-circle" /></div>
									<div class="p-l-10">
										<h4 class="m-b-0"><?php echo $row['name']; ?></h4>
										<small class="text-muted">
											<?php echo "<a href=\"mailto:".$row['email']."\">".$row['email']."</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href=\"tel:".$row['phone']."\">".$row['phone']."</a>"; ?><br />
											<?php echo $row['time']; ?>
										</small>
									</div>
								</div>
								<p style="margin-top: 2rem;"><?php echo nl2br($row['message']); ?></p>
							</div>
						</div>
					</div>
				</div>
<?php
		}
		else
		{
?>
				<div class="row page-titles">
					<div class="col-md-5 align-self-center">
						<h4 class="text-themecolor"><?php echo trans("Messages"); ?></h4>
					</div>
					<div class="col-md-7 align-self-center text-right">
						<div class="d-flex justify-content-end align-items-center">
							<ol class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="index.php"><?php echo trans("Home"); ?></a>
								</li>
								<li class="breadcrumb-item"><?php echo trans("Inbox"); ?></li>
								<li class="breadcrumb-item active"><?php echo trans("Messages"); ?></li>
							</ol>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="card b-all shadow-none">
							<div class="card-body">
<?php
			$r= mysqli_query($con, "SELECT * FROM messages ORDER BY id DESC");
			if (!mysqli_num_rows($r))
				echo "<div class=\"alert alert-warning\" style=\"margin: 0; clear: both;\"><h3 class=\"text-warning\"><i class=\"far fa-frown\"></i> ".trans("Sorry")."</h3>".trans("We couldn't find any record in database!")."</div>\n";
			else
			{
?>
								<div class="table-responsive">
									<table class="table color-bordered-table dark-bordered-table" style="margin-bottom: unset">
										<colgroup><col span="3" style="width: 31%"><col style="width: 5%"></colgroup>
										<style>.table {border: 1px solid #ddd !important} .table td {padding: unset} td a {display: block; padding: 1rem; color: #343a40;}
										tr {cursor: pointer; overflow: hidden;} tr:hover td {background:#f6f6f6}</style>
										<tbody>
<?php
				while ($row= mysqli_fetch_assoc($r))
				{
?>
											<tr><td><a href="<?php echo $_SERVER['PHP_SELF']."?id=".$row['id']; ?>"><?php if ($row['seen']=="0") echo "<i class=\"hidden-xs-down fa fa-star text-warning\"></i> "; echo $row['name']; ?></a></td>
											<td><a href="<?php echo $_SERVER['PHP_SELF']."?id=".$row['id']; ?>"><span><?php echo $row['subject']; ?></span></a></td>
											<td><a href="<?php echo $_SERVER['PHP_SELF']."?id=".$row['id']; ?>"><?php echo date("H:i:s d/m/Y", strtotime(htmlspecialchars($row['time']))); ?></a></td>
											<td><a style="color: red" class="fas fa-trash-alt" href="<?php echo $_SERVER['PHP_SELF']."?delete=".$row['id']; ?>"></a></td></tr>
<?php
				}
?>
										</tbody>
									</table>
								</div>
<?php
			}
?>
							</div>
						</div>
					</div>
				</div>
<?php
		}
?>
			</div>
		</div><?php require "right-sidebar"; getFooter(); ?>
	</div><?php getJSCalls(["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js","/assets/js/popper.min.js","https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js","/assets/js/perfect-scrollbar.jquery.min.js","/assets/js/waves.js","/assets/js/sidebarmenu.js","/assets/js/custom.js","/assets/js/sessionTimeout.php"]); ?>
</body>
</html>
<?php
	}
	mysqli_close($con);
?>