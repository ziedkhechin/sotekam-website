<?php
	require_once "../assets/config.php";
	if (isset($_POST['attempt']) && isset($_POST['username']) && isset($_POST['password'])) // Attempt to login by POST method
	{
		if ($stmt= mysqli_prepare($con, "SELECT id, username, password, name, role, avatar, theme, color, language FROM users WHERE username=?"))
			mysqli_stmt_bind_param($stmt, 's', $_POST['username']);
		$_POST['username']= trim(mysqli_real_escape_string($con, $_POST['username']));
		$_POST['password']= trim(mysqli_real_escape_string($con, $_POST['password']));
		mysqli_stmt_execute($stmt);
		mysqli_stmt_store_result($stmt);
		if (mysqli_stmt_num_rows($stmt)==1)
		{
			$res= [];
			mysqli_stmt_bind_result($stmt, $res['id'], $res['username'], $hashed_password, $res['name'], $res['role'], $res['avatar'], $res['theme'], $res['color'], $res['language']);
			if (mysqli_stmt_fetch($stmt))
				if (password_verify($_POST['password'], $hashed_password))
				{
					session_regenerate_id();
					$_SESSION= $res;
					$_SESSION['state']= "active";
					mysqli_stmt_free_result($stmt);
					mysqli_stmt_close($stmt);
					$f= fopen(".log", "a");
					fputs($f, "[".date('d-M-Y H:i:s')."] «".strtoupper($_SESSION['username'])."» HAS BEEN SIGNED IN WITH IP ADDRESS «".$_SERVER['REMOTE_ADDR']."», SESSION «".session_id()."», DEVICE «".$_SERVER['HTTP_USER_AGENT']."».\n");
					fclose($f);
					if (isset($_GET['request_page']))
						header("Location: ".trim($_GET['request_page']).".php");
					else
						header("Location: index.php");
					exit;
			}
		}
		mysqli_stmt_free_result($stmt);
		mysqli_stmt_close($stmt);
		header ('location: '.$_SERVER['PHP_SELF'].'?error');
		exit;
	}
	elseif (count($_POST)==1 || count($_POST)==2) // Vertify login by POST method
	{
		if (isset($_POST['username']))
		{
			$sql="SELECT password FROM users WHERE username='".trim(mysqli_real_escape_string($con, $_POST['username']))."'";
			$r= mysqli_query($con, $sql);
			if (mysqli_num_rows($r)==0)
				echo "!";
			else
				if (isset($_POST['password']))
					echo (password_verify(trim(mysqli_real_escape_string($con, $_POST['password'])), mysqli_fetch_array($r)['password'])) ? "@" : "!!";				
		}
	}
	elseif (isset($_SESSION['username'])) // Redirect if connected
	{
		header("Location: index.php");
		exit;
	}
	else // Show login page
	{
		echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
		getHead(["color"=>"#111111","title"=>trans("Admin Login"),"icon"=>"/assets/images/logo-icon.png","css"=>["https://use.fontawesome.com/releases/v5.6.1/css/all.css","/assets/css/login.css"]]);
?>
<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-b-160 p-t-50">
				<form class="login100-form validate-form" role="form" method="post" action="<?php echo $_SERVER['PHP_SELF'].((isset($_GET['request_page']))? "?request_page=".trim($_GET['request_page']):"") ?>">
					<input type="hidden" name="attempt" />
					<span class="login100-form-title" style="padding-bottom: 43px;"><?php echo trans('Admin Login'); ?></span>
					<div class="wrap-input100 rs1 validate-input" data-validate="<?php echo trans('Invalid Username'); ?>">
						<input autofocus="" class="input100" type="text" name="username" onchange="check()" autocomplete="username" />
						<span class="label-input100"><?php echo trans('Username'); ?></span>
					</div>
					<div class="wrap-input100 rs2 validate-input" data-validate="<?php echo trans('Invalid Password'); ?>">
						<input class="input100" type="password" name="password" onchange="check()" autocomplete="current-password" />
						<span class="label-input100"><?php echo trans('Password'); ?></span>
					</div>
					<div class="container-login100-form-btn">
						<button disabled="disabled" class="login100-form-btn"><?php echo trans('Sign in'); ?></button>
					</div>
				</form>
				<footer style="margin-top: 25px; text-align: center; color: white; font-size: 12px;">
					<span dir="auto">
						&copy;&nbsp;<span id="year"></span>&nbsp;<?php echo trans('SOTEKAM Inc.')."<br />".trans('WEBSITE CREATED & DEVELOPED BY'); ?> 
						<a href="http://on.fb.me/1zspNmb" style="color: black; font-size: 12px; font-weight: bold; line-height: unset; text-decoration: unset;" target="_blank">ZK</a>
					</span>
				</footer>
			</div>
		</div>
	</div><?php getJSCalls(["https://code.jquery.com/jquery-3.3.1.min.js"]); ?>
	<script type="text/javascript">
		$('#year')[0].innerHTML=new Date().getFullYear();
		function check ()
		{
			var xhr=new XMLHttpRequest(), user=$('input[name="username"]'), pass=$('input[name="password"]'), btn=$('button[class="login100-form-btn"]');
			btn.attr("disabled","disabled");
			xhr.open('post', 'login.php', true);
			xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			xhr.send('username='+user[0].value+'&password='+pass[0].value);
			xhr.onreadystatechange= function()
			{
				if (this.readyState==4 && this.status==200)
					switch (this.responseText)
					{
						case '!': user.parent().addClass('alert-validate'); btn.attr("disabled","disabled"); break;
						case '!!': pass.parent().addClass('alert-validate'); btn.attr("disabled","disabled"); break;
						case '@': user.parent().removeClass('alert-validate'); pass.parent().removeClass('alert-validate'); btn.removeAttr("disabled"); $('form')[0].submit();
					}
			}
		}
		(function($)
		{
			"use strict";
			$('.input100').each(function()
			{
				$(this).on('blur', function()
				{
					if ($(this).val().trim() != "")
						$(this).addClass('has-val');
					else
						$(this).removeClass('has-val');
				})
			})
			var input = $('.validate-input .input100');
			$('.validate-form').on('submit', function()
			{
				var check = true;
				for (var i = 0; i < input.length; i++)
					if (validate(input[i]) == false)
					{
						$(input[i]).parent().addClass('alert-validate');
						check = false;
					}
				return check;
			});
			$('.validate-form .input100').each(function()
			{
				$(this).focus(function()
				{
					$(this).parent().removeClass('alert-validate');
				});
			});

			function validate(input)
			{
				if ($(input).val().trim()=='')
					return false;
			}
		})(jQuery);
	</script>
</body>
</html>
<?php
	}
	mysqli_close($con);
?>