<?php
	require_once "../assets/config.php";
	if (!isset($_SESSION['username']))
	{
		header("Location: login.php?request_page=lockscreen");
		exit;
	}
	else
	{
		$_SESSION['state']="inactive";
		echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
		getHead(["color"=>"#111111","title"=>trans("Lock Screen"),"icon"=>"/assets/images/logo-icon.png","css"=>["https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css","/assets/css/lockscreen.css"]]);
?>
<body class="lock-screen" onload="startTime()">
	<div class="lock-wrapper">
		<div id="time"></div>
		<div class="lock-box text-center">
			<div class="lock-name"><?php echo $_SESSION['username']; ?></div>
			<img alt="lock avatar" src="/assets/images/users/<?php echo $_SESSION['avatar']; ?>">
			<div class="lock-pwd">
				<form class="form-inline" role="form" method="post" action="login.php<?php echo (isset($_GET['request_page']) && !empty($_GET['request_page']))? '?request_page='.htmlspecialchars($_GET['request_page']):''; ?>">
					<input type="hidden" name="attempt" />
					<input type="hidden" name="username" value="<?php echo $_SESSION['username']; ?>" />
					<div class="form-group">
						<input class="form-control lock-input" type="password" autofocus="" required="" name="password" placeholder="<?php echo trans('Password'); ?>" oninput="this.setCustomValidity('');" onchange="check();" autocomplete="current-password" />
						<button class="btn btn-lock" type="submit" disabled="disabled" title="<?php echo trans('Sign in'); ?>"><i class="fa fa-arrow-right"></i></button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var c= {"skin-green": "#318f94, #5ec58c", "skin-red": "#f62d51, #660fb5", "skin-blue": "#0178bc, #00bdda", "skin-purple": "#533fd0, #840fb5", "skin-megna": "#01c0c8, #1f72a2",
		"skin-green-dark": "#318f94, #5ec58c", "skin-red-dark": "#f62d51, #660fb5", "skin-blue-dark": "#0178bc, #00bdda", "skin-purple-dark": "#533fd0, #840fb5", "skin-megna-dark": "#01c0c8, #1f72a2"};
		document.querySelector('.lock-screen').style.cssText="background-image: linear-gradient(to right, "+c['<?php echo $_SESSION['theme']; ?>']+" 100%);"
		function check ()
		{
			var xhr=new XMLHttpRequest(), user=document.querySelector('input[name="username"]'), pass=document.querySelector('input[name="password"]'), btn=document.querySelector('button[type="submit"]');
			pass.setCustomValidity("");
			btn.setAttribute("disabled","disabled");
			xhr.open('post', 'login.php', true);
			xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			xhr.send('username='+user.value+'&password='+pass.value);
			xhr.onreadystatechange=function()
			{
				if (this.readyState==4 && this.status==200)
					switch (this.responseText)
					{
						case '!!': pass.setCustomValidity("<?php echo trans('Invalid Password'); ?>"); pass.reportValidity(); btn.setAttribute("disabled","disabled"); break;
						case '@': pass.setCustomValidity(""); btn.removeAttribute("disabled"); document.forms[0].submit(); break;
					}
			}
		}
		function startTime()
		{
			var today=new Date();
			var h=today.getHours();
			var m=today.getMinutes();
			var s=today.getSeconds();
			m=checkTime(m);
			s=checkTime(s);
			document.getElementById('time').innerHTML=h+":"+m+":"+s;
			t=setTimeout(function(){startTime()},500);
		}
		function checkTime(i)
		{
			if (i<10)
				i="0"+i;
			return i;
		}
	</script>
</body>
</html>
<?php
	}
	mysqli_close($con);
?>