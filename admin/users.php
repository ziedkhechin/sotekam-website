<?php
	require_once "../assets/config.php";
	if (!isset($_SESSION['username']))
	{
		header("Location: login.php?request_page=users");
		exit;
	}
	elseif ($_SESSION['state']=="inactive")
	{
		header("Location: lockscreen.php?request_page=users");
		exit;
	}
	elseif (!in_array($_SESSION['role'], ["Admin","Editor"]))
	{
		header("Location: /error.php?code=403");
		exit;
	}
	elseif ($_SESSION['role']=="Admin" && isset($_GET['delete']) && $_GET['delete']!="11111")
	{
		mysqli_query($con, "DELETE FROM users WHERE id='".trim(mysqli_real_escape_string($con, $_GET['delete']))."'");
		header('Location: users.php');
		exit;
	}
	elseif ($_SESSION['role']=="Admin" && count($_POST)!=0 && isset($_REQUEST['mode']) && in_array(trim($_REQUEST['mode']),["add","edit"]) && isset($_POST['username']) && preg_match("/^[A-Za-z0-9_]{3,25}$/",$_POST['username']) && $_POST['username']!="root" && isset($_POST['password']) && ((trim($_REQUEST['mode'])=="add" && preg_match("/^.{6,25}$/",$_POST['password'])) || trim($_REQUEST['mode'])=="edit") && isset($_POST['name']) && preg_match("/^[A-Za-z ]{3,25}$/",$_POST['name']) && isset($_POST['role']) && in_array($_POST['role'],["0","1","2"]) && (isset($_POST['id']) || $_REQUEST['mode']=='add'))
	{
		if (!empty($_FILES["Avatar"]["name"]))
		{
			$pic= "/assets/images/users/".basename($_FILES["Avatar"]["name"]);
			move_uploaded_file($_FILES["Avatar"]["tmp_name"], $pic);
			$pic= basename($_FILES["Avatar"]["name"]);
		}
		else
			$pic= "default_user.jpg";
		$_POST['username']= trim(mysqli_real_escape_string($con, $_POST['username']));
		if (!empty($_POST['password']))
			$_POST['password']= password_hash(trim(mysqli_real_escape_string($con, $_POST['password'])), PASSWORD_DEFAULT);
		$_POST['name']= trim(mysqli_real_escape_string($con, $_POST['name']));
		switch ($_POST['role'])
		{
			case '0': $_POST['role']="User"; break;
			case '1': $_POST['role']="Editor"; break;
			case '2': $_POST['role']="Admin"; break;
		}
		switch ($_REQUEST['mode'])
		{
			case 'add':
				if ($stmt= mysqli_prepare($con, "INSERT INTO users (username, password, name, role, avatar) VALUES (?, ?, ?, ?, ?)"))
					mysqli_stmt_bind_param($stmt, 'sssss', $_POST['username'], $_POST['password'], $_POST['name'], $_POST['role'], $pic);
				break;
			case 'edit':
				if (!empty($_FILES["Avatar"]["name"]) && !empty($_POST['password']))
				{
					if ($stmt= mysqli_prepare($con, "UPDATE users SET username=?, password=?, name=?, role=?, avatar=? WHERE id=?")) mysqli_stmt_bind_param($stmt, 'ssssss', $_POST['username'], $_POST['password'], $_POST['name'], $_POST['role'], $pic, $_POST['id']);
				}
				else
					if (!empty($_FILES["Avatar"]["name"]))
					{
						if ($stmt= mysqli_prepare($con, "UPDATE users SET username=?, name=?, role=?, avatar=? WHERE id=?")) mysqli_stmt_bind_param($stmt, 'sssss', $_POST['username'], $_POST['name'], $_POST['role'], $pic, $_POST['id']);
					}
				else
					if (!empty($_POST['password']))
					{
						if ($stmt= mysqli_prepare($con, "UPDATE users SET username=?, password=?, name=?, role=? WHERE id=?")) mysqli_stmt_bind_param($stmt, 'sssss', $_POST['username'], $_POST['password'], $_POST['name'], $_POST['role'], $_POST['id']);
					}
				else
					if ($stmt= mysqli_prepare($con, "UPDATE users SET username=?, name=?, role=? WHERE id=?")) mysqli_stmt_bind_param($stmt, 'ssss', $_POST['username'], $_POST['name'], $_POST['role'], $_POST['id']);
				break;
		}
		mysqli_stmt_execute($stmt);
		mysqli_stmt_close($stmt);
		header('Location: users.php');
		exit;
	}
	else
	{
		echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
		getHead(["color"=>$_SESSION['color'],"title"=>trans("Users"),"icon"=>"/assets/images/logo-icon.png","css"=>["/assets/css/admin-modern.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css"]]);
?>
<body class="fixed-layout <?php echo $_SESSION['theme']." lock-nav\">"; getPreloader(); ?>
	<div id="main-wrapper"><?php require "topbar"; require "left-sidebar"; ?>
		<div class="page-wrapper">
			<div class="container-fluid">
				<div class="row page-titles">
					<div class="col-md-5 align-self-center">
						<h4 class="text-themecolor"><?php echo trans("Users"); ?></h4>
					</div>
					<div class="col-md-7 align-self-center text-right">
						<div class="d-flex justify-content-end align-items-center">
							<ol class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="index.php"><?php echo trans("Home"); ?></a>
								</li>
								<li class="breadcrumb-item active"><?php echo trans("Users"); ?></li>
							</ol>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
<?php
		if ($_SESSION['role']=="Admin")
		{
?>
								<div class="modal fade in" id="add-contact" role="dialog" style="display: none;" tabindex="-1">
									<div class="modal-dialog">
										<form class="modal-content" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
											<input type="hidden" name="mode" value="add" />
											<div class="modal-header">
												<h4 class="modal-title"><?php echo trans("Add New User"); ?></h4><button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
											</div>
											<div class="modal-body">
												<div class="form-group">
													<div class="col-md-12 m-b-20 input-group">
														<input class="form-control" name="username" maxlength="25" placeholder="<?php echo trans("Username"); ?>" type="text" autocomplete="off" autofocus="" required="" pattern="[A-Za-z0-9_]{3,25}" />
														<div class="input-group-append"><span class="input-group-text"><i class="fas fa-user"></i></span></div>
													</div>
													<div class="col-md-12 m-b-20 input-group">
														<input class="form-control" name="password" maxlength="25" placeholder="<?php echo trans("Password"); ?>" type="text" autocomplete="off" required="" pattern=".{6,25}" />
														<div class="input-group-append"><span class="input-group-text"><i class="fas fa-key"></i></span></div>
													</div>
													<div class="col-md-12 m-b-20 input-group">
														<input class="form-control" name="name" maxlength="25" placeholder="<?php echo trans("Name"); ?>" type="text" autocapitalize="words" autocomplete="off" required="" pattern="[A-Za-z ]{3,25}" />
														<div class="input-group-append"><span class="input-group-text"><i class="fas fa-user-tag"></i></span></div>
													</div>
													<div class="col-md-12 m-b-20" style="text-align: center">
														<span style="float: left"><?php echo trans("User"); ?></span><span><?php echo trans("Editor"); ?></span><span style="float: right"><?php echo trans("Admin"); ?></span>
														<input class="form-control" name="role" maxlength="15" placeholder="<?php echo trans("Role"); ?>" type="range" min="0" max="2" value="0" />
													</div>
													<div class="col-md-12 m-b-20">
														<div class="fileupload btn btn-danger btn-rounded waves-effect waves-light">
															<span><?php echo trans("Avatar"); ?></span>
															<input class="upload" type="file" name="Avatar" accept="image/png, image/jpeg" />
														</div>
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button class="btn btn-info waves-effect" type="submit"><?php echo trans("Save"); ?></button>
												<button class="btn btn-default waves-effect" data-dismiss="modal" type="button"><?php echo trans("Cancel"); ?></button>
											</div>
										</form>
									</div>
								</div>
								<div class="modal fade in" id="modify-contact" role="dialog" style="display: none;" tabindex="-1">
									<div class="modal-dialog">
										<form class="modal-content" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
											<input type="hidden" name="mode" value="edit" />
											<input type="hidden" name="id" />
											<div class="modal-header">
												<h4 class="modal-title"><?php echo trans("Edit User"); ?></h4><button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
											</div>
											<div class="modal-body">
												<div class="form-group">
													<div class="col-md-12 m-b-20 input-group">
														<input class="form-control" name="username" maxlength="25" placeholder="<?php echo trans("Username"); ?>" type="text" autocomplete="off" autofocus="" required="" pattern="[A-Za-z0-9_]{3,25}" />
														<div class="input-group-append"><span class="input-group-text"><i class="fas fa-user"></i></span></div>
													</div>
													<div class="col-md-12 m-b-20 input-group">
														<input class="form-control" name="password" maxlength="25" placeholder="<?php echo trans("Password"); ?>" type="text" autocomplete="off" pattern=".{6,25}" />
														<div class="input-group-append"><span class="input-group-text"><i class="fas fa-key"></i></span></div>
													</div>
													<div class="col-md-12 m-b-20 input-group">
														<input class="form-control" name="name" maxlength="25" placeholder="<?php echo trans("Name"); ?>" type="text" autocapitalize="words" autocomplete="off" required="" pattern="[A-Za-z ]{3,25}" />
														<div class="input-group-append"><span class="input-group-text"><i class="fas fa-user-tag"></i></span></div>
													</div>
													<div class="col-md-12 m-b-20" style="text-align: center">
														<span style="float: left"><?php echo trans("User"); ?></span><span><?php echo trans("Editor"); ?></span><span style="float: right"><?php echo trans("Admin"); ?></span>
														<input class="form-control" name="role" maxlength="15" placeholder="<?php echo trans("Role"); ?>" type="range" min="0" max="2" value="0" />
													</div>
													<div class="col-md-12 m-b-20">
														<div class="fileupload btn btn-danger btn-rounded waves-effect waves-light">
															<span><?php echo trans("Avatar"); ?></span>
															<input class="upload" type="file" name="Avatar" accept="image/png, image/jpeg" />
														</div>
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button class="btn btn-info waves-effect" type="submit"><?php echo trans("Save"); ?></button>
												<button class="btn btn-default waves-effect" data-dismiss="modal" type="button"><?php echo trans("Cancel"); ?></button>
											</div>
										</form>
									</div>
								</div>
<?php
		}
		if ($_SESSION['role']=="Admin")
			echo "\t\t\t\t\t\t\t\t".'<button class="btn btn-info btn-rounded m-t-10 mb-2 float-right" data-target="#add-contact" data-toggle="modal" type="button">'.trans("Add New User").'</button>'."\n";
?>
								<div class="table-responsive">
									<table class="table table-bordered m-t-30">
										<thead><tr><?php echo "<th>".trans("Username")."</th><th>".trans("Name")."</th><th>".trans("Role")."</th>"; echo ($_SESSION['role']=="Admin")? "<th>".trans("Password")."</th>":""; echo "<th>".trans("Date Added")."</th>"; echo ($_SESSION['role']=="Admin")? "<th>".trans("Options")."</th>":""; ?></tr></thead>
										<tbody>
<?php
		$r= mysqli_query($con, "SELECT * FROM users ORDER BY created_at DESC");
		$x= ['User'=>0, 'Editor'=>1, 'Admin'=>2];
		while ($row= mysqli_fetch_assoc($r))
		{
				
?>
											<tr>
												<td><?php echo htmlspecialchars($row['username']); ?></td>
												<td><img class="rounded-circle" src="/assets/images/users/<?php echo htmlspecialchars($row['avatar']); ?>" width="30" />&nbsp;&nbsp;<?php echo htmlspecialchars($row['name']); ?></td>
												<td><span class="label <?php echo ($row['role']=="Admin")? "label-danger":"label-info"; echo"\">".trans(htmlspecialchars($row['role'])); ?></span></td><?php echo ($_SESSION['role']=="Admin")? "<td>**********</td>\n":"\n"; ?>
												<td><?php echo date("H:i:s d/m/Y", strtotime(htmlspecialchars($row['created_at']))); ?></td><?php echo ($_SESSION['role']=="Admin" && $row['username']!="root")? "\n\t\t\t\t\t\t\t\t\t\t\t\t<td><button class=\"btn btn-info fas fa-pen\" data-target=\"#modify-contact\" data-toggle=\"modal\" onclick=\"var form=document.querySelectorAll('#modify-contact input');form[1].value='".$row['id']."';form[2].value='".htmlspecialchars($row['username'])."';form[4].value='".htmlspecialchars($row['name'])."';form[5].value='".$x[htmlspecialchars($row['role'])]."';\"></button> <a href=\"".$_SERVER['PHP_SELF']."?delete=".$row['id']."\" class=\"btn btn-danger fas fa-user-slash\"></a></td>\n":"\n"; ?>
											</tr>
<?php
		}
?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><?php require "right-sidebar"; getFooter(); ?>
	</div><?php getJSCalls(["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js","/assets/js/popper.min.js","https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js","/assets/js/perfect-scrollbar.jquery.min.js","/assets/js/waves.js","/assets/js/sidebarmenu.js","/assets/js/custom.js","/assets/js/sessionTimeout.php"]); ?>
</body>
</html>
<?php
	}
	mysqli_close($con);
?>