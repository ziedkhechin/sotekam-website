<?php
	require_once "../assets/config.php";
	if (!isset($_SESSION['username']))
	{
		header("Location: login.php?request_page=log");
		exit;
	}
	elseif ($_SESSION['state']=="inactive")
	{
		header("Location: lockscreen.php?request_page=log");
		exit;
	}
	elseif ($_SESSION['role']!="Admin")
	{
		header("Location: /error.php?code=403");
		exit;
	}
	elseif (isset($_REQUEST['empty']))
	{
		unlink('.log');
		header("Location: log.php");
		exit;
	}
	else
	{
		echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
		getHead(["color"=>$_SESSION['color'],"title"=>trans("Log File"),"icon"=>"/assets/images/logo-icon.png","css"=>["/assets/css/admin-modern.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css"]]);
?>
<body class="fixed-layout <?php echo $_SESSION['theme']." lock-nav\">"; getPreloader(); ?>
	<div id="main-wrapper"><?php require "topbar"; require "left-sidebar"; ?>
		<div class="page-wrapper">
			<div class="container-fluid">
				<div class="row page-titles">
					<div class="col-md-5 align-self-center">
						<h4 class="text-themecolor"><?php echo trans("Log File"); ?></h4>
					</div>
					<div class="col-md-7 align-self-center text-right">
						<div class="d-flex justify-content-end align-items-center">
							<ol class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="index.php"><?php echo trans("Home"); ?></a>
								</li>
								<li class="breadcrumb-item active"><?php echo trans("Log File"); ?></li>
							</ol>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
								<a class="btn btn-info btn-rounded m-t-10 mb-2 float-right" style="color: white" <?php echo 'href="'.$_SERVER['PHP_SELF'].'?empty">'.trans("Empty").' '.trans("Log File"); ?></a>
								<pre style="border: 1px solid #ddd; clear: both; padding: 20px 5px;"><?php echo (file_exists('.log'))? file_get_contents('.log'):''; ?></pre>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><?php require "right-sidebar"; getFooter(); ?>
	</div><?php getJSCalls(["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js","/assets/js/popper.min.js","https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js","/assets/js/perfect-scrollbar.jquery.min.js","/assets/js/waves.js","/assets/js/sidebarmenu.js","/assets/js/custom.js","/assets/js/sessionTimeout.php"]); ?>
</body>
</html>
<?php
	}
	mysqli_close($con);
?>