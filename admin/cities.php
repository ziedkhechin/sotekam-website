<?php
	require_once "../assets/config.php";
	if (!isset($_SESSION['username']))
	{
		header("Location: login.php?request_page=cities");
		exit;
	}
	elseif ($_SESSION['state']=="inactive")
	{
		header("Location: lockscreen.php?request_page=cities");
		exit;
	}
	elseif (!in_array($_SESSION['role'], ["Admin","Editor"]))
	{
		header("Location: /error.php?code=403");
		exit;
	}
	elseif (in_array($_SESSION['role'], ["Admin","Editor"]) && isset($_GET['delete']))
	{
		mysqli_query($con, "DELETE FROM cities WHERE city='".trim(mysqli_real_escape_string($con, $_GET['delete']))."'");
		header('Location: cities.php');
		exit;
	}
	elseif (in_array($_SESSION['role'], ["Admin","Editor"]) && count($_POST)!=0 && isset($_REQUEST['mode']) && in_array(trim($_REQUEST['mode']),["add","edit"]) && isset($_POST['city']) && isset($_POST['postcode']) && isset($_POST['fees']) && !empty($_POST['city']) && !empty($_POST['postcode']) && !empty($_POST['fees']))
	{
		$p['city']= "'".trim(mysqli_real_escape_string($con, $_POST['city']))."'";
		$p['postcode']= "'".trim(mysqli_real_escape_string($con, $_POST['postcode']))."'";
		$p['fees']= trim(mysqli_real_escape_string($con, $_POST['fees']));
		switch (trim($_REQUEST['mode']))
		{
			case "add":
				mysqli_query($con, "INSERT INTO cities (city,postcode,fees)	VALUES ({$p['city']},{$p['postcode']},{$p['fees']})");
				break;
			case "edit":
				mysqli_query($con, "UPDATE cities SET postcode={$p['postcode']}, fees={$p['fees']} WHERE city={$p['city']}");
				break;
		}
		header('Location: cities.php');
		exit;
	}
	else
	{
		echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
		getHead(["color"=>$_SESSION['color'],"title"=>trans("Shipping Cities"),"icon"=>"/assets/images/logo-icon.png","css"=>["/assets/css/admin-modern.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css"]]);
?>
<body class="fixed-layout <?php echo $_SESSION['theme']." lock-nav\">"; getPreloader(); ?>
	<div id="main-wrapper"><?php require "topbar"; require "left-sidebar"; ?>
		<div class="page-wrapper">
			<div class="container-fluid">
				<div class="row page-titles">
					<div class="col-md-5 align-self-center">
						<h4 class="text-themecolor"><?php echo trans("Shipping Cities"); ?></h4>
					</div>
					<div class="col-md-7 align-self-center text-right">
						<div class="d-flex justify-content-end align-items-center">
							<ol class="breadcrumb">
								<li class="breadcrumb-item">
									<a href="index.php"><?php echo trans("Home"); ?></a>
								</li>
								<li class="breadcrumb-item active"><?php echo trans("Shipping Cities"); ?></li>
							</ol>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
								<div class="modal fade in" id="add-city" role="dialog" style="display: none;" tabindex="-1">
									<div class="modal-dialog">
										<form class="modal-content" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
											<input type="hidden" name="mode" value="add" />
											<div class="modal-header">
												<h4 class="modal-title"><?php echo trans("Add City"); ?></h4><button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
											</div>
											<div class="modal-body">
												<div class="form-group">
													<div class="col-md-12 m-b-20">
														<input class="form-control" name="city" maxlength="50" placeholder="<?php echo trans("City"); ?>" type="text" autocomplete="off" required="" autofocus />
													</div>
													<div class="col-md-12 m-b-20">
														<input class="form-control" name="postcode" maxlength="20" placeholder="<?php echo trans("Postcode"); ?>" type="text" autocomplete="off" required="" />
													</div>
													<div class="col-md-12 m-b-20">
														<input class="form-control" name="fees" placeholder="<?php echo trans("Fees"); ?>" type="number" step="0.001" autocomplete="off" required="" />
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button class="btn btn-info waves-effect" type="submit"><?php echo trans("Save"); ?></button>
												<button class="btn btn-default waves-effect" data-dismiss="modal" type="button"><?php echo trans("Cancel"); ?></button>
											</div>
										</form>
									</div>
								</div>
								<div class="modal fade in" id="modify-city" role="dialog" style="display: none;" tabindex="-1">
									<div class="modal-dialog">
										<form class="modal-content" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
											<input type="hidden" name="mode" value="edit" />
											<div class="modal-header">
												<h4 class="modal-title"><?php echo trans("Modify City"); ?></h4><button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
											</div>
											<div class="modal-body">
												<div class="form-group">
													<div class="col-md-12 m-b-20">
														<input class="form-control" name="city" readonly="readonly" />
													</div>
													<div class="col-md-12 m-b-20">
														<input class="form-control" name="postcode" maxlength="20" placeholder="<?php echo trans("Postcode"); ?>" type="text" autocomplete="off" required="" />
													</div>
													<div class="col-md-12 m-b-20">
														<input class="form-control" name="fees" placeholder="<?php echo trans("Fees"); ?>" type="number" step="0.001" autocomplete="off" required="" />
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button class="btn btn-info waves-effect" type="submit"><?php echo trans("Save"); ?></button>
												<button class="btn btn-default waves-effect" data-dismiss="modal" type="button"><?php echo trans("Cancel"); ?></button>
											</div>
										</form>
									</div>
								</div>
								<button class="btn btn-info btn-rounded m-t-10 mb-2 float-right" data-target="#add-city" data-toggle="modal" type="button"><?php echo trans("Add City"); ?></button>
<?php
		$r= mysqli_query($con, "SELECT * FROM cities ORDER BY city");
		if (!mysqli_num_rows($r))
			echo "<div class=\"alert alert-warning\" style=\"margin: 0; clear: both;\"><h3 class=\"text-warning\"><i class=\"far fa-frown\"></i> ".trans("Sorry")."</h3>".trans("We couldn't find any record in database!")."</div>\n";
		else
		{
?>
								<div class="table-responsive">
									<table class="table table-bordered m-t-30">
										<colgroup><col style="width: 30%"><col style="width: 30%"><col style="width: 30%"></colgroup>
<?php
			echo "<thead><tr><th>".trans("City")."</th><th>".trans("Postcode")."</th><th>".trans("Fees")."</th><th>".trans("Options")."</th></tr></thead><tbody>";
			while ($row= mysqli_fetch_assoc($r))
				echo "<tr><td>{$row['city']}</td><td>{$row['postcode']}</td><td>{$row['fees']}</td><td><button class=\"btn btn-info fas fa-pen\" data-target=\"#modify-city\" data-toggle=\"modal\" onclick=\"var form=document.querySelectorAll('#modify-city input');form[1].value='{$row['city']}';form[2].value='{$row['postcode']}';form[3].value='{$row['fees']}';\"></button> <a href=\"".$_SERVER['PHP_SELF']."?delete=".$row['city']."\" class=\"btn btn-danger fas fa-trash\"></a></td></tr>";
?>
										</tbody>
									</table>
								</div>
<?php
		}
?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><?php require "right-sidebar"; getFooter(); ?>
	</div><?php getJSCalls(["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js","/assets/js/popper.min.js","https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js","/assets/js/perfect-scrollbar.jquery.min.js","/assets/js/waves.js","/assets/js/sidebarmenu.js","/assets/js/custom.js","/assets/js/sessionTimeout.php"]); ?>
</body>
</html>
<?php
	}
	mysqli_close($con);
?>