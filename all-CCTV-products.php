<?php
	require_once "assets/config.php";
	include 'cart.php';
	$cart= new Cart;
	echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
	getHead(["color"=>"#1A569F","title"=>trans("Video surveillance"),
			"keywords"=>"Vidéo Surveillance,CCTV,Système Alarme,Détection d’Incendie,Télédistribution,Pointeuse,Contrôle d'accés,Videophone,Porte Automatique,GPS,Accessoires,anti-intrusion,anti-incendie,caméra de surveillance,contrôle d'accès,géolocalisation,sécurité,sécurité maison,tunisie,tunis",
			"description"=>"SOTEKAM: Notre solution de surveillance CCTV",
			"icon"=>"/assets/images/logo-icon-dark.png",
			"css"=>["https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css","assets/old/css/lightbox.min.css","assets/old/css/stack-interface.css","assets/old/css/theme.css"],
			"js"=>["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"]]);
	echo "<body data-smooth-scroll-offset=\"77\">\n";
	getPreloader("public");
	getNavbar("transp", $cart->total_items());
?>
	<script async defer src="https://apis.google.com/js/platform.js">{lang: 'fr'}</script>
	<a id="start"></a>
	<div class="main-container">
		<section class="imageblock switchable switchable--switch feature-large bg--dark space--sm">
			<div class="imageblock__content col-md-6 col-sm-4 pos-right">
				<div class="background-image-holder"><img alt="image" src="assets/images/landing-02.jpg"></div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-sm-7 mt--2">
						<h1>Notre solution de surveillance CCTV</h1>
						<p class="lead">SOTEKAM vous propose des solutions vous permettant de contrôler le rendement de vos employés et de garder un œil sur votre entreprise et sur votre marchandise.</p>
						<div class="modal-instance block">
							<div class="video-play-icon video-play-icon--sm modal-trigger"></div><span><strong>Regarder l'Aperçu</strong>&nbsp;&nbsp;&nbsp;136 secondes</span>
							<div class="modal-container">
								<div class="modal-content bg-dark" data-height="60%" data-width="60%">
									<iframe allowfullscreen="allowfullscreen" src="https://www.youtube.com/embed/unj6McogtL0?autoplay=1"></iframe>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="imageblock switchable feature-large space--sm">
			<div class="imageblock__content col-md-6 col-sm-4 pos-right">
				<div class="background-image-holder"><img alt="image" src="assets/images/landing-03.jpg"></div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-sm-7">
						<br />
						<br />
						<br />
						<h2>La vidéo surveillance</h2>
						<p class="lead">avec des caméras IP ou des caméras analogiques pour l'intérieur et l'extérieur, jour et nuit.<br />
						Nos caméras vous permettent de visualiser à distance votre salon, votre chambre, votre magasin, votre entreprise, ...</p><br />
					</div>
				</div>
			</div>
		</section>
		<section class="space--xs hidden-md hidden-xs hidden-xss">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-md-3">
						<h3>Surveillance à Distance</h3>
						<p class="lead">Visualiser en direct, contrôler, enregistrer des vidéos de vos caméras, video encodeurs<br />
						et DVRs...<br />
						Tout cela à partir de votre téléphone mobile ou tablette!</p>
					</div>
					<div class="col-sm-4 col-md-4 col-md-offset-1">
						<video autoplay="" height="550" loop="" width="350"><source src="assets/videos/phone_slide.mp4" type="video/mp4"></video>
					</div>
					<div class="col-sm-4 col-md-3 col-md-offset-1">
						<hr class="short">
						<p>Permet à l'utilisateur de transmettre l'affichage vidéo via WIFI. Prend en charge la commutation multi-canaux, les captures d'écran... Prend en charge ptz haut et bas, gauche et droite, focus, zoom et contrôle iris et autres fonctions...<br />
						Prend en charge la fonction de lecture à distance.</p><br /><br /><br /><br />
						<a href="https://play.google.com/store/apps/details?id=com.vss.vssmobile" target="_blank"><img alt="Obtenez le sur Google Play" height="58" src="assets/images/badge_googleplay.png" width="192"></a><br /><br />
						<a href="https://itunes.apple.com/us/app/vss-mobile/id717131265" target="_blank"><img alt="Télécharger le sur App Store" height="58" src="assets/images/badge_appstore.png" width="192"></a>
					</div>
				</div>
			</div>
		</section>
		<section class="text-center">
			<div class="container">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
						<h2>Tout ce qu'il faut pour sécuriser</h2>
						<p class="lead">SOTEKAM Vente le matériel vidéo CCTV de tout type: Analogiques, IP, HDCVI, HCVR, DVR, NVR, accessoires de vidéo surveillances, etc...</p>
					</div>
				</div>
			</div>
		</section>
		<section class="space--sm">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="masonry">
							<div class="masonry-filter-container">
								<span>Catégorie:</span>
								<div class="masonry-filter-holder">
									<div class="masonry__filters" data-filter-all-text="Toute Catégories"></div>
								</div>
							</div>
							<div class="row">
								<div class="masonry__container">
<?php
			$r= mysqli_query($con, "SELECT id,code,title,subcategory,pic1,sp,vat FROM products WHERE (category='CCTV' OR subcategory='Accessoires Caméras') AND visibility='on'");
			if (mysqli_num_rows($r)==0)
				echo "<script>window.location.replace('index.php');</script>";
			else
				while ($row= mysqli_fetch_array($r))
				{
?>
									<div class="masonry__item col-sm-4" data-masonry-filter="<?php echo $row['subcategory'] ?>">
										<div class="product">
											<a href="product.php?code=<?php echo $row['id'] ?>"><img alt="Image" src="assets/images/products/<?php echo $row['pic1'] ?>"></a>
											<a class="block" href="product.php?code=<?php echo $row['id'] ?>">
											<div><span><?php echo $row['title'] ?></span> <h5><?php echo $row['code'] ?></h5></div>
											<div><span class="h4 inline-block"><?php echo ($row['sp']!=0)? (float)$row['sp']*(1+$row['vat']/100)." TND" : "Sur demande"; ?></span></div></a>
										</div>
									</div>
<?php
				}
?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="text-center imagebg space--xs parallax" data-overlay="4">
			<div class="background-image-holder"><img alt="background" src="assets/images/hero-01.jpg"></div>
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-md-7">
						<div class="cta">
							<p class="lead">Chaque achat de SOTEKAM est livré avec une garantie de 5 ans au maximum - avec une support gratuit.</p>
							<p class="type--fine-print">Pour plus d'informations,&nbsp;<a class="inner-link" href="contact-us.php">contactez-nous</a>.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="bg--primary unpad cta cta-2" id="video">
			<a href="#">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 text-center">
						<h2>Utiliser la technologie pour lutter contre le crime</h2>
					</div>
				</div>
			</div></a>
		</section>
<?php
	getFooter("dark_large");
	mysqli_close($con);
?>
		<a class="back-to-top inner-link" data-scroll-class="100vh:active" href="#start"><i class="stack-interface stack-up-open-big"></i></a>
	</div><?php getJSCalls(["https://cdnjs.cloudflare.com/ajax/libs/granim/2.0.0/granim.min.js","https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js","assets/old/js/smooth-scroll.min.js","assets/old/js/scripts.js"]); ?>
	<script async type="text/javascript" src="https://www.googletagmanager.com/gtag/js?id=UA-135626600-1"></script>
	<script async type="text/javascript">
		window.dataLayer= window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-135626600-1');
	</script>
</body>
</html>