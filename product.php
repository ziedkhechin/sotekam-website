<?php
	require_once "assets/config.php";
	if (isset($_GET['code']) && !empty($_GET['code']))
		$r= mysqli_query($con, "SELECT id,code,title,brand,category,shortdescr,descr,pic1,pic2,pic3,pic4,pic5,pdf,sp,vat FROM products WHERE id='".trim(mysqli_real_escape_string($con, $_GET['code']))."' AND visibility='on'");
	if (!isset($_GET['code']) || mysqli_num_rows($r)!=1)
	{
		header ("Location: error.php?code=404");
		exit;
	}
	$row= mysqli_fetch_array($r);
	include 'cart.php';
	$cart= new Cart;
	echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
	getHead(["color"=>"#1A569F","title"=>$row['title']." ".$row['code'],
			"keywords"=>"Vidéo Surveillance,CCTV,Système Alarme,Détection d’Incendie,Télédistribution,Pointeuse,Contrôle d'accés,Videophone,Porte Automatique,GPS,Accessoires,anti-intrusion,anti-incendie,caméra de surveillance,contrôle d'accès,géolocalisation,sécurité,sécurité maison,tunisie,tunis",
			"icon"=>"/assets/images/logo-icon-dark.png",
			"css"=>["https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.min.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css","assets/old/css/stack-interface.css","assets/old/css/theme.css"]]);
	echo "<body data-smooth-scroll-offset=\"77\">\n";
	getNavbar("simple", $cart->total_items());
?>
	<a id="start"></a>
	<div class="main-container">
		<section class="space--xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h1><?php echo $row['title']." ".(!empty($row['brand'])? $row['brand']." ":"").$row['code']; ?></h1>
						<ol class="breadcrumbs">
							<li>
								<a href="index.php">Accueil</a>
							</li>
							<li>Produits</li>
							<li><?php echo $row['code']; ?></li>
						</ol>
						<hr>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="container">
				<div class="row">
<?php
	if (strlen($row['pic1'].$row['pic2'].$row['pic3'].$row['pic4'].$row['pic5'])!=0)
	{
?>
					<div class="col-sm-6 col-md-6">
						<div class="slider border--round boxed--border" data-arrows="true" data-paging="true">
							<ul class="slides">
<?php
		echo (!empty($row['pic1']))? "\t\t\t\t\t\t\t\t<li><img alt=\"Image\" src=\"assets/images/products/{$row['pic1']}\"></li>\n":"";
		echo (!empty($row['pic2']))? "\t\t\t\t\t\t\t\t<li><img alt=\"Image\" src=\"assets/images/products/{$row['pic2']}\"></li>\n":"";
		echo (!empty($row['pic3']))? "\t\t\t\t\t\t\t\t<li><img alt=\"Image\" src=\"assets/images/products/{$row['pic3']}\"></li>\n":"";
		echo (!empty($row['pic4']))? "\t\t\t\t\t\t\t\t<li><img alt=\"Image\" src=\"assets/images/products/{$row['pic4']}\"></li>\n":"";
		echo (!empty($row['pic5']))? "\t\t\t\t\t\t\t\t<li><img alt=\"Image\" src=\"assets/images/products/{$row['pic5']}\"></li>\n":"";
?>
							</ul>
						</div>
					</div>
<?php
	}
?>
					<div class="col-sm-5 col-md-5 col-md-offset-1">
						<h2><?php echo $row['code']; ?></h2>
						<div class="text-block">
							<span class="h4 inline-block"><p><?php echo ($row['sp']!=0)? (float)$row['sp']*(1+$row['vat']/100)." TND" : "Sur demande"; ?></p></span>
						</div>
						<p><?php echo $row['shortdescr']; ?></p>
						<ul class="accordion accordion-2 accordion--oneopen">
<?php
	if (!empty($row['descr']))
	{
?>
							<li class="active">
								<div class="accordion__title">
									<span class="h5"><p>Caractéristiques</p></span>
								</div>
								<div class="accordion__content">
									<ul class="bullets">
<?php
		$descr= explode("\n", $row['descr']);
		foreach ($descr as $line)
			echo "\t\t\t\t\t\t\t\t\t\t<li><span>".$line."</span></li>\n";
?>
									</ul>
								</div>
							</li>
<?php
	}
	if (!empty($row['pdf']))
	{
?>
							<li>
								<div class="accordion__title">
									<span class="h5"><p>Télécharger</p></span>
								</div>
								<div class="accordion__content">
									<a href="<?php echo $row['pdf']; ?>" target="_blank"><?php echo basename($row['pdf']); ?></a>
								</div>
							</li>
<?php
	}
?>
						</ul>
						<form name="cartform" method="get" action="cartAction.php">
							<input name="action" type="hidden" value="addToCart">
							<input name="id" type="hidden" value="<?php echo $row["id"]; ?>">
							<div class="col-sm-6 col-md-4">
								<input name="qte" placeholder="Qte" type="number" min="1" value="1">
							</div>
							<div class="col-sm-6 col-md-8">
								<button class="col-md-12 btn btn--primary" style="font-size: unset; height: unset;" type="submit">
								<span class="btn__text">Ajouter au panier</span></button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
<?php
	$r= mysqli_query($con, "SELECT id,code,title,pic1,sp,vat FROM products WHERE category='{$row['category']}' AND visibility='on' ORDER BY RAND() LIMIT 3");
	if (mysqli_num_rows($r)!=1)
	{
?>
		<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="text-block">
							<h3>Produits Associés</h3>
						</div>
					</div>
<?php
		while ($row= mysqli_fetch_array($r))
		{
?>
					<div class="col-sm-4">
						<div class="product">
							<a href="product.php?code=<?php echo $row['id'] ?>"><img alt="Image" src="assets/images/products/<?php echo $row['pic1'] ?>"></a>
							<a class="block" href="HAC-HFW1200S.php"><div><span><?php echo $row['title'] ?></span> <h5><?php echo $row['code'] ?></h5></div>
							<div><span class="h4 inline-block"><?php echo ($row['sp']!=0)? (float)$row['sp']*(1+$row['vat']/100)." TND" : "Sur demande"; ?></span></div></a>
						</div>
					</div>
<?php
		}
?>
				</div>
			</div>
		</section>
<?php
	}
	getFooter("dark");
	mysqli_close($con);
?>
		<a class="back-to-top inner-link" data-scroll-class="100vh:active" href="#start"><i class="stack-interface stack-up-open-big"></i></a>
	</div><?php getJSCalls(["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js","https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.0/flickity.pkgd.min.js","assets/old/js/smooth-scroll.min.js","assets/old/js/scripts.js"]); ?>
	<script async type="text/javascript" src="https://www.googletagmanager.com/gtag/js?id=UA-135626600-1"></script>
	<script async type="text/javascript">
		window.dataLayer= window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-135626600-1');
	</script>
</body>
</html>