<?php
	require_once "assets/config.php";
	if (!isset($_GET['code']) || !in_array(trim($_GET['code']),['400','403','404','500','503']))
	{
		header ("Location: error.php?code=404");
		exit;
	}
	else
	{
		echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
			getHead(["color"=>"#009ee5","title"=>trans("Error Page"),"icon"=>"/assets/images/logo-icon-dark.png","css"=>["/assets/css/error.css","https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CRoboto:300,400,500,600,700","https://use.fontawesome.com/releases/v5.6.1/css/all.css"]]);
?>
<body>
	<style type="text/css">
		.m-error-6 {background-image: url('/assets/images/error_bg.jpg');}
		@media (min-width: 769px) {.fas {font-size: 94px; bottom: 12px; position: relative;}}
		@media (max-width: 768px) {.fas {font-size: 47px; bottom:7px; position: relative;}}
		span {color: #ffc500; cursor: pointer;}
	</style>
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<div class="m-grid__item m-grid__item--fluid m-grid m-error-6">
			<div class="m-error_container">
<?php
		echo "\t\t\t\t<div class=\"m-error_subtitle m--font-light\"><h1>";
		switch (trim($_GET['code'])) 
		{
			case "400": echo "400"; break;
			case "403": echo "4<i class=\"fas fa-lock\"></i>3"; break;
			case "404": echo "4<i class=\"fas fa-ghost\"></i>4"; break;
			case "500": echo "500"; break;
			case "503": echo "5<i class=\"fas fa-skull\"></i>3"; break;
		}
		echo "</h1></div>\n";
?>
				<p class="m-error_description m--font-light"><?php echo trans(trim($_GET['code']))."<br />".trans("If you want, you can")."\n\t\t\t\t\t<span onclick=\"(window.history.length==1)? window.location.replace('index.php'):window.history.back();\">".trans("return to the previous page")."</span>\n\t\t\t\t</p>\n"; ?>
			</div>
		</div>
	</div>
</body>
</html>
<?php
	}
	mysqli_close($con);
?>