<?php
	require_once "assets/config.php";
	include 'cart.php';
	$cart= new Cart;
	if ($cart->total_items()==0)
	{
		header ("Location: shop-cart.php");
		exit;
	}
	echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
	getHead(["color"=>"#1A569F","title"=>trans("Checkout"),
			"icon"=>"/assets/images/logo-icon-dark.png",
			"css"=>["https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css","assets/old/css/lightbox.min.css","assets/old/css/stack-interface.css","assets/old/css/theme.css"],
			"js"=>["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"]]);
	echo "<body data-smooth-scroll-offset=\"77\">\n";
	getPreloader("public");
	getNavbar("simple", $cart->total_items());
?>
	<div class="main-container">
		<iframe src="order-step-1.php" height="710"></iframe>
<?php
	getFooter("dark");
	mysqli_close($con);
?>
	</div>
</body>
</html>