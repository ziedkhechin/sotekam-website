<?php
	require_once "assets/config.php";
	include 'cart.php';
	$cart= new Cart;
	echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
	getHead(["color"=>"#1A569F","title"=>trans("Basket"),
			"icon"=>"/assets/images/logo-icon-dark.png",
			"css"=>["https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css","assets/old/css/stack-interface.css","assets/old/css/theme.css"]]);
	echo "<body data-smooth-scroll-offset=\"77\">\n";
	getNavbar("simple", $cart->total_items());
?>
	<a id="start"></a>
	<div class="main-container">
		<section class="space--xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h1>Panier d'Achat</h1>
						<hr>
					</div>
				</div>
			</div>
		</section>
		<section class="space--xs">
			<div class="container">
				<form class="cart-form">
					<div class="row">
<?php
	if ($cart->total_items()>0)
	{
		$cartItems= $cart->contents();
		foreach ($cartItems as $item)
		{
?>
						<div class="col-sm-3">
							<div>
								<div class="product__controls">
									<div class="col-xs-3">
										<label>Quantité:</label>
									</div>
									<div class="col-xs-6">
										<input min="1" name="quantity" placeholder="Qte" required="" type="number" value="<?php echo $item["qty"]; ?>" onchange="updateCartItem(this, '<?php echo $item["rowid"]; ?>')">
									</div>
									<div class="col-xs-3 text-right">
										<a class="checkmark checkmark--cross bg--error" href="cartAction.php?action=removeCartItem&id=<?php echo $item["rowid"]; ?>"></a>
									</div>
								</div><img alt="Image" src="assets/images/products/<?php echo $item["image"]; ?>">
								<div>
									<h5><?php echo $item["model"]; ?></h5><span><?php echo $item["name"]; ?></span>
								</div>
								<div>
									<span class="h4 inline-block"><?php echo $item["price"]; ?> TND	</span>
								</div>
							</div>
						</div>
<?php
		}
	}
	else
	{
?>
						<br /><br /><br /><br />
						<div class="row boxed boxed--lg boxed--border bg--secondary">
							<h3><center><b>Votre panier est vide.</b></center></h3>
							<p><center>Pour plus d'information, n'hésitez pas à <a href="contact-us.php">Contactez-nous</a>.</center></p>
						</div>
						<br /><br /><br /><br /><br /><br /><br />
<?php
	}
?>
					</div>
<?php
	if ($cart->total_items()>0)
	{
?>
					<div class="row">
						<div class="col-md-2 col-md-offset-10 text-right text-center-xs">
							<a class="btn btn--sm bg--error" id="modalOptions"><span class="btn__text">Vider le Panier</span></a>
						</div>
					</div>
					<div class="row mt--1">
						<div class="col-sm-12">
							<div class="boxed boxed--border cart-total">
								<div>
									<div class="col-xs-6">
										<span class="h5">Sous-total:</span>
									</div>
									<div class="col-xs-6 text-right">
										<span><?php echo number_format($cart->total(), 2); ?> TND</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<br />
					<div class="row">
						<div class="text-right text-center-xs">
							<a class="btn btn--sm type--uppercase" onclick="window.history.back();"><span class="btn__text">&laquo; Retour</span></a>
							<a class="btn btn--sm btn--primary type--uppercase" href="checkout.php"><span class="btn__text">Commander &raquo;</span></a>
						</div>
					</div>
<?php
	}
?>
				</form>
			</div>
		</section>
<?php
	getFooter("dark");
	mysqli_close($con);
?>
		<a class="back-to-top inner-link" data-scroll-class="100vh:active" href="#start"><i class="stack-interface stack-up-open-big"></i></a>
	</div><?php getJSCalls(["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js","assets/old/js/jquery.confirm.js","assets/old/js/bootstrap.min.js","assets/old/js/smooth-scroll.min.js","assets/old/js/scripts.js"]); ?>
	<script async type="text/javascript" src="https://www.googletagmanager.com/gtag/js?id=UA-135626600-1"></script>
	<script async type="text/javascript">
		window.dataLayer= window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-135626600-1');
		
		$("#modalOptions").confirm({
			text: "Êtes-vous sûr de supprimer tous les articles du panier?",
			modalOptionsBackdrop: 'static',
			modalOptionsKeyboard: false,
			confirm: function() {window.location.replace("cartAction.php?action=removeAllCartItems");}
		});

		function updateCartItem(obj,id)
		{
			$.get("cartAction.php", {action:"updateCartItem", id:id, qty:obj.value}, function(data)
			{
				if (data=='ok')
					location.reload();
				else
					alert('La mise à jour du panier a échoué, réessayez.');
			});
		}
	</script>
</body>
</html>