<?php
	require_once "assets/config.php";
	include 'cart.php';
	$cart= new Cart;
	echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
	getHead(["color"=>"#1A569F","title"=>"Porte Automatique Coulissante",
			"icon"=>"/assets/images/logo-icon-dark.png",
			"css"=>["https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.min.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css","assets/old/css/stack-interface.css","assets/old/css/theme.css"],
			"js"=>["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"]]);
	echo "<body class=\"boxed-layout\">\n";
	getPreloader("public");
	getNavbar("transp", $cart->total_items());
?>
	<a id="start"></a>
	<div class="main-container">
			<section class="cover height-80 text-center imagebg parallax" data-overlay="7">
				<div class="background-image-holder">
					<img alt="background" src="assets/images/landing-14.jpg" />
				</div>
				<div class="container pos-vertical-center">
					<div class="row">
						<div class="col-sm-12">
							<br /><h1>Confort et sécurité: une porte automatique<br />de qualité vous facilite la vie.</h1>
							<div class="modal-instance block">
								<div class="video-play-icon modal-trigger"></div>
								<div class="modal-container">
									<div class="modal-content bg-dark" data-width="60%" data-height="60%">
										<iframe src="https://www.youtube.com/embed/YebELwFNhB0?autoplay=1&start=9&end=54" allowfullscreen="allowfullscreen"></iframe>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="space--xs text-center">
				<div class="container">
					<div class="row">
						<div class="col-sm-10 col-md-8">
							<span class="h1">Etes-vous à la recherche de motorisation portail?</span>
							<p class="lead">Nous vous proposons des solutions adéquates à votre portail: moteur portail battant, kit motorisation portail, ainsi qu’un système de sécurité complet à<br />
							installer à partir du portail battant.</p><br /><br />
						</div>
						
					</div>
					<div>
					<img alt="background" src="assets/images/landing-15.jpg" />
				</div>
				</div>
			</section>
			<section class="height-80 parallax imagebg">
				<div class="background-image-holder">
					<img alt="background" src="assets/images/landing-16.jpg" />
				</div>
			</section>
			<section class="text-center bg--dark unpad--bottom">
				<div class="container">
					<div class="row">
						<div class="col-sm-10 col-md-8">
							<span class="h1">Porte Basculante Automatique</span>
							<p class="lead">Porte de garage basculante pré montée avec cadre dormant en profilés tubulaires rectangulaires, rails de guidage avec arrêts de sécurité et panneau de
							porte en une partie, verrouillage par deux pênes autobloquants latéraux.</p><br /><br />
						</div>
					</div>
				</div>
				<img alt="Image" height="600" src="assets/images/landing-18.jpg" /><br />
			</section>
			<section class="space--xs text-center">
				<div class="container">
					<div class="row">
						<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2"><br />
							<h2>Comme toute nouveauté!</h2>
							<p class="lead">la porte automatique se devait d'apporter des améliorations. Les nouvelles technologies permettent ainsi une utilisation plus pratique et plus sûre.
							Comment? Tour d'horizon des avantages de la porte automatique:</p>
						</div>
					</div>
				</div>
			</section>
			<section class="space--xs">
				<div class="container">
					<div class="row">
						<div class="col-sm-4">
							<div class="feature boxed boxed--sm boxed--border">
								<i class="icon icon-Security-Block"></i>
								<h4>Sécurité</h4>
								<hr>
								<p>Verrouillage en trois points empêchant les infractions, arrêt sur obstacle, ouverture manuelle possible de l'intérieur.</p>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="feature boxed boxed--sm boxed--border">
								<i class="icon icon-Smile"></i>
								<h4>Confort</h4>
								<hr>
								<p>Ouverture et fermeture par simple pression d'un bouton de télécommande, éclairage en option.</p>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="feature boxed boxed--sm boxed--border">
								<i class="icon icon-Timer-2"></i>
								<h4>Rapidité</h4>
								<hr>
								<p>Il garantit une ouverture plus rapide de la porte par rapport aux portes normales.</p>
							</div>
						</div>
					</div>
				</div>
			</section>
		<section class="imagebg space--sm text-center" data-gradient-bg="#8F48BD,#5448BD,#C70039,#BD48B1">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-md-6">
						<div class="cta">
							<h2>Solutions de Sécurité sur Mesure.</h2><a class="btn btn--primary type--uppercase" href="automatic-door.php"><span class="btn__text">Découvrir les produits de Porte Automatique<br /></span><span class="label">Nouveau</span></a>
						</div>
					</div>
				</div>
			</div>
		</section>
<?php
	getFooter("dark");
	mysqli_close($con);
?>
		<a class="back-to-top inner-link" data-scroll-class="100vh:active" href="#start"><i class="stack-interface stack-up-open-big"></i></a>
	</div><?php getJSCalls(["assets/old/js/parallax.js","https://cdnjs.cloudflare.com/ajax/libs/granim/2.0.0/granim.min.js","assets/old/js/jquery.steps.min.js","assets/old/js/smooth-scroll.min.js","assets/old/js/scripts.js"]); ?>
	<script async type="text/javascript" src="https://www.googletagmanager.com/gtag/js?id=UA-135626600-1"></script>
	<script async type="text/javascript">
		window.dataLayer= window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-135626600-1');
	</script>
</body>
</html>