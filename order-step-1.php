<?php
	require_once "assets/config.php";
	include 'cart.php';
	$cart= new Cart;
	if ($cart->total_items()==0)
	{
		header ("Location: shop-cart.php");
		exit;
	}
	elseif (isset($_POST['email'],$_POST['last_name'],$_POST['first_name'],$_POST['phone'],$_POST['address1'],$_POST['city'],$_POST['zip']) && !empty($_POST['email']) && !empty($_POST['phone']) && !empty($_POST['address1']) && !empty($_POST['city']) && preg_match("/^[A-Za-z]{3,30}$/",$_POST['last_name']) && preg_match("/^[A-Za-z]{3,30}$/",$_POST['first_name']) && preg_match("/^[0-9]{4,5}$/",$_POST['zip']))
	{
		$cartItems= $cart->contents();
		$c= ["email"=> trim(mysqli_real_escape_string($con, $_POST['email'])),"last_name"=> trim(mysqli_real_escape_string($con, $_POST['last_name'])),"company"=>trim(mysqli_real_escape_string($con, $_POST['company'])),"first_name"=> trim(mysqli_real_escape_string($con, $_POST['first_name'])),
		"phone"=> trim(mysqli_real_escape_string($con, $_POST['phone'])),"address1"=> trim(mysqli_real_escape_string($con, $_POST['address1'])),"address2"=>trim(mysqli_real_escape_string($con, $_POST['address2'])),"city"=> trim(mysqli_real_escape_string($con, $_POST['city'])),"zip"=> trim(mysqli_real_escape_string($con, $_POST['zip']))];
		$r= $db->query("SELECT id FROM customers WHERE email='{$c['email']}'");
		if ($r->num_rows)
		{
			$id_ctl= $r->fetch_assoc()['id'];
			$db->query("UPDATE customers SET phone='{$c['phone']}', last_name='{$c['last_name']}', first_name='{$c['first_name']}', company='{$c['company']}', address1='{$c['address1']}', address2='{$c['address2']}', city='{$c['city']}', zip='{$c['zip']}' WHERE email='{$c['email']}'");
		}
		else
		{
			$db->query("INSERT INTO customers (email,last_name,first_name,company,address1,address2,city,zip,phone) VALUES ('{$c['email']}', '{$c['last_name']}', '{$c['first_name']}', '{$c['company']}', '{$c['address1']}', '{$c['address2']}', '{$c['city']}', '{$c['zip']}', '{$c['phone']}')");
			$id_ctl= $db->insert_id;
		}
		$db->query("INSERT INTO orders (customer_id) VALUES ($id_ctl)");
		$id_ord= $db->insert_id;
		foreach ($cartItems as $item)
			$db->query("INSERT INTO order_items (order_id, product_id, quantity) VALUES ('$id_ord',{$item['id']},{$item['qty']})");
		header ("Location: order-step-2.php?action=submitOrder&id=".$id_ord);
		exit;
	}
	else
	{
?>
<!DOCTYPE html>
<html class="js multi-step windows chrome desktop page--no-banner page--show card-fields cors svg opacity csspointerevents placeholder no-touchevents displaytable display-table generatedcontent cssanimations boxshadow flexbox flexboxlegacy no-flexboxtweener anyflexbox no-shopemoji floating-labels">
<head>
	<link href="assets/old/css/v2-ltr-edge.css" media="all" rel="stylesheet">
</head>
<body>
	<button class="order-summary-toggle order-summary-toggle--show" data-drawer-toggle="[data-order-summary]">
	<div class="wrap">
		<div class="order-summary-toggle__inner">
			<div class="order-summary-toggle__icon-wrapper">
				<svg class="order-summary-toggle__icon" height="19" width="20" xmlns="http://www.w3.org/2000/svg">
				<path d="M17.178 13.088H5.453c-.454 0-.91-.364-.91-.818L3.727 1.818H0V0h4.544c.455 0 .91.364.91.818l.09 1.272h13.45c.274 0 .547.09.73.364.18.182.27.454.18.727l-1.817 9.18c-.09.455-.455.728-.91.728zM6.27 11.27h10.09l1.454-7.362H5.634l.637 7.362zm.092 7.715c1.004 0 1.818-.813 1.818-1.817s-.814-1.818-1.818-1.818-1.818.814-1.818 1.818.814 1.817 1.818 1.817zm9.18 0c1.004 0 1.817-.813 1.817-1.817s-.814-1.818-1.818-1.818-1.818.814-1.818 1.818.814 1.817 1.818 1.817z"></path></svg>
			</div>
			<div class="order-summary-toggle__text order-summary-toggle__text--show">
				<span>Afficher le résumé de la commande</span> <svg class="order-summary-toggle__dropdown" height="6" width="11" xmlns="http://www.w3.org/2000/svg">
				<path d="M.504 1.813l4.358 3.845.496.438.496-.438 4.642-4.096L9.504.438 4.862 4.534h.992L1.496.69.504 1.812z"></path></svg>
			</div>
			<div class="order-summary-toggle__text order-summary-toggle__text--hide">
				<span>Cacher le résumé de la commande</span> <svg class="order-summary-toggle__dropdown" height="7" width="11" xmlns="http://www.w3.org/2000/svg">
				<path d="M6.138.876L5.642.438l-.496.438L.504 4.972l.992 1.124L6.138 2l-.496.436 3.862 3.408.992-1.122L6.138.876z"></path></svg>
			</div>
		</div>
	</div></button>
	<div class="content">
		<div class="wrap">
			<div class="sidebar" role="complementary">
				<div class="sidebar__content">
					<div class="order-summary order-summary--is-collapsed" data-order-summary="">
						<div class="order-summary__sections">
							<div class="order-summary__section order-summary__section--product-list">
								<div class="order-summary__section__content">
									<table class="product-table">
										<tbody>
<?php
		$cartItems= $cart->contents();
		foreach ($cartItems as $item)
		{
?>
											<tr class="product">
												<td class="product__image">
													<div class="product-thumbnail">
														<div class="product-thumbnail__wrapper"><img class="product-thumbnail__image" src="assets/images/products/<?php echo $item["image"]; ?>"></div><span aria-hidden="true" class="product-thumbnail__quantity"><?php echo $item["qty"]; ?></span>
													</div>
												</td>
												<td class="product__description"><span class="product__description__name order-summary__emphasis"><?php echo $item["model"]; ?></span> <span class="product__description__variant order-summary__small-text"><?php echo $item["name"]; ?></span></td>
												<td class="product__price"><span class="order-summary__emphasis"><?php echo $item["qty"]; ?> × <?php echo $item["price"]; ?> TND</span></td>
											</tr>
<?php
		}
?>
										</tbody>
									</table>
									<div class="order-summary__scroll-indicator">Défiler pour plus d'articles<svg height="12" viewbox="0 0 10 12" width="10" xmlns="http://www.w3.org/2000/svg"><path d="M9.817 7.624l-4.375 4.2c-.245.235-.64.235-.884 0l-4.375-4.2c-.244-.234-.244-.614 0-.848.245-.235.64-.235.884 0L4.375 9.95V.6c0-.332.28-.6.625-.6s.625.268.625.6v9.35l3.308-3.174c.122-.117.282-.176.442-.176.16 0 .32.06.442.176.244.234.244.614 0 .848"></path></svg>
									</div>
								</div>
							</div>
							<div class="order-summary__section order-summary__section--total-lines">
								<table aria-atomic="true" aria-live="polite" class="total-line-table">
									<tbody class="total-line-table__tbody">
										<tr class="total-line total-line--subtotal">
											<td class="total-line__name">Sous-total :</td>
											<td class="total-line__price"><span class="order-summary__emphasis"><?php echo number_format($cart->total(), 2); ?> <span class="payment-due__currency"><span class="payment-due__currency">TND</span></span></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="main" role="main">
				<div class="main__header">
					<ul class="breadcrumb">
						<li class="breadcrumb__item breadcrumb__item--completed"><a href="shop-cart.php" target="_top">Panier</a>&nbsp;&raquo;&nbsp;</li>
						<li class="breadcrumb__item breadcrumb__item--current"><span class="breadcrumb__text">Informations du Client</span>&nbsp;&raquo;&nbsp;</li>
						<li class="breadcrumb__item breadcrumb__item--blank"><span class="breadcrumb__text">Méthode de livraison</span>&nbsp;&raquo;&nbsp;</li>
						<li class="breadcrumb__item breadcrumb__item--blank"><span>Succès</span></li>
					</ul>
				</div>
				<div class="main__content">
					<div class="step">
						<form accept-charset="UTF-8" name="ContactForm" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" autocomplete="on">
							<div class="step__sections">
								<div class="section section--contact-information">
									<div class="section__header">
										<div class="layout-flex layout-flex--tight-vertical layout-flex--loose-horizontal layout-flex--wrap">
											<h2 class="section__title layout-flex__item layout-flex__item--stretch">Informations du Client</h2>
										</div>
									</div>
									<div class="section__content">
										<div class="fieldset">
											<div class="field field--required">
												<div class="field__input-wrapper">
													<label class="field__label field__label--visible">Email</label>
													<input required="" autocomplete="shipping email" class="field__input" name="email" placeholder="Email" size="30" type="email">
												</div>
											</div>
											<div class="field field--required field--half">
												<div class="field__input-wrapper">
													<label class="field__label field__label--visible">Nom</label>
													<input required="" pattern="[A-Za-z]{3,30}" autocomplete="shipping family-name" class="field__input" name="last_name" placeholder="Nom" size="30" type="text">
												</div>
											</div>
											<div class="field field--required field--half">
												<div class="field__input-wrapper">
													<label class="field__label field__label--visible">Prénom</label>
													<input required="" pattern="[A-Za-z]{3,30}" autocomplete="shipping given-name" class="field__input" name="first_name" placeholder="Prénom" size="30" type="text">
												</div>
											</div>
											<div class="field field--optional">
												<div class="field__input-wrapper">
													<label class="field__label field__label--visible">Société (optionel)</label>
													<input pattern="[A-Za-z]{3,30}" autocomplete="shipping organization" class="field__input" name="company" placeholder="Société (optionel)" size="30" type="text">
												</div>
											</div>
											<div class="field field--required">
												<div class="field__input-wrapper">
													<label class="field__label field__label--visible">Numéro de Téléphone</label>
													<input required="" autocomplete="shipping tel" class="field__input field__input--numeric" name="phone" placeholder="Numéro de Téléphone" size="15" type="text">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="section section--shipping-address">
									<div class="section__header">
										<h2 class="section__title">Adresse de Livraison</h2>
									</div>
									<div class="section__content">
										<div class="field field--required field--two-thirds">
											<div class="field__input-wrapper">
												<label class="field__label field__label--visible">Adresse</label>
												<input required="" autocomplete="shipping address-line1" class="field__input" name="address1" placeholder="Adresse" size="40" type="text">
											</div>
										</div>
										<div class="field field--optional field--third">
											<div class="field__input-wrapper">
												<label class="field__label field__label--visible">Apt, suite, etc. (optionel)</label>
												<input autocomplete="shipping address-line2" class="field__input" name="address2" placeholder="Apt, suite, etc. (optionel)" size="40" type="text">
											</div>
										</div>
										<div class="field field--required field--show-floating-label field--half">
											<div class="field__input-wrapper field__input-wrapper--select">
												<label class="field__label field__label--visible">Ville</label>
												<select required="" class="field__input field__input--select" name="city" size="1">
<?php
		$rq= "SELECT city FROM cities ORDER BY city";
		$q1= $db->query($rq);
		if (!$q1)
			echo $db->connect_error;
		else
			while ($row= $q1->fetch_assoc())
				echo "<option value=\"{$row['city']}\">{$row['city']}</option>";
?>
												</select>
											</div>
										</div>
										<div class="field field--required field--half">
											<div class="field__input-wrapper">
												<label class="field__label field__label--visible">Code postal</label>
												<input required="" pattern="[0-9]{4,5}" autocomplete="shipping postal-code" class="field__input field__input--zip" name="zip" placeholder="Code postal" size="6" type="text">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="step__footer">
								<button class="step__footer__continue-btn btn" type="submit"><span>Suivant</span></button>
								<a class="step__footer__previous-link" href="shop-cart.php" target="_top">
									<svg class="icon-svg icon-svg--color-accent icon-svg--size-10 previous-link__icon rtl-flip" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 10"><path d="M8 1L7 0 3 4 2 5l1 1 4 4 1-1-4-4"></path></svg>
									<span class="step__footer__previous-link-content">Retour au panier</span>
								</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="assets/old/js/checkout.js"></script>
	<script type="text/javascript">ContactForm.city.selectedIndex=-1</script>
</body>
</html>
<?php
	}
	mysqli_close($con);
?>