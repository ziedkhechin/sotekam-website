<?php
	require_once "assets/config.php";
	include 'cart.php';
	$cart= new Cart;
	echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
	getHead(["color"=>"#1A569F","title"=>trans("Tv-distribution"),
			"keywords"=>"Vidéo Surveillance,CCTV,Système Alarme,Détection d’Incendie,Télédistribution,Pointeuse,Contrôle d'accés,Videophone,Porte Automatique,GPS,Accessoires,anti-intrusion,anti-incendie,caméra de surveillance,contrôle d'accès,géolocalisation,sécurité,sécurité maison,tunisie,tunis",
			"description"=>"SOTEKAM: Découvrez les meilleurs produits pour la réception TV par satellite",
			"icon"=>"/assets/images/logo-icon-dark.png",
			"css"=>["https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.min.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css","assets/old/css/lightbox.min.css","assets/old/css/stack-interface.css","assets/old/css/theme.css"],
			"js"=>["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"]]);
	echo "<body data-smooth-scroll-offset=\"77\">\n";
	getPreloader("public");
	getNavbar("transp", $cart->total_items());
?>
	<a id="start"></a>
	<div class="main-container">
		<section class="cover height-80 imagebg text-center parallax" data-overlay="5">
			<div class="background-image-holder"><img alt="background" src="assets/images/landing-27.jpg"></div>
			<div class="container pos-vertical-center">
				<div class="row">
					<div class="col-sm-11">
						<h2>Découvrez les meilleurs produits pour la réception TV par satellite</h2>
						<p>La réception TV par satellite demande toutefois un équipement particulier composé notamment<br />
						d'une télévision, d'une antenne satellite et d'un Multiswitch.</p>
					</div>
				</div>
			</div>
		</section>
		<section class="text-center bg--secondary">
			<div class="container">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
						<h2>Les Produits Professionnel de SOTEKAM</h2>
						<p class="lead">de la plus haute qualité dans la production.</p>
					</div>
				</div>
			</div>
		</section>
		<section class="space--xs bg--secondary">
			<div class="container">
				<div class="row">
					<div class="gallery-1">
                        <div class="masonry__item col-sm-6 col-xs-12" data-masonry-filter="Digital">
                            <div class="project-thumb hover-element hover--active">
                                <a data-lightbox="Gallery 1" href="img/work-03.jpg">
                                   <div class="hover-element__initial">
                                       <div class="background-image-holder"><img alt="Image" src="assets/images/work-03.jpg"></div>
                                   </div>
                                </a>
                            </div>
                        </div>
                        <div class="masonry__item col-sm-6 col-xs-12" data-masonry-filter="Digital">
                            <div class="project-thumb hover-element hover--active">
                                <a data-lightbox="Gallery 1" href="img/work-07.jpg">
                                   <div class="hover-element__initial">
                                       <div class="background-image-holder"><img alt="Image" src="assets/images/work-07.jpg"></div>
                                   </div>
                                </a>
                            </div>
                         </div>
				    </div>
					<div class="gallery-1">
						<div class="col-sm-12">
							<div class="gallery__image">
								<a data-lightbox="Gallery 1" href="img/work-08.jpg">
								   <div class="background-image-holder"><img alt="Image" src="assets/images/work-08.jpg"></div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="space--xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="masonry">
							<div class="masonry-filter-container">
								<span>Catégorie:</span>
								<div class="masonry-filter-holder">
									<div class="masonry__filters" data-filter-all-text="Toute Catégories"></div>
								</div>
							</div>
							<div class="row">
								<div class="masonry__container">
<?php
			$r= mysqli_query($con, "SELECT id,code,title,subcategory,pic1,sp,vat FROM products WHERE category IN ('Multiswitch','LNB','Television','TV Receivers') AND visibility='on'");
			if (mysqli_num_rows($r)==0)
				echo "<script>window.location.replace('index.php');</script>";
			else
				while ($row= mysqli_fetch_array($r))
				{
?>
									<div class="masonry__item col-sm-4" data-masonry-filter="<?php echo $row['subcategory'] ?>">
										<div class="product">
											<a href="product.php?code=<?php echo $row['id'] ?>"><img alt="Image" src="assets/images/products/<?php echo $row['pic1'] ?>"></a>
											<a class="block" href="product.php?code=<?php echo $row['id'] ?>">
											<div><span><?php echo $row['title'] ?></span> <h5><?php echo $row['code'] ?></h5></div>
											<div><span class="h4 inline-block"><?php echo ($row['sp']!=0)? (float)$row['sp']*(1+$row['vat']/100)." TND" : "Sur demande"; ?></span></div></a>
										</div>
									</div>
<?php
				}
?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="imagebg parallax" data-overlay="3">
			<div class="background-image-holder"><img alt="background" src="assets/images/landing-29.jpg"></div>
			<div class="container">
				<div class="row">
					<div class="col-sm-7 col-md-5">
						<div>
							<div class="col-md-10 col-md-offset-1 col-sm-12">
								<h3>Next: nouvelle qualité des systèmes SMATV</h3>
								<p>Les systèmes basés sur des multiswitches sont des solutions professionnelles et populaires pour distribuer des émissions FM et TV terrestres<br />
								(FM / DAB (+) et DVB-T)
								et des signaux satellites (DVB-S / S2) à plusieurs points d'abonné.</p>
								<hr class="short">
								<p>Les produits de Next dédiés aux systèmes d'antenne partagés sont des équipements de première classe
								avec une garantie de trois ans.
								Les réseaux de distribution SMATV basés sur ces dispositifs assurent un fonctionnement fiable pendant une longue période.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="text-center bg--secondary">
			<div class="container">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
						<h2>Prix simple pour tous</h2>
						<p class="lead">Si vous recherchez des produits TV ou SATELLITE, nous avons une option pour tous!</p>
					</div>
				</div>
			</div>
		</section>
<?php
	getFooter("dark_large");
	mysqli_close($con);
?>
		<a class="back-to-top inner-link" data-scroll-class="100vh:active" href="#start"><i class="stack-interface stack-up-open-big"></i></a>
	</div><?php getJSCalls(["https://cdnjs.cloudflare.com/ajax/libs/granim/2.0.0/granim.min.js","https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.0/flickity.pkgd.min.js","https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js","assets/old/js/parallax.js","assets/old/js/smooth-scroll.min.js","assets/old/js/scripts.js"]); ?>
	<script async type="text/javascript" src="https://www.googletagmanager.com/gtag/js?id=UA-135626600-1"></script>
	<script async type="text/javascript">
		window.dataLayer= window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-135626600-1');
	</script>
</body>
</html>