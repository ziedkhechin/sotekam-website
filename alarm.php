<?php
	require_once "assets/config.php";
	include 'cart.php';
	$cart= new Cart;
	echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
	getHead(["color"=>"#1A569F","title"=>trans("Alarm System"),
			"keywords"=>"Vidéo Surveillance,CCTV,Système Alarme,Détection d’Incendie,Télédistribution,Pointeuse,Contrôle d'accés,Videophone,Porte Automatique,GPS,Accessoires,anti-intrusion,anti-incendie,caméra de surveillance,contrôle d'accès,géolocalisation,sécurité,sécurité maison,tunisie,tunis",
			"description"=>"SOTEKAM vous propose des solutions efficaces pour lutter le crime",
			"icon"=>"/assets/images/logo-icon-dark.png",
			"css"=>["https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.min.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css","assets/old/css/lightbox.min.css","assets/old/css/stack-interface.css","assets/old/css/theme.css"],
			"js"=>["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"]]);
	echo "<body data-smooth-scroll-offset=\"77\">\n";
	getPreloader("public");
	getNavbar("transp", $cart->total_items());
?>
	<a id="start"></a>
	<div class="main-container">
		<section class="cover height-70 imagebg text-center slider slider--ken-burns" data-arrows="true" data-paging="true">
			<ul class="slides">
				<li class="imagebg" data-overlay="6">
					<div class="background-image-holder background--top"><img alt="background" src="assets/images/landing-23.jpg"></div>
					<div class="container pos-vertical-center">
						<div class="row">
							<div class="col-sm-12">
								<h1>Nous disposons d'un large choix des systèmes de sécurité</h1>
							</div>
						</div>
					</div>
				</li>
				<li class="imagebg" data-overlay="6">
					<div class="background-image-holder"><img alt="background" src="assets/images/landing-24.gif"></div>
					<div class="container pos-vertical-center">
						<div class="row">
							<div class="col-sm-12">
								<h1>SOTEKAM vous propose des solutions efficaces<br />pour lutter le crime</h1>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</section>
		<section class="text-center cta cta-4 space--xxs unpad--bottom bg--secondary">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<span class="label label--inline">Important!</span> <span>Nous sommes fiers de l'excellent service à la clientèle que nous fournissons. <a href="contact-us.php">Contactez-nous</a> aujourd'hui et nous vous aiderons dans votre requête.</span>
					</div>
				</div>
			</div>
		</section>
		<section class="switchable switchable--switch bg--secondary">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 col-md-5">
							<div class="switchable__text">
							<h2>Nos alarmes intrus sont surveillées en continu</h2>
							<p class="lead">Nous comprenons les défis rencontrés lors de la protection de votre entreprise contre l'accès non autorisé et pouvons fournir un système d'alarme intrus adaptée à vos besoins. En utilisant une technologie intégrée de détection
							d'intrusion intégrée, SOTEKAM peut vous aider à protéger vos locaux et les membres du public 24 heures sur 24, 365 jours par an.</p>
						</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="slider box-shadow-wide border--round" data-arrows="true" data-paging="true">
								<ul class="slides">
									<li> <img alt="img" src="assets/images/landing-25.jpg"> </li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="space--xs switchable bg--secondary">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 col-md-5">
							<div class="switchable__text">
							<p class="lead">Chaque entreprise a des besoins différents, c'est pourquoi nos systèmes d'alarme sont conçus individuellement pour fournir la plus grande protection. Des capteurs de fenêtre et des contacts magnétiques à la dernière
							technologie de détection de mouvement et de vibration, nous trouverons le mélange idéal d'équipements pour aider votre entreprise à rester sécurisée.</p>
						</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="slider box-shadow-wide border--round" data-arrows="true" data-paging="true">
								<ul class="slides">
									<li><img alt="img" src="assets/images/landing-26.jpg"></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</section>
		<section class="space--xs bg--secondary">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="feature feature-2 boxed boxed--border">
							<div>
								<p>Il existe deux types de systèmes d'alarme. Les systèmes d'alarme sans fil fonctionnent sur les batteries et non sur l'électricité. Ce qui signifie
								qu'ils sont efficaces lors des coupures de courant ainsi. Les systèmes câblés n'utilisent pas de piles et consomment de l'électricité à partir de la source.
								Donc, il n'est pas nécessaire de recharger ou de remplacer régulièrement la batterie.</p>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="feature feature-2 boxed boxed--border">
							<div>
								<p>Les maisons et les propriétés commerciales qui ont des systèmes d'alarme contre le cambriolage sont moins susceptibles d'être visés par des intrus
								et des cambrioleurs. Lorsque les infiltrés découvrent que vous avez un système d'alarme installé, ils sont convaincus de s'éloigner de votre propriété.
								Même s'ils ne sont pas conscients de sa présence, le son alarmant les effraie.</p>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="feature feature-2 boxed boxed--border">
							<div>
								<p>Il est pratiquement impossible de nommer des gardes de sécurité ou d'y être personnellement pour protéger la propriété 24 heures sur 24.
								Ces systèmes de sécurité offrent une protection 24 heures sur 24 contre les cambrioleurs et les interruptions.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="space--xs text-center bg--secondary">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h2>Choisissez un système d'alarme adaptée au type d'intrusion</h2>
					</div>
				</div>
			</div>
		</section>
		<section class="space--sm">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="masonry">
							<div class="masonry-filter-container">
								<span>Catégorie:</span>
								<div class="masonry-filter-holder">
									<div class="masonry__filters" data-filter-all-text="Toute Catégories"></div>
								</div>
							</div>
							<div class="row">
								<div class="masonry__container">
<?php
			$r= mysqli_query($con, "SELECT id,code,title,subcategory,pic1,sp,vat FROM products WHERE (category='Alarm System' OR subcategory='Batteries Centrale') AND visibility='on'");
			if (mysqli_num_rows($r)==0)
				echo "<script>window.location.replace('index.php');</script>";
			else
				while ($row= mysqli_fetch_array($r))
				{
?>
									<div class="masonry__item col-sm-4" data-masonry-filter="<?php echo $row['subcategory'] ?>">
										<div class="product">
											<a href="product.php?code=<?php echo $row['id'] ?>"><img alt="Image" src="assets/images/products/<?php echo $row['pic1'] ?>"></a>
											<a class="block" href="product.php?code=<?php echo $row['id'] ?>">
											<div><span><?php echo $row['title'] ?></span> <h5><?php echo $row['code'] ?></h5></div>
											<div><span class="h4 inline-block"><?php echo ($row['sp']!=0)? (float)$row['sp']*(1+$row['vat']/100)." TND" : "Sur demande"; ?></span></div></a>
										</div>
									</div>
<?php
				}
?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="space--xs text-center bg--secondary">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-md-6">
						<div class="cta">
							<h2>Solutions de Sécurité sur Mesure.</h2>
						</div>
					</div>
				</div>
			</div>
		</section>
<?php
	getFooter("white");
	mysqli_close($con);
?>
		<a class="back-to-top inner-link" data-scroll-class="100vh:active" href="#start"><i class="stack-interface stack-up-open-big"></i></a>
	</div><?php getJSCalls(["https://cdnjs.cloudflare.com/ajax/libs/granim/2.0.0/granim.min.js","https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.0/flickity.pkgd.min.js","https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js","assets/old/js/smooth-scroll.min.js","assets/old/js/scripts.js"]); ?>
	<script async type="text/javascript" src="https://www.googletagmanager.com/gtag/js?id=UA-135626600-1"></script>
	<script async type="text/javascript">
		window.dataLayer= window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-135626600-1');
	</script>
</body>
</html>