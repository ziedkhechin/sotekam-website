<?php
	require_once "assets/config.php";
	include 'cart.php';
	$cart= new Cart;
	echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
	getHead(["color"=>"#1A569F","title"=>trans("Presentation"),
			"keywords"=>"Vidéo Surveillance,CCTV,Système Alarme,Détection d’Incendie,Télédistribution,Pointeuse,Contrôle d'accés,Videophone,Porte Automatique,GPS,Accessoires,anti-intrusion,anti-incendie,caméra de surveillance,contrôle d'accès,géolocalisation,sécurité,sécurité maison,tunisie,tunis",
			"description"=>"Sécurité, alarmes et systèmes de protection",
			"icon"=>"/assets/images/logo-icon-dark.png",
			"css"=>["https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.min.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css","assets/old/css/stack-interface.css","assets/old/css/theme.css"],
			"js"=>["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"]]);
	echo "<body data-smooth-scroll-offset=\"77\">\n";
	getPreloader("public");
	getNavbar("simple", $cart->total_items());
?>
	<a id="start"></a>
	<div class="main-container">
		<section class="text-center imagebg space--lg" data-overlay="6">
			<div class="background-image-holder"><img alt="background" src="assets/images/landing-01.jpg"></div>
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-md-6">
						<h1>Sécurité, alarmes et systèmes de protection</h1>
						<p class="lead">Utiliser la technologie pour lutter contre le crime</p>
					</div>
				</div>
			</div>	
		</section>
		<section class="text-center">
			<div class="container">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
						<h2>Une entreprise distributrice de matériel de sécurité électronique.</h2>
						<p class="lead">Nous travaillons, exclusivement, avec des professionnels. Nous offrons à nos clients:</p>
						<ul>
							<li>- Une structure organisée et expérimentée</li>
							<li>- Un stock important et varié</li>
							<li>- Un centre de transfert de compétence, garantissant une assistance gratuite et continue</li>
						</ul>
						<p></p>
					</div>
				</div>
			</div>
		</section>
		<section class="text-center">
			<div class="container">
				<div class="row">
					<div class="col-sm-11">
						<div class="slider border--round" data-arrows="true" data-paging="true">
							<ul class="slides">
								<li><img alt="Image" src="assets/images/gallery-01.jpg"></li>
								<li><img alt="Image" src="assets/images/gallery-02.jpg"></li>
								<li><img alt="Image" src="assets/images/gallery-03.jpg"></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="text-center bg--secondary space--xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
						<h2>Qui sommes-nous?</h2>
						<p>SOTEKAM est un fabricant de solutions de sécurité et d'automatisation pour les applications résidentielles et commerciales. Des systèmes d'intrusion, d'incendie et de domotique, à la dernière vidéo IP et au contrôle d'accès, nous nous concentrons sur les technologies qui créent des «maisons connectées» et des «bâtiments connectés». Notre mission est:</p>
					</div>
				</div>
			</div>
		</section>
		<section class="bg--secondary">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="feature">
							<h4>Satisfaire nos clients</h4>
							<p>tout en leur présentant des solutions de sécurité adéquates qui répondent à leurs exigences et leurs attentes.</p>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="feature">
							<h4>Garantir</h4>
							<p>le suivi de la sécurité et du bon fonctionnement des équipements là où ils sont installés.</p>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="feature">
							<h4>Fournir</h4>
							<p>tous les besoins du client</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="imagebg space--sm text-center" data-gradient-bg="#8F48BD,#5448BD,#C70039,#BD48B1">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-md-6">
						<div class="cta">
							<h2>Solutions de Sécurité sur Mesure.</h2><a class="btn btn--primary type--uppercase" href="all-CCTV-products.php"><span class="btn__text">Découvrir les produits CCTV<br /></span><span class="label">NEW</span></a>
						</div>
					</div>
				</div>
			</div>
		</section>
<?php
	getFooter("dark");
	mysqli_close($con);
?>
		<a class="back-to-top inner-link" data-scroll-class="100vh:active" href="#start"><i class="stack-interface stack-up-open-big"></i></a>
	</div><?php getJSCalls(["assets/old/js/parallax.js","https://cdnjs.cloudflare.com/ajax/libs/granim/2.0.0/granim.min.js","https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.0/flickity.pkgd.min.js","assets/old/js/jquery.steps.min.js","assets/old/js/smooth-scroll.min.js","assets/old/js/scripts.js"]); ?>
	<script async type="text/javascript" src="https://www.googletagmanager.com/gtag/js?id=UA-135626600-1"></script>
	<script async type="text/javascript">
		window.dataLayer= window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-135626600-1');
	</script>
</body>
</html>