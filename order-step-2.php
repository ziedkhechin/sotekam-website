<?php
	require_once "assets/config.php";
	include 'cart.php';
	$cart= new Cart;
	if (!(isset($_REQUEST['action'], $_REQUEST['id']) && !empty($_REQUEST['id'])))
	{
		header ("Location: order-step-1.php");
		exit;
	}
	else
	{
		$r= $db->query("SELECT address1, address2, zip, status, cities.city, cities.fees FROM customers INNER JOIN orders ON customers.id=orders.customer_id INNER JOIN cities ON customers.city=cities.city WHERE orders.id=".trim(mysqli_real_escape_string($con, $_REQUEST['id'])));
		$row= $r->fetch_assoc();
		if ($row['status']!='0' || $cart->total_items()==0)
		{
			header ("Location: error.php?code=403");
			exit;
		}
		$adr= $row['address1'].(($row['address2']!="")? ", ".$row['address2']:"").", ".$row['zip'].", ".$row['city']." - Tunisie";
		$fee= $row['fees'];
		$cartItems= $cart->contents();
		$workforce= 0;
		foreach ($cartItems as $item)
		{
			$r= $db->query("SELECT sp,vat FROM products WHERE id=".$item['id']);
			$row= $r->fetch_assoc();
			$workforce+= $item['qty']*$row['sp']*(1+($row['vat']/100))*0.25;
		}
		if ($_REQUEST['action']=='confirming' && isset($_POST['method']) && in_array($_POST['method'], ["0","1"]))
		{
			$db->query("UPDATE orders SET method='{$_POST['method']}', fees=".(($_POST['method']=="0")? $fee:$fee+$workforce).", total=".(($_POST['method']=="0")? $cart->total()+$fee : $cart->total()+$workforce+$fee).", date=now() WHERE id={$_REQUEST['id']}");
			header ("Location: order-step-3.php?action=submitOrder&id={$_REQUEST['id']}");
			exit;
		}
		elseif ($_REQUEST['action']=='submitOrder')
		{
?>
<!DOCTYPE html>
<html class="js multi-step windows chrome desktop page--no-banner page--show card-fields cors svg opacity csspointerevents placeholder no-touchevents displaytable display-table generatedcontent cssanimations boxshadow flexbox flexboxlegacy no-flexboxtweener anyflexbox no-shopemoji floating-labels">
<head>
	<link href="assets/old/css/v2-ltr-edge.css" media="all" rel="stylesheet">
</head>
<body onload="change()">
	<button class="order-summary-toggle order-summary-toggle--show" data-drawer-toggle="[data-order-summary]">
	<div class="wrap">
		<div class="order-summary-toggle__inner">
			<div class="order-summary-toggle__icon-wrapper">
				<svg class="order-summary-toggle__icon" height="19" width="20" xmlns="http://www.w3.org/2000/svg">
				<path d="M17.178 13.088H5.453c-.454 0-.91-.364-.91-.818L3.727 1.818H0V0h4.544c.455 0 .91.364.91.818l.09 1.272h13.45c.274 0 .547.09.73.364.18.182.27.454.18.727l-1.817 9.18c-.09.455-.455.728-.91.728zM6.27 11.27h10.09l1.454-7.362H5.634l.637 7.362zm.092 7.715c1.004 0 1.818-.813 1.818-1.817s-.814-1.818-1.818-1.818-1.818.814-1.818 1.818.814 1.817 1.818 1.817zm9.18 0c1.004 0 1.817-.813 1.817-1.817s-.814-1.818-1.818-1.818-1.818.814-1.818 1.818.814 1.817 1.818 1.817z"></path></svg>
			</div>
			<div class="order-summary-toggle__text order-summary-toggle__text--show">
				<span>Afficher le résumé de la commande</span> <svg class="order-summary-toggle__dropdown" height="6" width="11" xmlns="http://www.w3.org/2000/svg">
				<path d="M.504 1.813l4.358 3.845.496.438.496-.438 4.642-4.096L9.504.438 4.862 4.534h.992L1.496.69.504 1.812z"></path></svg>
			</div>
			<div class="order-summary-toggle__text order-summary-toggle__text--hide">
				<span>Cacher le résumé de la commande</span> <svg class="order-summary-toggle__dropdown" height="7" width="11" xmlns="http://www.w3.org/2000/svg">
				<path d="M6.138.876L5.642.438l-.496.438L.504 4.972l.992 1.124L6.138 2l-.496.436 3.862 3.408.992-1.122L6.138.876z"></path></svg>
			</div>
		</div>
	</div></button>
	<div class="content">
		<div class="wrap">
			<div class="sidebar" role="complementary">
				<div class="sidebar__content">
					<div class="order-summary order-summary--is-collapsed" data-order-summary="">
						<div class="order-summary__sections">
							<div class="order-summary__section order-summary__section--product-list">
								<div class="order-summary__section__content">
									<table class="product-table">
										<tbody>
<?php
			$cartItems= $cart->contents();
			foreach($cartItems as $item)
			{
?>
											<tr class="product">
												<td class="product__image">
													<div class="product-thumbnail">
														<div class="product-thumbnail__wrapper"><img class="product-thumbnail__image" src="assets/images/products/<?php echo $item["image"]; ?>"></div><span aria-hidden="true" class="product-thumbnail__quantity"><?php echo $item["qty"]; ?></span>
													</div>
												</td>
												<td class="product__description"><span class="product__description__name order-summary__emphasis"><?php echo $item["model"]; ?></span> <span class="product__description__variant order-summary__small-text"><?php echo $item["name"]; ?></span></td>
												<td class="product__price"><span class="order-summary__emphasis"><?php echo $item["qty"]; ?> × <?php echo $item["price"]; ?> TND</span></td>
											</tr>
<?php
			}
?>
										</tbody>
									</table>
									<div class="order-summary__scroll-indicator">Défiler pour plus d'articles<svg height="12" viewbox="0 0 10 12" width="10" xmlns="http://www.w3.org/2000/svg"><path d="M9.817 7.624l-4.375 4.2c-.245.235-.64.235-.884 0l-4.375-4.2c-.244-.234-.244-.614 0-.848.245-.235.64-.235.884 0L4.375 9.95V.6c0-.332.28-.6.625-.6s.625.268.625.6v9.35l3.308-3.174c.122-.117.282-.176.442-.176.16 0 .32.06.442.176.244.234.244.614 0 .848"></path></svg></div>
								</div>
							</div>
							<div class="order-summary__section order-summary__section--total-lines">
								<table aria-atomic="true" aria-live="polite" class="total-line-table">
									<tbody class="total-line-table__tbody">
										<tr class="total-line total-line--subtotal">
											<td class="total-line__name">Sous-total :</td>
											<td class="total-line__price"><span class="order-summary__emphasis"><?php echo number_format($cart->total(), 2); ?> <span class="payment-due__currency">TND</span></span></td>
										</tr>
										<tr class="total-line total-line--shipping">
											<td class="total-line__name">Frais :</td>
											<td class="total-line__price"><span class="order-summary__emphasis"><span id="p1"></span> <span class="payment-due__currency">TND</span></span></td>
										</tr>
									</tbody>
									<tfoot class="total-line-table__footer">
										<tr class="total-line">
											<td class="total-line__name payment-due-label"><span class="payment-due-label__total">Total :</span></td>
											<td class="total-line__price payment-due"><span class="payment-due__price"><span id="p2"></span></span><span class="payment-due__currency">&nbsp;&nbsp;TND</span></td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="main" role="main">
				<div class="main__header">
					<ul class="breadcrumb">
						<li class="breadcrumb__item breadcrumb__item--completed">
							<a href="shop-cart.php" target="_top">Panier</a> <span>&nbsp; &raquo; &nbsp;</span>
						</li>
						<li class="breadcrumb__item breadcrumb__item--completed">
							<a href="customer-informations.php">Informations du Client</a> <span>&nbsp; &raquo; &nbsp;</span>
						</li>
						<li class="breadcrumb__item breadcrumb__item--current"><span class="breadcrumb__text">Méthode de livraison</span> <span>&nbsp; &raquo; &nbsp;</span></li>
						<li class="breadcrumb__item breadcrumb__item--blank"><span>Succès</span></li>
					</ul>
				</div>
				<div class="main__content">
					<div class="step">
						<form accept-charset="UTF-8" method="post" name="livraisonForm" action="<?php echo $_SERVER['PHP_SELF']; ?>">
							<input type="hidden" name="action" value="confirming" />
							<input type="hidden" name="id" value="<?php echo $_REQUEST['id']; ?>" />
							<div class="step__sections">
								<div class="section">
									<div class="content-box">
										<div class="content-box__row content-box__row--tight-spacing-vertical content-box__row--secondary">
											<div class="review-block">
												<div class="review-block__inner">
													<div class="review-block__label">Adresse de Livraison</div>
													<div class="review-block__content"><?php echo $adr; ?></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="section section--shipping-method">
									<div class="section__header">
										<h2 class="section__title">Méthode de Livraison</h2>
									</div>
									<div class="section__content">
										<div class="content-box">
											<div class="content-box__row">
												<div class="radio-wrapper">
													<div class="radio__input">
														<input onchange="change();" checked="checked" class="input-radio" name="method" type="radio" value="1">
													</div><span class="radio__label__primary">&nbsp; &nbsp; Livraison avec installation</span> <span class="radio__label__accessory"><span class="content-box__emphasis"><?php echo number_format($workforce+$fee, 2); ?> TND</span></span>
												</div>
											</div>
											<div class="content-box__row">
												<div class="radio-wrapper">
													<div class="radio__input">
														<input onchange="change();" class="input-radio" name="method" type="radio" value="0">
													</div><span class="radio__label__primary">&nbsp; &nbsp; Livraison seulement</span> <span class="radio__label__accessory"><span class="content-box__emphasis"><?php echo number_format($fee, 2); ?> TND</span></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="step__footer">
								<button class="step__footer__continue-btn btn" type="submit"><span>Confirmer</span></button>
							<a class="step__footer__previous-link" href="order-step-1.php"><svg class="icon-svg icon-svg--color-accent icon-svg--size-10 previous-link__icon rtl-flip" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 10"><path d="M8 1L7 0 3 4 2 5l1 1 4 4 1-1-4-4"></path></svg><span class="step__footer__previous-link-content">Retour à les informations du client</span></a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="assets/old/js/checkout.js"></script>
	<script type="text/javascript">
		function change()
		{
			switch (livraisonForm.method.value)
			{
				case "1":
					document.getElementById("p1").innerHTML= "<?php echo number_format($workforce+$fee, 2); ?>";
					document.getElementById("p2").innerHTML= "<?php echo number_format($cart->total()+$workforce+$fee, 2); ?>";
					break;
				case "0":
					document.getElementById("p1").innerHTML= "<?php echo number_format($fee, 2); ?>";
					document.getElementById("p2").innerHTML= "<?php echo number_format($cart->total()+$fee, 2); ?>";
					break;
			}
		}
	</script>
</body>
</html>
<?php
		}
	}
	mysqli_close($con);
?>