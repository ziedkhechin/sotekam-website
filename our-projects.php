<?php
	require_once "assets/config.php";
	include 'cart.php';
	$cart= new Cart;
	echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
	getHead(["color"=>"#1A569F","title"=>trans("Our Projects"),
			"icon"=>"/assets/images/logo-icon-dark.png",
			"css"=>["https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css","assets/old/css/stack-interface.css","assets/old/css/theme.css"],
			"js"=>["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"]]);
	echo "<body data-smooth-scroll-offset=\"77\">\n";
	getPreloader("public");
	getNavbar("transp", $cart->total_items());
?>
	<a id="start"></a>
	<div class="main-container">
		<section class="text-center imagebg space--lg" data-overlay="7">
			<div class="background-image-holder" target="_blank"><img alt="background" src="assets/images/cowork-03.jpg"></div>
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-md-6">
						<h1>Nos Projets</h1>
						<p class="lead">Parmi ceux qui font confiance à SOTEKAM, on retrouve des acteurs connus de différents secteurs de l'industrie et des services.</p>
					</div>
				</div>
			</div>
		</section>
		<section class="space--xxs"></section>
		<iframe src="companies.php" height="590px"></iframe>
<?php
	getFooter("white");
	mysqli_close($con);
?>
		<a class="back-to-top inner-link" data-scroll-class="100vh:active" href="#start"><i class="stack-interface stack-up-open-big"></i></a>
	</div><?php getJSCalls(["assets/old/js/jquery.steps.min.js","assets/old/js/smooth-scroll.min.js","assets/old/js/scripts.js"]); ?>
	<script async type="text/javascript" src="https://www.googletagmanager.com/gtag/js?id=UA-135626600-1"></script>
	<script async type="text/javascript">
		window.dataLayer= window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-135626600-1');
	</script>
</body>
</html>