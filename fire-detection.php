<?php
	require_once "assets/config.php";
	include 'cart.php';
	$cart= new Cart;
	echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
	getHead(["color"=>"#1A569F","title"=>trans("Fire Detection"),
			"keywords"=>"Vidéo Surveillance,CCTV,Système Alarme,Détection d’Incendie,Télédistribution,Pointeuse,Contrôle d'accés,Videophone,Porte Automatique,GPS,Accessoires,anti-intrusion,anti-incendie,caméra de surveillance,contrôle d'accès,géolocalisation,sécurité,sécurité maison,tunisie,tunis",
			"description"=>"Nous vous offrons la vente d’une gamme complète de système sécurité-incendie.",
			"icon"=>"/assets/images/logo-icon-dark.png",
			"css"=>["https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.min.css","https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css","assets/old/css/lightbox.min.css","assets/old/css/stack-interface.css","assets/old/css/theme.css"],
			"js"=>["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"]]);
	echo "<body data-smooth-scroll-offset=\"77\">\n";
	getPreloader("public");
	getNavbar("transp", $cart->total_items());
?>
	<a id="start"></a>
	<div class="main-container">
		<section class="text-center height-80 imagebg parallax" data-overlay="5">
			<div class="background-image-holder"><img alt="background" src="assets/images/landing-19.jpg"></div>
			<div class="container pos-vertical-center">
				<div class="row">
					<div class="col-sm-12">
						<div class="typed-headline">
							<span class="h2 inline-block">Le risque d’incendie est l’un des risques posés à la</span> <span class="h2 inline-block typed-text typed-text--cursor color--primary" data-typed-strings="santé,sécurité des personnes,propriété"></span>
						</div>
						<p class="lead">Nous vous offrons la vente d’une gamme complète de système sécurité-incendie.</p>
					</div>
				</div>
			</div>
		</section>
		<section class="imageblock switchable feature-large space--xxs bg--dark">
			<div class="imageblock__content col-md-6 col-sm-4 pos-right">
				<div class="background-image-holder"><img alt="image" src="assets/images/landing-20.jpg"></div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-sm-7">
						<h2>Sélection de votre système d'alarme incendie</h2>
						<p>Le type de système utilisé est souvent déterminé par la taille du bâtiment et la complexité de la stratégie d'incendie.</p>
						<p>Pour les locaux plus petits, cela pourrait être un système qui divise le bâtiment en un certain nombre de zones d'incendie simples et déclenche une évacuation si un incendie survient.</p>
						<p>Pour les locaux plus grands, un système de détection d'incendie adressable aurait tendance à être déployé où chaque détecteur possède une adresse unique permettant d'identifier
						précisément les incidents et la stratégie de prévention des incendies peut être programmée.</p>
					</div>
				</div>
			</div>
		</section>
		<section class="text-center">
			<div class="container">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
						<h2>SOTEKAM comprend les problèmes causés lorsqu'un incendie perturbe une entreprise.</h2>
						<p>Laissez-nous travaillez avec vous pour concevoir un système afin de minimiser les pertes et les perturbations de votre entreprise.</p>
					</div>
				</div>
			</div>
		</section>
		<section class="space--sm">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="masonry">
							<div class="masonry-filter-container">
								<span>Catégorie:</span>
								<div class="masonry-filter-holder">
									<div class="masonry__filters" data-filter-all-text="Toute Catégories"></div>
								</div>
							</div>
							<div class="row">
								<div class="masonry__container">
<?php
			$r= mysqli_query($con, "SELECT id,code,title,subcategory,pic1,sp,vat FROM products WHERE category='Fire Detection' AND visibility='on'");
			if (mysqli_num_rows($r)==0)
				echo "<script>window.location.replace('index.php');</script>";
			else
				while ($row= mysqli_fetch_array($r))
				{
?>
									<div class="masonry__item col-sm-4" data-masonry-filter="<?php echo $row['subcategory'] ?>">
										<div class="product">
											<a href="product.php?code=<?php echo $row['id'] ?>"><img alt="Image" src="assets/images/products/<?php echo $row['pic1'] ?>"></a>
											<a class="block" href="product.php?code=<?php echo $row['id'] ?>">
											<div><span><?php echo $row['title'] ?></span> <h5><?php echo $row['code'] ?></h5></div>
											<div><span class="h4 inline-block"><?php echo ($row['sp']!=0)? (float)$row['sp']*(1+$row['vat']/100)." TND" : "Sur demande"; ?></span></div></a>
										</div>
									</div>
<?php
				}
?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="unpad">
			<div class="masonry masonry--gapless">
				<div class="masonry__container masonry--active">
					<div class="masonry__item col-md-4 col-sm-6 col-xs-12" data-masonry-filter="Digital">
						<div class="project-thumb hover-element height-50">
							<div class="hover-element__initial">
								<div class="background-image-holder"><img alt="background" src="assets/images/landing-21.jpg"></div>
							</div>
						</div>
					</div>
					<div class="masonry__item col-md-8 col-sm-6 col-xs-12" data-masonry-filter="Digital">
						<div class="project-thumb hover-element height-50">
							<div class="hover-element__initial">
								<div class="background-image-holder"><img alt="background" src="assets/images/landing-22.jpg"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
<?php
	getFooter("dark");
	mysqli_close($con);
?>
		<a class="back-to-top inner-link" data-scroll-class="100vh:active" href="#start"><i class="stack-interface stack-up-open-big"></i></a>
	</div><?php getJSCalls(["https://cdnjs.cloudflare.com/ajax/libs/granim/2.0.0/granim.min.js","https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.0/flickity.pkgd.min.js","https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js","assets/old/js/parallax.js","assets/old/js/smooth-scroll.min.js","assets/old/js/scripts.js","assets/old/js/typed.min.js"]); ?>
	<script async type="text/javascript" src="https://www.googletagmanager.com/gtag/js?id=UA-135626600-1"></script>
	<script async type="text/javascript">
		window.dataLayer= window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-135626600-1');
	</script>
</body>
</html>