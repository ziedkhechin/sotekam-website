<?php
	require_once "assets/config.php";
	include 'cart.php';
	$cart= new Cart;
	echo "<!DOCTYPE html>\n<html lang=\"".$_SESSION['language']."-tn\">\n";
	getHead(["color"=>"#1A569F","title"=>trans("Search"),
			"icon"=>"/assets/images/logo-icon-dark.png",
			"css"=>["https://use.fontawesome.com/releases/v5.6.1/css/all.css","https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css","assets/old/css/stack-interface.css","assets/old/css/theme.css"],
			"js"=>["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"]]);
	echo "<body data-smooth-scroll-offset=\"77\">\n";
	getNavbar("simple", $cart->total_items());
?>
	<a id="start"></a>
	<div class="main-container">
		<section class="space--xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h1>Rechercher dans SOTEKAM</h1>
						<ol class="breadcrumbs">
							<li>
								<a href="index.php">Accueil</a>
							</li>
							<li>Rechercher</li>
						</ol>
						<hr>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="container">
				<form class="row" method="get" action="https://www.google.com/search" target="_blank">
					<input type="hidden" name="sitesearch" value="www.sotekam.tn">
					<div class="col-md-10 col-12"><input class="form-control" style="height: 39px" name="q" placeholder="Ecrire quelque chose" type="text"></div>
					<div class="col-md-2 col-12"><button type="submit" class="btn btn--primary type--uppercase">Rechercher</button></div>
				</form>
				<br /><br /><br /><br /><br /><br /><br /><br />
			</div>
		</section>
<?php
	getFooter("dark");
	mysqli_close($con);
?>
		<a class="back-to-top inner-link" data-scroll-class="100vh:active" href="#start"><i class="stack-interface stack-up-open-big"></i></a>
	</div><?php getJSCalls(["assets/old/js/smooth-scroll.min.js","assets/old/js/scripts.js","assets/old/js/typed.min.js"]); ?>
	<script async type="text/javascript" src="https://www.googletagmanager.com/gtag/js?id=UA-135626600-1"></script>
	<script async type="text/javascript">
		window.dataLayer= window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-135626600-1');
	</script>
</body>
</html>